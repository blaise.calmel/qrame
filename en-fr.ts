<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>Columns2Use</name>
    <message>
        <location filename="UI/wColumns2Use.py" line="65"/>
        <source>Columns to Use</source>
        <translation>Colonnes à utiliser</translation>
    </message>
    <message>
        <location filename="UI/wColumns2Use.py" line="66"/>
        <source>Check All</source>
        <translation>Sélectionner tout</translation>
    </message>
    <message>
        <location filename="UI/wColumns2Use.py" line="67"/>
        <source>Uncheck All</source>
        <translation>Déselectionner tout</translation>
    </message>
</context>
<context>
    <name>ColumnsSelection</name>
    <message>
        <location filename="UI/wColumnsSelection.py" line="86"/>
        <source>Columns Selection</source>
        <translation>Sélection des colonnes</translation>
    </message>
    <message>
        <location filename="UI/wColumnsSelection.py" line="118"/>
        <source>Excel</source>
        <translation type="obsolete">Excel</translation>
    </message>
    <message>
        <location filename="UI/wColumnsSelection.py" line="119"/>
        <source>Select Excel File</source>
        <translation type="obsolete">Sélectionner un Fichier Excel</translation>
    </message>
    <message>
        <location filename="UI/wColumnsSelection.py" line="120"/>
        <source>Sheet</source>
        <translation type="obsolete">Page</translation>
    </message>
    <message>
        <location filename="UI/wColumnsSelection.py" line="124"/>
        <source>Section column</source>
        <translation type="obsolete">Colonne Section</translation>
    </message>
    <message>
        <location filename="UI/wColumnsSelection.py" line="122"/>
        <source>Distance column</source>
        <translation type="obsolete">Colonne Distance</translation>
    </message>
    <message>
        <location filename="UI/wColumnsSelection.py" line="123"/>
        <source>QRame data</source>
        <translation type="obsolete">Données QRame</translation>
    </message>
    <message>
        <location filename="UI/wColumnsSelection.py" line="87"/>
        <source>Import sheet</source>
        <translation>Importer la Page</translation>
    </message>
    <message>
        <location filename="UI/wColumnsSelection.py" line="88"/>
        <source>Import column</source>
        <translation>Importer la colonne</translation>
    </message>
    <message>
        <location filename="UI/wColumnsSelection.py" line="89"/>
        <source>QRame column</source>
        <translation>Colonne QRame</translation>
    </message>
    <message>
        <location filename="UI/wColumnsSelection.py" line="91"/>
        <source>Import</source>
        <translation>Importer</translation>
    </message>
    <message>
        <location filename="UI/wColumnsSelection.py" line="93"/>
        <source>QRame</source>
        <translation>QRame</translation>
    </message>
    <message>
        <location filename="UI/wColumnsSelection.py" line="94"/>
        <source>Remove Column</source>
        <translation>Supprimer la Colonne</translation>
    </message>
    <message>
        <location filename="UI/wColumnsSelection.py" line="95"/>
        <source>Add Column</source>
        <translation>Ajouter la Colonne</translation>
    </message>
</context>
<context>
    <name>IntercomparisonSettings</name>
    <message>
        <location filename="UI/wIntercomparisonSettings.py" line="294"/>
        <source>Settings</source>
        <translation type="obsolete">Paramètres</translation>
    </message>
    <message>
        <location filename="UI/wIntercomparisonSettings.py" line="340"/>
        <source>Intercomparison process</source>
        <translation>Processus d&apos;intercomparaison</translation>
    </message>
    <message>
        <location filename="UI/wIntercomparisonSettings.py" line="341"/>
        <source>Reference discharge</source>
        <translation>Débit de référence</translation>
    </message>
    <message>
        <location filename="UI/wIntercomparisonSettings.py" line="183"/>
        <source>Nb. of transects by meas.</source>
        <translation type="obsolete">Nb. de transects par mesure</translation>
    </message>
    <message>
        <location filename="UI/wIntercomparisonSettings.py" line="346"/>
        <source>Bias uncertainty (%)</source>
        <translation>Incertitude du biais (%)</translation>
    </message>
    <message>
        <location filename="UI/wIntercomparisonSettings.py" line="339"/>
        <source>Apply intercomparison calculations</source>
        <translation>Appliquer le calcul d&apos;intercomparaison</translation>
    </message>
    <message>
        <location filename="UI/wIntercomparisonSettings.py" line="343"/>
        <source>Ref. Q uncertainty (%)</source>
        <translation>Incertitude du Q de référence</translation>
    </message>
    <message>
        <location filename="UI/wIntercomparisonSettings.py" line="344"/>
        <source>Max. permissible deviation (%)</source>
        <translation>Écart max toléré (%)</translation>
    </message>
    <message>
        <location filename="UI/wIntercomparisonSettings.py" line="345"/>
        <source>Nb. of transects by meas. (N)</source>
        <translation>Nb. de transects par mesure (N)</translation>
    </message>
    <message>
        <location filename="UI/wIntercomparisonSettings.py" line="348"/>
        <source>Grouped analysis</source>
        <translation>Analyse groupée</translation>
    </message>
    <message>
        <location filename="UI/wIntercomparisonSettings.py" line="349"/>
        <source>Selected column</source>
        <translation>Colonne sélectionnée</translation>
    </message>
    <message>
        <location filename="UI/wIntercomparisonSettings.py" line="304"/>
        <source>Double effect</source>
        <translation type="obsolete">Double effet</translation>
    </message>
    <message>
        <location filename="UI/wIntercomparisonSettings.py" line="353"/>
        <source>Section label</source>
        <translation>Nom des sections</translation>
    </message>
    <message>
        <location filename="UI/wIntercomparisonSettings.py" line="354"/>
        <source>Team label</source>
        <translation>Nom des équipes</translation>
    </message>
    <message>
        <location filename="UI/wIntercomparisonSettings.py" line="355"/>
        <source>Nb. sections (S)</source>
        <translation>Nb. de sections (S)</translation>
    </message>
    <message>
        <location filename="UI/wIntercomparisonSettings.py" line="356"/>
        <source>Nb. teams (P)</source>
        <translation>Nb. d&apos;équipes(P)</translation>
    </message>
    <message>
        <location filename="UI/wIntercomparisonSettings.py" line="338"/>
        <source>Intercomparison</source>
        <translation>Intercomparaison</translation>
    </message>
    <message>
        <location filename="UI/wIntercomparisonSettings.py" line="347"/>
        <source>Auto</source>
        <translation>Auto</translation>
    </message>
    <message>
        <location filename="UI/wIntercomparisonSettings.py" line="352"/>
        <source>Second factor</source>
        <translation>A deux facteurs</translation>
    </message>
    <message>
        <location filename="UI/wIntercomparisonSettings.py" line="350"/>
        <source>Correct discharge</source>
        <translation>Correction du débit</translation>
    </message>
    <message>
        <location filename="UI/wIntercomparisonSettings.py" line="351"/>
        <source>Correct discharge by LOWESS interpolation</source>
        <translation>Corriger le débit par interpolation LOWESS</translation>
    </message>
    <message>
        <location filename="UI/wIntercomparisonSettings.py" line="363"/>
        <source>Distances Selection</source>
        <translation type="obsolete">Spécifier Distances</translation>
    </message>
</context>
<context>
    <name>Main</name>
    <message>
        <location filename="UI/main.py" line="361"/>
        <source>Open File</source>
        <translation type="obsolete">Ouvrir un fichier</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="361"/>
        <source>pickle File (*.pickle)</source>
        <translation type="obsolete">Fichier pickle (*.pickle)</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="987"/>
        <source>No valid ADCP measurement</source>
        <translation>Pas de mesures ADCP valides</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="1547"/>
        <source>Median Q</source>
        <translation type="obsolete">Q médian</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2054"/>
        <source>Mean U(Q) [Oursin]</source>
        <translation>U(Q) moyen [Oursin]</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="747"/>
        <source>U(Q) [empirical]</source>
        <translation type="obsolete">U(Q) [empirique]</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="748"/>
        <source>U(Q) min [empirical]</source>
        <translation type="obsolete">U(Q) min [empirique]</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="748"/>
        <source>U(Q) max [empirical]</source>
        <translation type="obsolete">U(Q) max [empirique]</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="749"/>
        <source>Repeatability sr  [empirical]</source>
        <translation type="obsolete">Répétabilité sr [empirique]</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="750"/>
        <source>Interlaboratory sL [empirical]</source>
        <translation type="obsolete">Interlaboratoire sL [empirique]</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="751"/>
        <source>Reproductilibty sR [empirical]</source>
        <translation type="obsolete">Reproductibilité sr [empirique]</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="752"/>
        <source>Ar [empirical]</source>
        <translation type="obsolete">Ar [empirique]</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="752"/>
        <source>AR [empirical]</source>
        <translation type="obsolete">AR [empirique]</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2656"/>
        <source>Manufacturer</source>
        <translation>Constructeur</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2657"/>
        <source>Model</source>
        <translation>Modèle</translation>
    </message>
    <message>
        <location filename="UI/FigDischargeCorrection.py" line="118"/>
        <source>Discharge</source>
        <translation>Débit</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2662"/>
        <source>Deviation to ref.</source>
        <translation>Écart à la référence</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="1277"/>
        <source>Score</source>
        <translation type="obsolete">Score</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2666"/>
        <source>Std discharge</source>
        <translation>E-T des débits</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2667"/>
        <source>Mean V</source>
        <translation>Vitesse moyenne</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2668"/>
        <source>Nb. transects</source>
        <translation>Nb. transects</translation>
    </message>
    <message>
        <location filename="UI/Measurements2Use.py" line="120"/>
        <source>Start time</source>
        <translation>Heure de début</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2674"/>
        <source>End time</source>
        <translation>Heure de fin</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2681"/>
        <source>Top law</source>
        <translation>Loi en surface</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2682"/>
        <source>Bot law</source>
        <translation>Loi au fond</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2683"/>
        <source>Exponent</source>
        <translation>Exposant</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2685"/>
        <source>Coefficient</source>
        <translation>Coefficient</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2665"/>
        <source>U Total</source>
        <translation>U Totale</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2686"/>
        <source>u System</source>
        <translation>u systématique</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2687"/>
        <source>u Compass</source>
        <translation>u compas</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2688"/>
        <source>u MB</source>
        <translation>u Fond-mobile</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2689"/>
        <source>u Ensembles</source>
        <translation>u Verticales</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2690"/>
        <source>u Meas Q</source>
        <translation>u Q mesuré</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2691"/>
        <source>u Invalid data</source>
        <translation>u Données invalides</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2692"/>
        <source>u Top Q</source>
        <translation>u Q Surface</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2693"/>
        <source>u Bottom Q</source>
        <translation>u Q Fond</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2694"/>
        <source>u Left Q</source>
        <translation>u Q RG</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2695"/>
        <source>u Right Q</source>
        <translation>u Q RD</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2696"/>
        <source>u CV</source>
        <translation>u CV</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2697"/>
        <source>Bottom Q</source>
        <translation>Q Fond</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2698"/>
        <source>Top Q</source>
        <translation>Q Surface</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2699"/>
        <source>Mid Q</source>
        <translation>Q Mesuré</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2700"/>
        <source>Left Q</source>
        <translation>Q RG</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2701"/>
        <source>Right Q</source>
        <translation>Q RD</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="3079"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="3083"/>
        <source>Permission denied : check if csv file is not already open.</source>
        <translation>Autorisation refusée : vérifiez si le fichier csv n&apos;est pas déjà ouvert.</translation>
    </message>
    <message>
        <location filename="UI/Measurements2Use.py" line="117"/>
        <source>Select Measurement</source>
        <translation>Mesures sélectionnées</translation>
    </message>
    <message>
        <location filename="UI/Measurements2Use.py" line="118"/>
        <source>Cochran test</source>
        <translation>Test de Cochran</translation>
    </message>
    <message>
        <location filename="UI/Measurements2Use.py" line="118"/>
        <source>Grubbs test</source>
        <translation>Test de Grubbs</translation>
    </message>
    <message>
        <location filename="UI/Measurements2Use.py" line="192"/>
        <source>Invalid</source>
        <translation>Invalide</translation>
    </message>
    <message>
        <location filename="UI/Measurements2Use.py" line="186"/>
        <source>Valid</source>
        <translation>Valide</translation>
    </message>
    <message>
        <location filename="UI/Measurements2Use.py" line="229"/>
        <source>None</source>
        <translation>Aucun</translation>
    </message>
    <message>
        <location filename="Classes/LoadMeasurements.py" line="63"/>
        <source>Loading data...</source>
        <translation>Chargement des données...</translation>
    </message>
    <message>
        <location filename="Classes/LoadMeasurements.py" line="77"/>
        <source>Extracting data...</source>
        <translation type="obsolete">Extraction des données...</translation>
    </message>
    <message>
        <location filename="Classes/LoadMeasurements.py" line="78"/>
        <source>Open measurement </source>
        <translation type="obsolete">Ouverture de la mesure </translation>
    </message>
    <message>
        <location filename="Classes/LoadMeasurements.py" line="207"/>
        <source>Update measurement </source>
        <translation>Mise à jour de la mesure </translation>
    </message>
    <message>
        <location filename="UI/progress_bar.py" line="45"/>
        <source>Processing...</source>
        <translation type="obsolete">Traitement des données...</translation>
    </message>
    <message>
        <location filename="UI/ProgressBar.py" line="55"/>
        <source>Stop extraction</source>
        <translation>Arrêter l&apos;extraction</translation>
    </message>
    <message>
        <location filename="UI/ProgressBar.py" line="56"/>
        <source>You are about to cancel ADCP measurements extraction. Are you sure ?</source>
        <translation>Vous êtes sur le point d&apos;arrêter l&apos;extraction des mesures ADCP. Êtes-vous sûr ?</translation>
    </message>
    <message>
        <location filename="UI/SelectDialog.py" line="46"/>
        <source>Select Directory</source>
        <translation>Sélectionner un dossier</translation>
    </message>
    <message>
        <location filename="UI/SelectDialog.py" line="103"/>
        <source>Default</source>
        <translation type="obsolete">Par défaut</translation>
    </message>
    <message>
        <location filename="UI/SelectDialog.py" line="105"/>
        <source>Bottom track</source>
        <translation type="obsolete">Suivi de fond</translation>
    </message>
    <message>
        <location filename="UI/ProgressBar.py" line="63"/>
        <source>Yes</source>
        <translation>Oui</translation>
    </message>
    <message>
        <location filename="UI/SelectDialog.py" line="110"/>
        <source>No</source>
        <translation type="obsolete">Non</translation>
    </message>
    <message>
        <location filename="UI/SelectDialog.py" line="94"/>
        <source>Confirm settings</source>
        <translation type="obsolete">Confirmer les paramètres</translation>
    </message>
    <message>
        <location filename="UI/SelectDialog.py" line="94"/>
        <source>You are about to open ADCP measurements which can be a long process, 
please confirm the following settings : 
- Navigation reference : </source>
        <translation type="obsolete">Vous êtes sur le point d&apos;ouvrir des données ADCP ce qui peut être un processus long. Veuillez confirmer les paramètres suivant :
- Référence de navigation : </translation>
    </message>
    <message>
        <location filename="UI/SelectDialog.py" line="94"/>
        <source>
- Q Weighted extrap : </source>
        <translation type="obsolete">
- Débit extrapolé pondéré : </translation>
    </message>
    <message>
        <location filename="UI/SelectDialog.py" line="115"/>
        <source>Invalid data : select a folder which contains ADCP 
folders</source>
        <translation type="obsolete">Données invalides : Sélectionnez un dossier qui contient des fichiers de mesures ADCP.</translation>
    </message>
    <message>
        <location filename="UI/SelectDialog.py" line="256"/>
        <source>Save File as Pickle</source>
        <translation>Sauvegarder le fichier comme Pickle</translation>
    </message>
    <message>
        <location filename="UI/SelectDialog.py" line="260"/>
        <source>Export data as csv</source>
        <translation>Exporter les données en CSV</translation>
    </message>
    <message>
        <location filename="UI/SelectDialog.py" line="264"/>
        <source>Export for BaRatinAGE</source>
        <translation>Export pour BaRatinAGE</translation>
    </message>
    <message>
        <location filename="UI/SelectDialog.py" line="268"/>
        <source>Export for Bareme</source>
        <translation>Export pour Barème</translation>
    </message>
    <message>
        <location filename="UI/SelectDialog.py" line="272"/>
        <source>Save figure</source>
        <translation>Sauvegarder la figure</translation>
    </message>
    <message>
        <location filename="UI/Columns2Use.py" line="79"/>
        <source>Selected columns</source>
        <translation>Sélection des colonnes</translation>
    </message>
    <message>
        <location filename="UI/FigDischargeUncertainty.py" line="81"/>
        <source>Empirical uncertainty
and its uncertainty</source>
        <translation>Incertitude empirique et
son incertitude associée</translation>
    </message>
    <message>
        <location filename="UI/FigDischargeUncertainty.py" line="77"/>
        <source>Mean discharge</source>
        <translation type="obsolete">Débit moyen</translation>
    </message>
    <message>
        <location filename="UI/FigDischargeUncertainty.py" line="87"/>
        <source>Discharge and
uncertainty</source>
        <translation>Débit et son incertitude
associée</translation>
    </message>
    <message>
        <location filename="UI/FigDischargeUncertainty.py" line="80"/>
        <source>OURSIN mean</source>
        <translation type="obsolete">OURSIN moyen</translation>
    </message>
    <message>
        <location filename="UI/FigDischargeCorrection.py" line="117"/>
        <source>Time</source>
        <translation>Temps</translation>
    </message>
    <message>
        <location filename="UI/FigUncertaintySources.py" line="74"/>
        <source>Systematic</source>
        <translation>Systématique</translation>
    </message>
    <message>
        <location filename="UI/FigUncertaintySources.py" line="74"/>
        <source>Compass</source>
        <translation>Compas</translation>
    </message>
    <message>
        <location filename="UI/FigUncertaintySources.py" line="75"/>
        <source>Moving-bed</source>
        <translation>Fond-mobile</translation>
    </message>
    <message>
        <location filename="UI/FigUncertaintySources.py" line="75"/>
        <source>Measured</source>
        <translation>Mesuré</translation>
    </message>
    <message>
        <location filename="UI/FigUncertaintySources.py" line="76"/>
        <source>Nb of ens.</source>
        <translation>Nb. de verticales</translation>
    </message>
    <message>
        <location filename="UI/FigUncertaintySources.py" line="76"/>
        <source>Invalid boat</source>
        <translation>Support invalide</translation>
    </message>
    <message>
        <location filename="UI/FigUncertaintySources.py" line="77"/>
        <source>Invalid depth</source>
        <translation>Fond invalide</translation>
    </message>
    <message>
        <location filename="UI/FigUncertaintySources.py" line="77"/>
        <source>Invalid water</source>
        <translation>Mesure invalide</translation>
    </message>
    <message>
        <location filename="UI/FigUncertaintySources.py" line="78"/>
        <source>Top</source>
        <translation>Surface</translation>
    </message>
    <message>
        <location filename="UI/FigUncertaintySources.py" line="78"/>
        <source>Bottom</source>
        <translation>Fond</translation>
    </message>
    <message>
        <location filename="UI/FigUncertaintySources.py" line="79"/>
        <source>Left</source>
        <translation>RG</translation>
    </message>
    <message>
        <location filename="UI/FigUncertaintySources.py" line="79"/>
        <source>Right</source>
        <translation>RD</translation>
    </message>
    <message>
        <location filename="UI/FigUncertaintySources.py" line="80"/>
        <source>Coeff. of var.</source>
        <translation>Coef. de var.</translation>
    </message>
    <message>
        <location filename="UI/FigUncertaintySources.py" line="157"/>
        <source>Uncertainty</source>
        <translation>Incertitude</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2660"/>
        <source>Nav. Ref.</source>
        <translation>Nav. Ref.</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2684"/>
        <source>Alpha</source>
        <translation>Alpha</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="3235"/>
        <source>If you haven&apos;t saved your data, changes will be lost. 
 Are you sure you want to Close? </source>
        <translation>Si vous n&apos;avez pas sauvegardé vos données, elles seront perdues.
Êtes vous sûr de vouloir fermer ?</translation>
    </message>
    <message>
        <location filename="UI/SelectDialog.py" line="125"/>
        <source>You are about to open ADCP measurements which can be a long process. Please make sure that you select proper settings.</source>
        <translation>Vous êtes sur le point d&apos;ouvrir l&apos;ensemble des mesures ADCP. Veuillez vérifier que vous avez sélectionné les paramètres adéquats.</translation>
    </message>
    <message>
        <location filename="UI/SelectDialog.py" line="147"/>
        <source>Invalid data : please select a folder which contains ADCP folders</source>
        <translation>Données invalides : Veuillez sélectionner un dossier qui contient des dossiers de mesures ADCP.</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="1885"/>
        <source>Optimized</source>
        <translation>Optimisée</translation>
    </message>
    <message>
        <location filename="Classes/LoadMeasurements.py" line="139"/>
        <source>Open measurement... </source>
        <translation>Ouverture des mesures...</translation>
    </message>
    <message>
        <location filename="UI/SelectDialog.py" line="124"/>
        <source>Open Measurements</source>
        <translation>Lecture des mesures</translation>
    </message>
    <message>
        <location filename="UI/SelectDialog.py" line="135"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="UI/SelectDialog.py" line="134"/>
        <source>Open</source>
        <translation>Ouvrir</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="3234"/>
        <source>Close QRame</source>
        <translation>Fermer QRame</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="362"/>
        <source>Error encountered on measurements : </source>
        <translation type="obsolete">Une erreur a été rencontrée avec les mesures : </translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2675"/>
        <source>Width</source>
        <translation>Largeur</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2676"/>
        <source>Area</source>
        <translation>Aire</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2677"/>
        <source>Depth</source>
        <translation>Profondeur</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2680"/>
        <source>Perimeter</source>
        <translation>Périmètre mouillé</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="930"/>
        <source>Error encountered on measurement(s) : </source>
        <translation>Erreur rencontrée sur les mesures : </translation>
    </message>
    <message>
        <location filename="UI/main.py" line="550"/>
        <source>  is already open. Opening it will overwrite the previous measure. Please confirm or rename the folder.</source>
        <translation> est déjà ouverte. Vous êtes sur le point d&apos;écraser la mesure précédente. Merci de confirmer ou de renommer le dossier.</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2108"/>
        <source>Empirical : </source>
        <translation>Empirique : </translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2112"/>
        <source>Repeatability sr</source>
        <translation>Répétabilité sr</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2097"/>
        <source>Interlaboratory sL</source>
        <translation>Interlaboratoire sL</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2112"/>
        <source>Reproductilibty sR</source>
        <translation>Reproductibilité sR</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2728"/>
        <source>Ar</source>
        <translation></translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2729"/>
        <source>AR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2664"/>
        <source>Normalized deviation</source>
        <translation>Écart normalisé</translation>
    </message>
    <message>
        <location filename="UI/FigDischargeCorrection.py" line="100"/>
        <source>Reference discharge</source>
        <translation>Débit de référence</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2112"/>
        <source>Section sS</source>
        <translation>Section sS</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2112"/>
        <source>Participant sP</source>
        <translation>Équipe sP</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2112"/>
        <source>S-P interaction sSP</source>
        <translation>Interaction S-P sSP</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2191"/>
        <source>Min. Q</source>
        <translation>Débit min.</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2191"/>
        <source>Max. Q</source>
        <translation>Débit max.</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2663"/>
        <source>Z-Score</source>
        <translation>Z-Score</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2722"/>
        <source>Empirical U(Q, N)</source>
        <translation>U(Q, N) empirique</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2723"/>
        <source>Empirical min. U(Q, N)</source>
        <translation>U(Q, N) min. empirique</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2724"/>
        <source>Empirical max. U(Q, N)</source>
        <translation>U(Q, N) max. empirique</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2712"/>
        <source>sS</source>
        <translation>sS</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2713"/>
        <source>sP</source>
        <translation>sP</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2714"/>
        <source>sSP</source>
        <translation>sSP</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2726"/>
        <source>sr</source>
        <translation>sr</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2727"/>
        <source>sR</source>
        <translation>sR</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2725"/>
        <source>sL</source>
        <translation>sL</translation>
    </message>
    <message>
        <location filename="UI/FigDischargeUncertainty.py" line="89"/>
        <source>Median discharge
and uncertainty</source>
        <translation type="obsolete">Débit et incertitude
médians</translation>
    </message>
    <message>
        <location filename="UI/FigDischargeUncertainty.py" line="98"/>
        <source>Discharge distribution
on the section</source>
        <translation type="obsolete">Répartition des débits sur la section</translation>
    </message>
    <message>
        <location filename="UI/FigUncertaintySources.py" line="146"/>
        <source>Empirical uncertainty</source>
        <translation>Incertitude empirique</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2053"/>
        <source>Standard Deviation Q</source>
        <translation>Écart-type Q</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="276"/>
        <source>Change scale</source>
        <translation>Changer l&apos;échelle</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2112"/>
        <source>Mean U(Q)</source>
        <translation>U(Q) moyenne</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2097"/>
        <source>Mean U(Q) min</source>
        <translation>U(Q) min. moyenne</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2097"/>
        <source>Mean U(Q) max</source>
        <translation>U(Q) max. moyenne</translation>
    </message>
    <message>
        <location filename="UI/FigDischargeDistribution.py" line="81"/>
        <source>Discharge distribution</source>
        <translation>Distribution des débits</translation>
    </message>
    <message>
        <location filename="UI/FigDischargeDistribution.py" line="86"/>
        <source>Global discharge
distribution</source>
        <translation>Distribution globale</translation>
    </message>
    <message>
        <location filename="UI/FigDischargeUncertainty.py" line="89"/>
        <source>Medians</source>
        <translation>Médianes</translation>
    </message>
    <message>
        <location filename="UI/Measurements2Use.py" line="189"/>
        <source>Isolated</source>
        <translation>Isolée</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2659"/>
        <source>Serial Numb.</source>
        <translation>Num. de Série</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2741"/>
        <source>Comments</source>
        <translation>Commentaires</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2740"/>
        <source>User Label</source>
        <translation>Label Utilisateur</translation>
    </message>
    <message>
        <location filename="Classes/LoadMeasurements.py" line="81"/>
        <source>Data formatting... </source>
        <translation>Mise en forme des données... </translation>
    </message>
    <message>
        <location filename="Classes/LoadMeasurements.py" line="91"/>
        <source>Finish... </source>
        <translation>Finalisation... </translation>
    </message>
    <message>
        <location filename="Classes/LoadMeasurements.py" line="99"/>
        <source>Loading pickle..</source>
        <translation>Chargement du pickle...</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="676"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="1058"/>
        <source>LOWESS correction can not be computed.</source>
        <translation>La correction LOWESS n&apos;a pas pu être calculée.</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2052"/>
        <source>Mean Q</source>
        <translation>Q Moyen</translation>
    </message>
    <message>
        <location filename="UI/FigDischargeDistribution.py" line="94"/>
        <source>Unknown</source>
        <translation>Inconnu</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2658"/>
        <source>Tool</source>
        <translation>Appareil</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="2678"/>
        <source>Excluded Dist.</source>
        <translation>Dist. Exclue</translation>
    </message>
    <message>
        <location filename="UI/FigDischargeDistribution.py" line="147"/>
        <source>Mean value</source>
        <translation>Valeur moyenne</translation>
    </message>
    <message>
        <location filename="UI/FigDischargeCorrection.py" line="84"/>
        <source>Transect discharge</source>
        <translation>Débit des transects</translation>
    </message>
    <message>
        <location filename="UI/FigDischargeCorrection.py" line="88"/>
        <source>Lowess corrected discharge</source>
        <translation>Débit corrigé Lowess</translation>
    </message>
    <message>
        <location filename="UI/FigDischargeCorrection.py" line="96"/>
        <source>Lowess interpolation</source>
        <translation>Interpolation Lowess</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="269"/>
        <source>Rename</source>
        <translation>Renommer</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="270"/>
        <source>Update</source>
        <translation>Mettre à jour</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="271"/>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="291"/>
        <source>Update name</source>
        <translation>Changer le nom</translation>
    </message>
    <message>
        <location filename="UI/Measurements2Use.py" line="220"/>
        <source>Good</source>
        <translation>Bien</translation>
    </message>
    <message>
        <location filename="UI/Measurements2Use.py" line="223"/>
        <source>Caution</source>
        <translation>Vigilance</translation>
    </message>
    <message>
        <location filename="UI/Measurements2Use.py" line="226"/>
        <source>Warning</source>
        <translation>Avertissement</translation>
    </message>
    <message>
        <location filename="UI/Measurements2Use.py" line="119"/>
        <source>Nav. Quality</source>
        <translation>Nav. Qualité</translation>
    </message>
    <message>
        <location filename="UI/Measurements2Use.py" line="119"/>
        <source>Water Quality</source>
        <translation>Mesure Qualité</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="305"/>
        <source>Measurement name is already present. Please change it first.</source>
        <translation>Le nom de mesure est déjà présent. Merci de le changer d&apos;abord.</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="UI/main_window.py" line="209"/>
        <source>Discharge and uncertainty</source>
        <translation type="obsolete">Débit et incertitude</translation>
    </message>
    <message>
        <location filename="UI/main_window.py" line="210"/>
        <source>Discharge time</source>
        <translation type="obsolete">Débits chronologiques</translation>
    </message>
    <message>
        <location filename="UI/main_window.py" line="247"/>
        <source>Uncertainty sources</source>
        <translation>Sources d&apos;incertitudes</translation>
    </message>
    <message>
        <location filename="UI/main_window.py" line="252"/>
        <source>File</source>
        <translation>Fichier</translation>
    </message>
    <message>
        <location filename="UI/main_window.py" line="260"/>
        <source>Open</source>
        <translation>Ouvrir</translation>
    </message>
    <message>
        <location filename="UI/main_window.py" line="254"/>
        <source>ADCP data</source>
        <translation>Données ADCP</translation>
    </message>
    <message>
        <location filename="UI/main_window.py" line="256"/>
        <source>Previous results</source>
        <translation>Résultats enregistrés</translation>
    </message>
    <message>
        <location filename="UI/main_window.py" line="258"/>
        <source>Export current data as .csv</source>
        <translation>Exporter les données en .CSV</translation>
    </message>
    <message>
        <location filename="UI/main_window.py" line="261"/>
        <source>Open ADCP data (select folder)</source>
        <translation>Ouvrir les données ADCP (sélectionner un dossier)</translation>
    </message>
    <message>
        <location filename="UI/main_window.py" line="262"/>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="UI/main_window.py" line="263"/>
        <source>Current settings</source>
        <translation>Paramètres actuels</translation>
    </message>
    <message>
        <location filename="UI/main_window.py" line="264"/>
        <source>Checked</source>
        <translation>Sélectionnés</translation>
    </message>
    <message>
        <location filename="UI/main_window.py" line="265"/>
        <source>Selected measurement</source>
        <translation>Mesures sélectionnées</translation>
    </message>
    <message>
        <location filename="UI/main_window.py" line="267"/>
        <source>Reset zoom/pan of graph</source>
        <translation>Réinitialise le zoom/déplcament</translation>
    </message>
    <message>
        <location filename="UI/main_window.py" line="269"/>
        <source>Zoom</source>
        <translation>Zoom</translation>
    </message>
    <message>
        <location filename="UI/main_window.py" line="271"/>
        <source>Move graph</source>
        <translation>Déplacer</translation>
    </message>
    <message>
        <location filename="UI/main_window.py" line="272"/>
        <source>Table</source>
        <translation>Tableau</translation>
    </message>
    <message>
        <location filename="UI/main_window.py" line="273"/>
        <source>Select table&apos;s columns</source>
        <translation>Sélectionner une colonne</translation>
    </message>
    <message>
        <location filename="UI/main_window.py" line="274"/>
        <source>Export for Bareme (.dat)</source>
        <translation>Export pour Barème (.dat)</translation>
    </message>
    <message>
        <location filename="UI/main_window.py" line="275"/>
        <source>Export for BaRatinAGE (.csv, .BAD)</source>
        <translation>Export pour BaRatinAGE (.csv, .BAD)</translation>
    </message>
    <message>
        <location filename="UI/main_window.py" line="276"/>
        <source>Save to open later (.pickle)</source>
        <translation>Sauvegarder les mesures (.pickle)</translation>
    </message>
    <message>
        <location filename="UI/main_window.py" line="277"/>
        <source>Import HYDRO2 file</source>
        <translation>Importer un fichier HYDRO2</translation>
    </message>
    <message>
        <location filename="UI/main_window.py" line="257"/>
        <source>Ctrl+R</source>
        <translation></translation>
    </message>
    <message>
        <location filename="UI/main_window.py" line="259"/>
        <source>Ctrl+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="UI/main_window.py" line="255"/>
        <source>Ctrl+O</source>
        <translation></translation>
    </message>
    <message>
        <location filename="UI/main_window.py" line="266"/>
        <source>HomeGraph</source>
        <translation>Réinitialisation graphique</translation>
    </message>
    <message>
        <location filename="UI/main_window.py" line="270"/>
        <source>PanGraph</source>
        <translation>Déplacer</translation>
    </message>
    <message>
        <location filename="UI/main_window.py" line="278"/>
        <source>Intercomparison</source>
        <translation>Intercomparaison</translation>
    </message>
    <message>
        <location filename="UI/main_window.py" line="279"/>
        <source>Intercomparison settings</source>
        <translation>Paramètres d&apos;intercomparaison</translation>
    </message>
    <message>
        <location filename="UI/main_window.py" line="280"/>
        <source>AddMeasurement</source>
        <translation></translation>
    </message>
    <message>
        <location filename="UI/main_window.py" line="281"/>
        <source>Add measurement</source>
        <translation>Ajouter une mesure</translation>
    </message>
    <message>
        <location filename="UI/main_window.py" line="245"/>
        <source>Uncertainty results</source>
        <translation>Résultats d&apos;incertitude</translation>
    </message>
    <message>
        <location filename="UI/main_window.py" line="246"/>
        <source>Discharge over time</source>
        <translation>Chronologie des débits</translation>
    </message>
    <message>
        <location filename="UI/main_window.py" line="250"/>
        <source>Discharge distribution</source>
        <translation>Distribution des débits</translation>
    </message>
    <message>
        <location filename="UI/main_window.py" line="251"/>
        <source>Discharge correction</source>
        <translation>Correction des Débits</translation>
    </message>
    <message>
        <location filename="UI/main_window.py" line="282"/>
        <source>Ctrl+F</source>
        <translation>Ctrl+F</translation>
    </message>
    <message>
        <location filename="UI/main_window.py" line="248"/>
        <source>Violin</source>
        <translation>Violons</translation>
    </message>
    <message>
        <location filename="UI/main_window.py" line="249"/>
        <source># Transects</source>
        <translation># Répétitions</translation>
    </message>
</context>
<context>
    <name>Measurements2Use</name>
    <message>
        <location filename="UI/wMeasurements2Use.py" line="62"/>
        <source>Measurements to Use</source>
        <translation>Mesures utilisées</translation>
    </message>
    <message>
        <location filename="UI/wMeasurements2Use.py" line="63"/>
        <source>Check All</source>
        <translation>Sélectionner tout</translation>
    </message>
    <message>
        <location filename="UI/wMeasurements2Use.py" line="64"/>
        <source>Uncheck Invalid</source>
        <translation>Désélectionner invalides</translation>
    </message>
</context>
<context>
    <name>Options</name>
    <message>
        <location filename="UI/wOptions.py" line="280"/>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="UI/wOptions.py" line="281"/>
        <source>Language</source>
        <translation>Langue</translation>
    </message>
    <message>
        <location filename="UI/wOptions.py" line="286"/>
        <source>English</source>
        <translation>Anglais</translation>
    </message>
    <message>
        <location filename="UI/wOptions.py" line="283"/>
        <source>French</source>
        <translation>Français</translation>
    </message>
    <message>
        <location filename="UI/wOptions.py" line="284"/>
        <source>Units</source>
        <translation>Unités</translation>
    </message>
    <message>
        <location filename="UI/wOptions.py" line="285"/>
        <source>SI</source>
        <translation>SI</translation>
    </message>
    <message>
        <location filename="UI/wOptions.py" line="287"/>
        <source>Navigation Reference</source>
        <translation>Navigation de référence</translation>
    </message>
    <message>
        <location filename="UI/wOptions.py" line="288"/>
        <source>Default</source>
        <translation>Par défaut</translation>
    </message>
    <message>
        <location filename="UI/wOptions.py" line="289"/>
        <source>Bottom track</source>
        <translation>Suivi de fond</translation>
    </message>
    <message>
        <location filename="UI/wOptions.py" line="291"/>
        <source>Moving Bed</source>
        <translation>Fond-mobile</translation>
    </message>
    <message>
        <location filename="UI/wOptions.py" line="292"/>
        <source>Observed no moving-bed</source>
        <translation>Absence de fond-mobile observée</translation>
    </message>
    <message>
        <location filename="UI/wOptions.py" line="293"/>
        <source>Extrapolation</source>
        <translation>Extrapolation</translation>
    </message>
    <message>
        <location filename="UI/wOptions.py" line="296"/>
        <source>Exponent</source>
        <translation>Exposant</translation>
    </message>
    <message>
        <location filename="UI/wOptions.py" line="300"/>
        <source>Discharge weighted</source>
        <translation>Débit pondéré</translation>
    </message>
    <message>
        <location filename="wOptions.py" line="284"/>
        <source>Force extrapolation method</source>
        <translation type="obsolete">Forcer la méthode d&apos;extrapolation</translation>
    </message>
    <message>
        <location filename="UI/wOptions.py" line="302"/>
        <source>Power - Power</source>
        <translation>Puissance - Puissance</translation>
    </message>
    <message>
        <location filename="UI/wOptions.py" line="303"/>
        <source>Constant - No Slip</source>
        <translation>Constant - Sans glissement</translation>
    </message>
    <message>
        <location filename="UI/wOptions.py" line="304"/>
        <source>3-Point - No Slip</source>
        <translation>3-Points - Sans glissement</translation>
    </message>
    <message>
        <location filename="UI/wOptions.py" line="331"/>
        <source>Intercomparison</source>
        <translation type="obsolete">Intercomparaison</translation>
    </message>
    <message>
        <location filename="UI/wOptions.py" line="332"/>
        <source>Tolerated deviation (%)</source>
        <translation type="obsolete">Écart max toléré</translation>
    </message>
    <message>
        <location filename="UI/wOptions.py" line="333"/>
        <source>Nb. of transects by meas.</source>
        <translation type="obsolete">Nb. de transects par mesure</translation>
    </message>
    <message>
        <location filename="UI/wOptions.py" line="301"/>
        <source>Automatic</source>
        <translation>Automatique</translation>
    </message>
    <message>
        <location filename="UI/wOptions.py" line="297"/>
        <source>Discharge</source>
        <translation>Débit</translation>
    </message>
    <message>
        <location filename="UI/wOptions.py" line="298"/>
        <source>Velocity</source>
        <translation>Vitesse</translation>
    </message>
    <message>
        <location filename="UI/wOptions.py" line="294"/>
        <source>Law</source>
        <translation>Loi</translation>
    </message>
    <message>
        <location filename="UI/wOptions.py" line="295"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="UI/main.py" line="1901"/>
        <source>Optimized</source>
        <translation>Optimisée</translation>
    </message>
    <message>
        <location filename="UI/wOptions.py" line="299"/>
        <source>Subsection (% L to R)</source>
        <translation>Sous-section (% RG à RD)</translation>
    </message>
    <message>
        <location filename="UI/wOptions.py" line="290"/>
        <source>GPS if available</source>
        <translation>GPS si disponible</translation>
    </message>
    <message>
        <location filename="UI/wOptions.py" line="305"/>
        <source>Excluded Dist.</source>
        <translation>Dist. Exclue</translation>
    </message>
</context>
<context>
    <name>ProgressBar</name>
    <message>
        <location filename="UI/wProgressBar.py" line="57"/>
        <source>Loading Data</source>
        <translation>Chargement des données</translation>
    </message>
    <message>
        <location filename="UI/wProgressBar.py" line="58"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="UI/wProgressBar.py" line="59"/>
        <source>00:00</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>updateCombo</name>
    <message>
        <location filename="UI/wTableUpdateCombo.py" line="47"/>
        <source>Update value</source>
        <translation>Modifier la valeur</translation>
    </message>
    <message>
        <location filename="UI/wTableUpdateCombo.py" line="48"/>
        <source>TextLabel</source>
        <translation>Sélectionner la valeur</translation>
    </message>
</context>
<context>
    <name>updateDate</name>
    <message>
        <location filename="UI/wTableUpdateDate.py" line="47"/>
        <source>Update value</source>
        <translation>Modifier la Date</translation>
    </message>
    <message>
        <location filename="UI/wTableUpdateDate.py" line="48"/>
        <source>TextLabel</source>
        <translation>Date</translation>
    </message>
</context>
<context>
    <name>updateValue</name>
    <message>
        <location filename="UI/wTableUpdateValue.py" line="47"/>
        <source>Update value</source>
        <translation>Modifier la valeur</translation>
    </message>
    <message>
        <location filename="UI/wTableUpdateValue.py" line="48"/>
        <source>TextLabel</source>
        <translation>Valeur à modifier</translation>
    </message>
</context>
</TS>
