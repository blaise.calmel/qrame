from PyQt5 import QtWidgets, QtGui, QtCore
from UI import wTableUpdateValue


class TableUpdateValue(QtWidgets.QDialog, wTableUpdateValue.Ui_updateValue):
    """Dialog to allow users to change salinity.

    Parameters
    ----------
    wSalinity.Ui_salinity : QDialog
        Dialog window to allow users to change salinity
    """

    def __init__(self, parent=None, title=None, value=None, type='float'):
        super(TableUpdateValue, self).__init__(parent)
        self.setupUi(self)
        if title is not None:
            self.label_value.setText(title)

        if type == 'float':
            # set qlineedit to numbers only, 2 decimals
            rx = QtCore.QRegExp("\d(\.\d{1,2})?")
            validator = QtGui.QRegExpValidator(rx, self)
            self.update_value.setValidator(validator)

        if value:
            self.update_value.setText(str(value))

