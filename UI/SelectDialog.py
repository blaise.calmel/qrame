"""
QRame
Copyright (C) 2023  INRAE

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import os
import glob
import numpy as np
import scipy.io as sio
from PyQt5.QtWidgets import QDialog, QFileDialog, QMessageBox, QApplication
from PyQt5.QtCore import Qt

class SelectFolderDialog(QDialog):
    """ Dialog class to import ADCP moving-boat data from a folder

        Attributes
        ----------
        path_folder: str
            Path of the selected folder
        path_meas: list[str]
            List of measurements path
        type_meas: list[str]
            List of measurements type (TRDI, SonTek, QRev)
        name_meas: list[str]
            List of measurements name
        _datatype: str
            Tool type
        """
    def __init__(self, parent=None):
        """Initializes settings and connections.

        Parameters
        ----------
        parent
            Identifies parent GUI.
        """

        super(SelectFolderDialog, self).__init__(parent)
        # self._translate = QCoreApplication.translate
        self.path_folder = None
        self.path_meas = None
        self.type_meas = None
        self.name_meas = None
        self._datatype = 'ADCP'

        if parent is not None:
            path_folder = QFileDialog.getExistingDirectory(self, parent._translate("Main", "Select Directory"),
                                                           parent.path_folder)
            if len(path_folder) > 0:
                QApplication.setOverrideCursor(Qt.WaitCursor)
                self.path_folder = os.path.abspath(path_folder)

                # ADCP folders path
                path_folders = np.array(glob.glob(self.path_folder + os.sep + "*"))
                # Load their name
                name_folders = np.array([os.path.basename(x) for x in path_folders])

                # Open measurement
                type_meas = list()
                path_meas = list()
                name_meas = list()

                # Identify files
                for id_meas in range(len(path_folders)):
                    print(path_folders[id_meas])
                    if os.path.isdir(path_folders[id_meas]):
                        list_files = os.listdir(path_folders[id_meas])
                        exte_files = list([i.split('.')[-1] for i in list_files])
                        if 'mmt' in exte_files or 'mat' in exte_files:
                            if 'mat' in exte_files:
                                loca_meas = [i for i, s in enumerate(exte_files) if "mat" in s]  # transects index
                                fileNameRaw = [(list_files[x]) for x in loca_meas]
                                qrev_data = False
                                path_qrev = []
                                time_qrev = 0
                                for name in fileNameRaw:
                                    path = os.path.join(path_folders[id_meas], name)
                                    try:
                                        mat_data = sio.loadmat(path, struct_as_record=False, squeeze_me=True)
                                    except TypeError:
                                        mat_data = list()
                                    if 'version' in mat_data:
                                        if time_qrev < os.path.getmtime(path):
                                            path_qrev = path
                                            time_qrev = os.path.getmtime(path)
                                            qrev_data = True
                                if qrev_data:
                                    mat_data = sio.loadmat(path_qrev, struct_as_record=False, squeeze_me=True)
                                    type_meas.append('QRev')
                                    name_meas.append(name_folders[id_meas])
                                    path_meas.append(mat_data)
                                else:
                                    type_meas.append('SonTek')
                                    fileName = [s for i, s in enumerate(fileNameRaw)
                                                if "QRev.mat" not in s]
                                    name_meas.append(name_folders[id_meas])
                                    path_meas.append(
                                        [os.path.join(path_folders[id_meas], fileName[x]) for x in
                                         range(len(fileName))])

                            elif 'mmt' in exte_files:
                                type_meas.append('TRDI')
                                loca_meas = exte_files.index("mmt")  # index of the measurement file
                                fileName = list_files[loca_meas]
                                path_meas.append(os.path.join(path_folders[id_meas], fileName))
                                name_meas.append(name_folders[id_meas])

                QApplication.restoreOverrideCursor()
                # Check that settings are properly selected
                if len(type_meas) > 0:
                    # Confirm settings
                    open_meas = QMessageBox()
                    open_meas.setIcon(QMessageBox.Question)
                    open_meas.setWindowTitle(parent._translate('Main', 'Open Measurements'))
                    open_meas.setText(
                        parent._translate('Main',
                                          "You are about to open ADCP measurements which can be a long process. "
                                          "Please make sure that you select proper settings."
                                          )
                    )
                    open_meas.setStandardButtons(
                        QMessageBox.Open | QMessageBox.Cancel
                    )
                    open_meas.button(open_meas.Open).setText(parent._translate("Main", "Open"))
                    open_meas.button(open_meas.Cancel).setText(parent._translate("Main", "Cancel"))
                    open_meas.setDefaultButton(QMessageBox.Open)
                    open_meas = open_meas.exec()

                    if open_meas == QMessageBox.Open:
                        self.path_meas = path_meas
                        self.type_meas = type_meas
                        self.name_meas = name_meas
                        self._datatype = 'ADCP'
                    else:
                        self.path_folder = None
                else:
                    parent.reportWarning(parent._translate("Main", 'Invalid data : please select a folder which '
                                                                   'contains ADCP folders'))
                    self.path_folder = None


class SelectFileDialog(QDialog):
    """ Dialog class to import ADCP moving-boat measurement from a file.

            Attributes
            ----------
            path_file: str
                File's path
            type_file: str
                Data type
            name_file: str
                File's name

            """
    def __init__(self, open_type, parent=None):
        """Initializes settings and connections.

        Parameters
        ----------
        open_type: str
            Type of data to open (*.dat, *.adcp, *.results, *.xlsx, *.all,
        parent
            Identifies parent GUI.
        """

        super(SelectFileDialog, self).__init__(parent)
        self.path_file = None
        self.type_file = None
        self.name_file = None
        dict_format = {
            'dat': f"{parent._translate('Main', 'HYDRO2 file')} (*.dat)",
            'adcp': f"{parent._translate('Main', 'ADCP measurement')} (*.mmt *.mat)",
            'results': f"{parent._translate('Main', 'Results')} (*.pickle *.ods *.xlsx *.csv)",
            'xlsx': f"{parent._translate('Main', 'Excel file')} (*.xlsx)",
            'all': f"{parent._translate('Main', 'All')} (*.pickle *.ods *.xlsx *.csv *.mmt *.mat)",
        }
        open_data_str = ';;'.join([dict_format[i] for i in open_type])
        if parent is not None:
            # path_file, _ = QFileDialog.getOpenFileName(self, 'Open file', parent.path_folder, open_data_str)
            path_file, _ = QFileDialog.getOpenFileNames(self, 'Open file', parent.path_folder, open_data_str)
            if len(path_file) > 0:
                filename, file_extension = os.path.splitext(path_file[0])
                if file_extension == '.dat':
                    self.path_file = os.path.abspath(path_file[0])
                elif file_extension == '.pickle':
                    self.path_file = os.path.abspath(path_file[0])
                    self.type_file = '.pickle'
                elif file_extension in ['.mmt', '.mat']:
                    keys = [elem.split('.')[-1].split('.')[0] for elem in path_file]
                    self.name_file = os.path.basename(os.path.dirname(path_file[0]))
                    if 'mat' in keys:
                        loca_meas = [i for i, s in enumerate(path_file) if "mat" in s]  # transects index
                        fileNameRaw = [(path_file[x]) for x in loca_meas]
                        qrev_data = False
                        for path in fileNameRaw:
                            # path = os.path.join(path_file, name)
                            try:
                                mat_data = sio.loadmat(path, struct_as_record=False, squeeze_me=True)
                            except TypeError:
                                mat_data = list()
                            if 'version' in mat_data:
                                self.type_file = 'QRev'
                                self.path_file = mat_data
                                qrev_data = True
                                break
                        if not qrev_data:
                            self.type_file = 'SonTek'
                            self.path_file = [s for i, s in enumerate(fileNameRaw)
                                        if "QRev.mat" not in s]
                            self.path_file = [os.path.abspath(x) for x in path_file]
                    elif 'mmt' in keys:
                        self.type_file = 'TRDI'
                        self.path_file = os.path.abspath(path_file[0])

                elif file_extension in ['.ods', '.xlsx', '.csv']:
                    self.type_file = file_extension
                    self.path_file = os.path.abspath(path_file[0])
            # if file_extension == '.xlsx':
            #     path_file = QFileDialog.getOpenFileName(self, 'Open file', None,)
            #     if len(path_file[0]) > 0:
            #         self.path_file = os.path.abspath(path_file[0])
            #         self.name_file = os.path.basename(path_file[0])


class SaveDialog(QDialog):
    """Dialog to allow users to specify file for saving (_QRev.mat or figure)
        Attributes
        ----------
        full_Name: str
            Filename with path to save file
        file_extension: str
            File extension to save data
    """

    def __init__(self, save_type, parent):
        """Initializes settings and connections.

        Parameters
        ----------
        save_type:
            Type of data to save
        parent
            Identifies parent GUI.
        """
        super(SaveDialog, self).__init__(parent)
        # self.setupUi(self)
        self.full_Name = None
        self.file_extension = None

        if parent is not None:
            if parent.path_save is not None and len(parent.path_folder) > 0:
                file_name = parent.name_data
                path = parent.path_save + os.sep + file_name
            else:
                path = os.getcwd()

        # Check data type
        if save_type == 'pickle':
            title = parent._translate('Main', 'Save File as Pickle')
            filetype = f"{parent._translate('Main', 'Pickle file')}(*.pickle)"
            default_filter = None
        elif save_type == 'csv':
            title = parent._translate('Main', 'Export data as csv')
            filetype = f"{parent._translate('Main', 'Excel .csv (separator: semicolon)')}(*.csv)"
            default_filter = None
        elif save_type == 'kml':
            title = parent._translate('Main', 'Export data as kml')
            filetype = f"{parent._translate('Main', 'KML export')}(*.kml)"
            default_filter = None
        elif save_type == 'baratinage':
            title = parent._translate('Main', 'Export for BaRatinAGE')
            filetype = f"{parent._translate('Main', 'Excel csv (separator: semicolon)')}(*.csv);;BAD (*.BAD)"
            default_filter = None
        elif save_type == 'bareme':
            title = parent._translate('Main', 'Export for Bareme')
            filetype = f"{parent._translate('Main', 'csv (separator: semicolon)')}(*.csv)"
            default_filter = None
        elif save_type == 'fig':
            title = parent._translate('Main', 'Save figure')
            filetype = f"Encapsulated Postscript (*.eps);;" \
                       f"Joint Photographic Experts Group (*.jpg);;" \
                       f"Joint Photographic Experts Group (*.jpeg);;" \
                       f"Portable Document Format (*.pdf);;" \
                       f"Portable Network Graphics (*.png);;" \
                       f"Postscript (*.ps);;" \
                       f"Raw RGBA bitmap (*.raw);;" \
                       f"Raw RGBA bitmap (*.rgba);;" \
                       f"Scalable Vector Graphics (*.svg);;" \
                       f"Scalable Vector Graphics (*.svgz);;" \
                       f"Tagged Image File Format (*.tif);;" \
                       f"Tagged Image File Format (*.tiff);;" \
                       f"WebP Image Format (*.webp);;" \
                       f"{parent._translate('Main', 'All Files')} (*)"

            default_filter = "Portable Network Graphics (*.png)"

        # Get the full names (path + file) of the selected file
        if 'QRame' in path.split('\\')[-1]:
            file_save = QFileDialog.getSaveFileName(parent=self, caption=title, directory=path,
                                                    filter=filetype, initialFilter=default_filter)
        else:
            file_save = QFileDialog.getSaveFileName(parent=self, caption=title, directory=path+'_QRame',
                                                    filter=filetype, initialFilter=default_filter)

        # Save data
        if file_save is not None:
            self.full_Name, file_type = file_save
            file_extension = file_type.split('*')[-1][:-1]
            if len(self.full_Name) > 0:
                self.file_extension = file_extension
                if self.full_Name[-len(file_extension):] != file_extension:
                    self.full_Name = self.full_Name + file_extension
