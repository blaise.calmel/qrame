"""
QRame
Copyright (C) 2023  INRAE

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from PyQt5 import QtWidgets, QtGui, QtCore
import wIntercomparisonSettings


class IntercomparisonSettings(QtWidgets.QDialog, wIntercomparisonSettings.Ui_IntercomparisonSettings):
    """Dialog to allow users to change QRev options.

    Parameters
    ----------
    wIntercomparisonSettings.Ui_IntercomparisonSettings : QDialog
        Dialog window with intercomprison settings for users
    """

    def __init__(self, parent=None):
        """Initialize intercomparison dialog.
        """
        super(IntercomparisonSettings, self).__init__(parent)
        self.setupUi(self)

        self.parent = parent

        font = QtGui.QFont()
        font.setPointSize(12)
        font.setBold(False)
        font.setWeight(50)

        # set qlineedit to numbers only, 2 decimals
        rx = QtCore.QRegExp("\d+(\.\d{1,2})?")
        validator = QtGui.QRegExpValidator(rx, self)
        self.le_discharge_ref.setValidator(validator)
        self.le_u_ref.setValidator(validator)
        self.le_deviation.setValidator(validator)
        self.le_u_bias.setValidator(validator)

        # set qlineedit to integers only
        rx = QtCore.QRegExp("\d+")
        validator = QtGui.QRegExpValidator(rx, self)
        self.le_number_transects.setValidator(validator)

