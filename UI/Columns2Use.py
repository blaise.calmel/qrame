"""
QRame
Copyright (C) 2023  INRAE

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from PyQt5 import QtWidgets, QtCore, QtGui
import wColumns2Use
import copy

class Columns2Use(QtWidgets.QDialog, wColumns2Use.Ui_Columns2Use):
    """Dialog to allow users to select and deselect transects to use in the discharge computation.

    Parameters
    ----------
    wColumns2Use.Ui_Columns2Use : QDialog
        Dialog window with selected columns

    Attributes
    ----------
    _pressed_key: str
        Get the current pressed key button
    _selected_row: int
        Get index of the current selected row

    parent:
        Identifies parent GUI.
    units: dict
        Dictionary of units from units_conversion
    previous_checked: list[str]
        List of previously checked columns name
    current_checked: list[str]
        List of current checked columns name
    """

    def __init__(self, parent=None, units=None):
        """Initialize dialog
        """
        super(Columns2Use, self).__init__(parent)
        self.setupUi(self)
        self._pressed_key = None
        self._selected_row = 0

        self.parent = parent
        self.units = units
        self.previous_checked = parent.columns_checked
        self.current_checked = copy.deepcopy(self.previous_checked)

        # Populate table
        self.columns_table()

        # # Setup connections for buttons
        self.pb_check_all.clicked.connect(self.check_all)
        self.pb_uncheck_all.clicked.connect(self.uncheck_all)
        self.tableSelect.cellClicked.connect(self.col_table_clicked)

    def check_all(self):
        """Checks all transects for use.
        """
        self.current_checked = self.parent.columns
        self.columns_table()

    def uncheck_all(self):
        """Unchecks all transects for user.
        """
        self.current_checked = {}
        self.columns_table()

    def columns_table(self):
        """Create and populate main summary table.
        """
        parent = self.parent
        tbl = self.tableSelect

        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)

        summary_header = [parent._translate("Main", 'Selected columns')]
        ncols = len(summary_header)
        nrows = len(parent.columns)

        tbl.setRowCount(nrows)
        tbl.setColumnCount(ncols)
        tbl.setHorizontalHeaderLabels(summary_header)
        # tbl.horizontalHeader().hide()
        tbl.verticalHeader().hide()
        tbl.setEditTriggers(QtWidgets.QTableWidget.NoEditTriggers)
        header = tbl.horizontalHeader()
        header.setSectionResizeMode(0, QtWidgets.QHeaderView.Stretch)

        row = 0
        for key, value in parent.columns.items():
            col = 0
            item = QtWidgets.QTableWidgetItem(value[0])
            item.setData(QtCore.Qt.UserRole, key)

            if key in self.current_checked.keys():
                item.setCheckState(QtCore.Qt.Checked)
            else:
                item.setCheckState(QtCore.Qt.Unchecked)

            item.setFlags(QtCore.Qt.ItemIsEnabled)
            tbl.setItem(row, col, item)
            tbl.item(row, col).setFont(font)

            row += 1


    def keyPressEvent(self, event):
        print('Pressed : ', event.key())
        self._pressed_key = event.key()

    def keyReleaseEvent(self, event):
        print('Unpressed: ', event.key())
        self._pressed_key = None

    @QtCore.pyqtSlot(int, int)
    def col_table_clicked(self, row, column):
        """ Manages actions caused by the user clicking in selected columns
            of the table.

            Parameters
            ----------
            row: int
                row in table clicked by user
            column: int
                column in table clicked by user
        """
        print('col_table_clicked is running')
        tbl = self.tableSelect
        if self._pressed_key == 16777248:
            start = min(row, self._selected_row)
            end = max(row, self._selected_row) + 1
            if tbl.item(row, 0).checkState():
                for id_row in range(start, end):
                    tbl.item(id_row, 0).setCheckState(QtCore.Qt.Unchecked)
            else:
                for id_row in range(start, end):
                    tbl.item(id_row, 0).setCheckState(QtCore.Qt.Checked)
        else:
            if tbl.item(row, 0).checkState():
                tbl.item(row, 0).setCheckState(QtCore.Qt.Unchecked)
            else:
                tbl.item(row, 0).setCheckState(QtCore.Qt.Checked)

        self._selected_row = row


