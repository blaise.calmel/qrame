"""
QRame
Copyright (C) 2023  INRAE

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
"""

import os
import re
import sys
import copy
from PyQt5 import QtWidgets, QtGui, QtCore
from PyQt5.QtCore import Qt, QTranslator
from PyQt5.QtWidgets import QFileDialog, QApplication, QMainWindow
import pickle
import simplekml

import pandas as pd
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
# from matplotlib.backends import backend_pdf, backend_svg, backend_ps  # avoid bug after compilation
import numpy as np
from scipy.interpolate import interp1d
import statsmodels.api as sm
from datetime import datetime, time, date
from dateutil import parser

from Controller.LoadMeasurements import LoadMeasurements
from Controller.Intercomparison import Intercomparison
from Controller.common_functions import units_conversion, scientific_notation, formatSize

from UI.main_window import Ui_MainWindow
from UI.ProgressBar import ProgressBar
from UI.Measurements2Use import Measurements2Use
from UI.Columns2Use import Columns2Use
from UI.Options import Options
from UI.ColumnsSelection import ColumnsSelection
from UI.IntercomparisonSettings import IntercomparisonSettings
from UI.SelectDialog import SelectFolderDialog, SaveDialog, SelectFileDialog
from UI.MplCanvas import MplCanvas
from UI.FigDischargeUncertainty import FigDischargeUncertainty
from UI.FigTimeDischarge import FigTimeDischarge
from UI.FigUncertaintySources import FigUncertaintySources
from UI.FigDischargeDistribution import FigDischargeDistribution
from UI.FigDischargeCorrection import FigDischargeCorrection
from UI.TableUpdateValue import TableUpdateValue
from UI.TableUpdateCombo import TableUpdateCombo
from UI.TableUpdateDate import TableUpdateDate

import qrev
from qrev.Classes.stickysettings import StickySettings as SSet
qrev_path = os.path.dirname(os.path.dirname(os.path.realpath(qrev.Classes.__file__)))
sys.path.append(qrev_path)

def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        # base_path = os.path.abspath(".")
        base_path = os.path.abspath(os.path.join(os.path.abspath("."), os.pardir))

    return os.path.join(base_path, relative_path)

class NumericItem(QtWidgets.QTableWidgetItem):
    def __lt__(self, other):
        return (self.data(QtCore.Qt.UserRole) <
                other.data(QtCore.Qt.UserRole))

class ApplicationWindow(QMainWindow, Ui_MainWindow):
    """This is the primary class controlling the user interface and the computational code for QRame application

    Attributes
    ----------
    QRame_version: str
        Text specifying current version of code
    settingsFile: str
        Name of the settings file
    sticky_settings: StickySettings
        Used to store various settings (folder, units, etc)
    _translate: QCoreApplication.translate object
        Save words which need to be translate
    trans: QTranslator
        Translator

    name_data: str
        Name of the folder from opened data
    worker_extract: QThread object
        Thread to load data from mobile ADCP applying QRevint
    worker_intercomparison: QThread object
        Thread to apply intercomparison process

    transects_df: pandas DataFrame
        DataFrame with transects data
    mean_meas: pandas DataFrame
        DataFrame from grouped transects_df by measurement name

    data_bareme: pandas DataFrame
        Imported DataFrame from Bareme
    name_bareme: str
        Name of the Bareme file
    pbar_popup: ProgressBar object
        ProgressBar showing process of thread

    path_folder: str
        Path of the selected folder
    path_save: str
        Path of the last saved data
    language: str
        Current selected language
    units: dict
        Dictionary of units from units_conversion

    threshold: float
        Threshold to tolerated deviation from a transect to the its measurement, automaticly removed it if above
    use_weighted: bool
        Use QRevint weighted extrpolation

    column_grouped_idx: int
        Index of the columns grouped for grouped analysis
    columns_grouped: List[str]
        List of columns name that could be used for grouped analysis

    column_effect_section_idx: int
        Index of the section column
    column_effect_team_idx: int
        Index of the team column
    columns_effects: List[str]
        List of columns name that could be used for 2 factor intercomparison analysis

    intercomparison_process: bool
        Specify if QRame should apply intercomparison computation
    discharge_deviation: float
        Discharge maximum tolerated deviation from the reference
    empirical_n_transects: float
        Number ot transect for empirical uncertainty computation
    empirical_n_sections: int
        Number of sections for second factor analysis
    empirical_n_teams int
        Number of teams for second factor analysis
    discharge_mean: float
        Mean discharge value from all transects
    auto_discharge_reference: bool
        Use mean discharge as reference
    uncertainty_reference: float
        Uncertainty on the discharge reference
    discharge_reference: str
        User discharge reference
    auto_u_bias: bool
        Compute automaticly uncertainty bias
    u_bias: float
        Uncertainty bias value
    correct_discharge: bool
        Specify if QRame should compute LOWESS discharge correction
    change_correction: bool
        Specify if there is any change for discharge correction
    lowess_t: numpy array
        Middle time of each transect to compute LOWESS correction
    lowess_q: numpy array
        Correction discharge after LOWESS correction

    matching_col: dict
        Dictionnary matching columns from QRame to imported spreadsheet
    add_data: bool
        Indicate whether the new data should be added to the current data or overwritten them
    results_import_df: pandas DataFrame
        Imported data from spreadsheets

    select_folder: list[str]
        Folder paths
    meas_checked: list[str]
        Selected measurements name, can be updated
    meas_name: list[str]
        Default measurements name
    selected_meas: str
        Currenct selected measurement

    nav_ref: str
        Current navigation reference (default, BT or GPS)
    no_moving_bed: bool
        Specify if there is no moving bed observed

    extrap_law_idx: int
        Current extrapolation law index (0: Auto, 1: PP, 2: CST-NS, 3: 3PT-NS)

    extrap_type_idx: int
        Current extrapolation law data type index (0: Discharge, 1: Velocity)
    extrap_exponent: str/float
        Current extrapolation exponent ('default' or float)
    extrap_subsection: list[float, float]
        Subsection to use for extrapolation
    excluded_dist: str/float
        Force excluded distance for every measurements

    freeze: bool
        Freeze interface
    change_ui: bool
        Update user interface

    discharge_uncertainty_canvas: MplCanvas object
        Discharge and OURSIN uncertainty graph's canvas
    discharge_uncertainty_toolbar: NavigationToolbar2QT object
        Discharge and OURSIN uncertainty graph's toolbar
    discharge_uncertainty_fig: FigDischargeUncertainty object
        Discharge and OURSIN uncertainty graph's figure

    discharge_time_canvas: MplCanvas object
        Chronological discharge graph's canvas
    discharge_time_toolbar: NavigationToolbar2QT object
        Chronological discharge graph's toolbar
    discharge_time_fig: FigTimeDischarge object
        Chronological discharge graph's figure

    uncertainty_sources_canvas: MplCanvas object
        Uncertainty sources decomposition graph's canvas
    uncertainty_sources_toolbar: NavigationToolbar2QT object
        Uncertainty sources decomposition graph's toolbar
    uncertainty_sources_fig: FigUncertaintySources object
        Uncertainty sources decomposition graph's figure

    discharge_distribution_canvas: MplCanvas object
        Discharge distribution graph's canvas
    discharge_distribution_toolbar: NavigationToolbar2QT object
        Discharge distribution graph's toolbar
    discharge_distribution_fig: FigDischargeDistribution object
        Discharge distribution graph's figure

    discharge_correction_canvas: MplCanvas object
        LOWESS correction graph's canvas
    discharge_correction_toolbar: NavigationToolbar2QT object
        LOWESS correction graph's toolbar
    discharge_correction_fig: FigDischargeCorrection object
        LOWESS correction graph's figure

    q_distribution_violin: bool
        Plot distribution graph errorbox as violin
    q_distribution_nb_transects: bool
        Plot distribution graph number of transects per measurement
    q_distribution_color_score: bool
        Plot distribution graph with color based on the z score

    canvases: list
        List of canvases
    figs: list
        List of figures
    toolbars: list
        List of toolbars
    current_fig: Object
        Current selected figure object of the canvas
    current_canvas: MplCanvas object
        Current selected canvas
    figsMenu: QMenu object
        QMenu for figures

    tables: list
        List of current tables
    columns: list
        List of every avalaible columns
    columns_checked: list
        List of columns to show

    init_tables: bool
        Specify if tables should be initialized
    sorted_name: list
        List of measurements name sorted by selected column
    column_sorted: str
        Name of the selected column to sort measurement
    column_sorted_idx: int
        Index of the selected column to sort measurement
    ascending: bool
        Specify if current data are sorted ascending or descending
    tableMeasurements: main_window object
        Table used to show data by measurements
    tableMenu: QMenu object
        Open a menu after right click on a measurement's name in tableMeasurements
    """

    def __init__(self):
        super(ApplicationWindow, self).__init__()

        # Load main screen geometry
        self.setupUi(self)
        height = int(QtWidgets.QDesktopWidget().screenGeometry(0).height() * 0.8)
        width = int(QtWidgets.QDesktopWidget().screenGeometry(0).width() * 0.8)

        self.resize(width, height)
        self.showMaximized()
        self.tableResume.resizeColumnToContents(0)

        self.QRame_version = 'QRame v1.26'
        self.setWindowTitle(self.QRame_version)

        self.setWindowIcon(QtGui.QIcon(resource_path('QRame.ico')))
        # self.setWindowIcon(QtGui.QIcon('QRame.ico'))

        # Load previous settings
        self.settingsFile = 'QRAME_Settings'
        self.sticky_settings = SSet(self.settingsFile)
        self._translate = QtCore.QCoreApplication.translate
        self.trans = QTranslator(self)

        self.name_data = None
        self.worker_extract = None
        self.worker_intercomparison = None
        # self.measurements_dict = None
        # self.raw_transects_df = None
        self.transects_df = None
        self.mean_meas = None

        self.data_bareme = None
        self.name_bareme = None
        self.pbar_popup = None

        # Settings
        if 'path_folder' in self.sticky_settings.settings:
            if os.path.isdir(self.sticky_settings.get('path_folder')):
                self.path_folder = self.sticky_settings.get('path_folder')
            else:
                self.path_folder = os.getcwd()
                self.sticky_settings.set('path_folder', self.path_folder)
        else:
            self.path_folder = os.getcwd()
            self.sticky_settings.new('path_folder', self.path_folder)

        if 'path_save' in self.sticky_settings.settings:
            if os.path.isdir(self.sticky_settings.get('path_save')):
                self.path_save = self.sticky_settings.get('path_save')
            else:
                self.path_save = os.getcwd()
                self.sticky_settings.set('path_save', self.path_save)
        else:
            self.path_save = os.getcwd()
            self.sticky_settings.new('path_save', self.path_save)

        if 'language' in self.sticky_settings.settings:
            self.language = self.sticky_settings.get('language')
        else:
            self.language = 'en'
            self.sticky_settings.new('language', 'en')
        if self.language != 'en':
            self.change_language()

        if 'UnitsID' not in self.sticky_settings.settings:
            self.sticky_settings.new('UnitsID', 'SI')
        self.units = units_conversion(units_id=self.sticky_settings.get('UnitsID'))

        if 'threshold' in self.sticky_settings.settings:
            self.threshold = self.sticky_settings.get('threshold')
        else:
            self.threshold = None
            self.sticky_settings.new('threshold', None)

        if 'weighted' in self.sticky_settings.settings:
            self.use_weighted = self.sticky_settings.get('weighted')
        else:
            self.use_weighted = True
            self.sticky_settings.new('weighted', True)

        self.column_grouped_idx = 0
        self.columns_grouped = [None]
        self.column_effect_section_idx = 0
        self.column_effect_team_idx = 0
        self.columns_effects = [None]

        self.intercomparison_process = False
        self.discharge_deviation = 5
        self.empirical_n_transects = 1
        self.empirical_n_sections = 1
        self.empirical_n_teams = 1
        self.discharge_mean = None
        self.auto_discharge_reference = True
        self.uncertainty_reference = 0
        self.discharge_reference = ''
        self.auto_u_bias = True
        self.u_bias = 0
        self.correct_discharge = False
        self.change_correction = False
        self.lowess_t = None
        self.lowess_q = None

        # Import results from csv
        self.matching_col = None
        self.add_data = False
        self.results_import_df = None

        self.select_folder = None  # Save folder paths
        self.meas_checked = None  # Selected measurements name, can be updated
        self.meas_name = None  # Default measurements name
        self.selected_meas = None

        # Settings
        self.nav_ref = "default"
        self.no_moving_bed = False

        self.extrap_law_idx = 0
        self.extrap_type_idx = 0
        self.extrap_exponent = self._translate('Main', 'Optimized')
        self.extrap_subsection = [0, 100]
        self.excluded_dist = ''

        # Freeze action
        self.freeze = False
        self.change_ui = False

        # Figures
        self.discharge_uncertainty_canvas = None
        self.discharge_uncertainty_toolbar = None
        self.discharge_uncertainty_fig = None
        self.discharge_time_canvas = None
        self.discharge_time_toolbar = None
        self.discharge_time_fig = None
        self.uncertainty_sources_canvas = None
        self.uncertainty_sources_toolbar = None
        self.uncertainty_sources_fig = None
        self.discharge_distribution_canvas = None
        self.discharge_distribution_toolbar = None
        self.discharge_distribution_fig = None
        self.discharge_correction_canvas = None
        self.discharge_correction_toolbar = None
        self.discharge_correction_fig = None

        # Distribution Figure
        self.q_distribution_violin = False
        self.q_distribution_nb_transects = False
        self.q_distribution_color_score = False

        self.canvases = None
        self.figs = None
        self.toolbars = None
        self.current_fig = None
        self.current_canvas = None

        # Table
        self.tables = None
        self.columns = None
        if 'columns_checked' in self.sticky_settings.settings:
            self.columns_checked = self.sticky_settings.get('columns_checked')
        else:
            self.columns_checked = None
            self.sticky_settings.new('columns_checked', None)

        self.init_tables = True
        self.sorted_name = []
        self.column_sorted = None
        self.column_sorted_idx = None
        self.ascending = True
        self.tableMeasurements.cellDoubleClicked.connect(self.table_meas_double_clicked)
        self.tableMeasurements.horizontalHeader().sortIndicatorChanged.connect(
            lambda col_idx: self.sort_figure_x(col_idx))

        self.tableMenu = QtWidgets.QMenu(self)
        self.tableMenu.addAction(self._translate('Main', 'Rename'), self.renameMeas)
        self.tableMenu.addAction(self._translate('Main', 'Update'), self.updateMeas)
        self.tableMenu.addAction(self._translate('Main', 'Delete'), self.deleteMeas)

        # Save figure menu with Right click
        self.figsMenu = QtWidgets.QMenu(self)
        self.figsMenu.addAction(self._translate('Main', 'Save figure'), self.saveFig)
        self.figsMenu.addAction(self._translate('Main', 'Change scale'), self.change_scale)

        tab_idx = self.tab_all.indexOf(
            self.tab_all.findChild(QtWidgets.QWidget, "tab_q_correction")
        )
        if tab_idx > 0:
            self.tab_all.removeTab(tab_idx)

        # Setup UI
        self.setup_connections()
        self.disable_button()

    def setup_connections(self):
        """ Connect action to function at the start of the application.
        """
        # Menu
        self.actionOpen_data.triggered.connect(self.open_data)
        self.actionOpen_results.triggered.connect(self.open_result)
        self.actionImport_hydro2.triggered.connect(self.import_hydro2)
        self.actionExport_csv.triggered.connect(self.export_csv)
        self.actionExport_bareme.triggered.connect(self.export_bareme)
        self.actionExport_baratinage.triggered.connect(self.export_baratinage)
        self.actionSave_pickle.triggered.connect(self.save_pickle)

        self.cb_violin.clicked.connect(lambda: self.update_param(self.cb_violin, 'q_distribution_violin', [3]))
        self.cb_nb_transect.clicked.connect(lambda: self.update_param(self.cb_nb_transect,
                                                                      'q_distribution_nb_transects', [3]))
        self.cb_color_score.clicked.connect(lambda: self.update_param(self.cb_color_score,
                                                                      'q_distribution_color_score', [3]))

        # Toolbar
        self.actionOpen.triggered.connect(self.open_data)
        self.actionSettings.triggered.connect(self.apply_settings)
        self.actionChecked.triggered.connect(self.select_meas)
        self.actionTable.triggered.connect(self.select_col)
        self.actionIntercomparison.triggered.connect(self.apply_intercomparison)
        self.actionAddMeasurement.triggered.connect(self.add_measurement)

        # Toolbar graphs
        self.actionHomeGraph.triggered.connect(self.home)
        self.actionZoomGraph.triggered.connect(self.zoom)
        self.actionPanGraph.triggered.connect(self.pan)

        self.actionKML.triggered.connect(self.export_kml)

    def disable_button(self):
        """ Disable action at start of the application.
        """
        self.actionChecked.setEnabled(False)
        self.actionIntercomparison.setEnabled(False)
        self.actionAddMeasurement.setEnabled(False)

        self.actionImport_hydro2.setEnabled(False)
        self.actionExport_csv.setEnabled(False)
        self.actionExport_bareme.setEnabled(False)
        self.actionExport_baratinage.setEnabled(False)
        self.actionSave_pickle.setEnabled(False)
        self.actionTable.setEnabled(False)
        self.actionKML.setEnabled(False)

        self.actionHomeGraph.setEnabled(False)
        self.actionZoomGraph.setEnabled(False)
        self.actionPanGraph.setEnabled(False)

        self.tab_all.setEnabled(False)

    def reset_settings(self):
        """ Reset settings when loading a new project.
        """
        self.discharge_reference = ''
        self.auto_discharge_reference = True
        self.auto_u_bias = True
        self.column_grouped_idx = 0
        self.columns_grouped = [None]
        self.intercomparison_process = False
        self.column_effect_team_idx = 0
        self.column_effect_section_idx = 0
        self.excluded_dist = ''
        self.init_tables = True
        self.sorted_name = []
        self.column_sorted = None
        self.column_sorted_idx = None
        self.ascending = True
        self.selected_meas = None
        self.correct_discharge = False
        self.transects_df = None
        self.mean_meas = None
        self.results_import_df = None
        self.meas_name = None

    def open_data(self):
        """ Initialize a dialog to allow user to select folder with ADCP data (*.mmt and *.mat).
        """
        # Open dialog
        self.enable_menu(way=False)
        self.select_folder = SelectFolderDialog(parent=self)
        if self.select_folder.path_folder is not None:
            QApplication.setOverrideCursor(Qt.WaitCursor)
            self.path_folder = os.path.dirname(self.select_folder.path_folder)
            self.name_data = os.path.basename(self.select_folder.path_folder)
            self.sticky_settings.set('path_folder', self.path_folder)
            self.setWindowTitle(self.QRame_version + ' : ' + self.select_folder.path_folder)
            self.reset_settings()
            self.thread_extract(path_meas=self.select_folder.path_meas,
                                type_meas=self.select_folder.type_meas,
                                name_meas=self.select_folder.name_meas)
        else:
            self.enable_menu(way=True)
            QApplication.restoreOverrideCursor()

    def open_result(self):
        """ Select previous intercomparison results (*.pickle) dialog and load data.
        """
        self.enable_menu(way=False)
        # Open dialog window
        dialog = SelectFileDialog(open_type=['results'], parent=self)
        if dialog.path_file is not None:
            # Load previous QRame session as *.pickle or spreadsheet
            if isinstance(dialog.path_file, str) and dialog.type_file == '.pickle':
                self.reset_settings()
                self.path_folder = os.path.abspath(os.path.dirname(dialog.path_file))
                self.sticky_settings.set('path_folder', self.path_folder)
                self.name_data = os.path.basename(os.path.splitext(dialog.path_file)[0])
                # Launch extract thread
                self.thread_extract(path_meas=dialog.path_file, type_meas='pickle')
                self.setWindowTitle(self.QRame_version + ' : ' + os.path.abspath(dialog.path_file))
            elif isinstance(dialog.path_file, str) and dialog.type_file in ['.ods', '.xlsx', '.csv']:
                # Match spreadsheet columns to QRame dataframe columns
                self.define_columns()
                # Import spreadsheet
                if dialog.type_file == '.ods':
                    doc_import = pd.read_excel(dialog.path_file, engine="odf", sheet_name=None)
                elif dialog.type_file == '.xlsx':
                    doc_import = pd.read_excel(dialog.path_file, sheet_name=None)
                else:
                    doc_csv = pd.read_csv(dialog.path_file, sep=';')
                    doc_import = {'': doc_csv}

                QApplication.restoreOverrideCursor()
                # Format spreadsheet
                measurements_csv = self.open_import(doc_import)

                if measurements_csv is not None:
                    self.reset_settings()
                    self.setWindowTitle(self.QRame_version + ' : ' + os.path.abspath(dialog.path_file))
                    self.name_data = os.path.basename(dialog.path_file)
                    self.transects_df = measurements_csv
                    self.results_import_df = measurements_csv
                    self.meas_name = np.unique(measurements_csv['meas_name'])

                    if self.correct_discharge and self.intercomparison_process:
                        self.compute_correction()

                    self.thread_intercomp()
                else:
                    self.enable_menu(way=True)
            else:
                self.enable_menu(way=True)

        else:
            self.enable_menu(way=True)

    def add_measurement(self):
        """ Open an ADCP measurement (*.mmt or *.mat) or a spreadsheet or a previous QRame session (*.pickle)
        and add it to current data.
        """
        self.enable_menu(way=False)
        QApplication.setOverrideCursor(Qt.WaitCursor)
        # Open dialog window
        dialog = SelectFileDialog(open_type=['all', 'adcp', 'results'], parent=self)
        if dialog.path_file is not None:
            self.add_data = True
            # Load previous QRame session
            if dialog.type_file == '.pickle':
                self.column_grouped_idx = 0
                self.column_effect_section_idx = 0
                self.column_effect_team_idx = 0
                if self.worker_extract is not None:
                    # Load data in extract thread if previously launched
                    self.worker_extract.path_meas = dialog.path_file
                    self.worker_extract.type_meas = 'pickle'
                    self.worker_extract.start()
                else:
                    # Launch extract thread
                    self.path_folder = os.path.abspath(os.path.dirname(dialog.path_file))
                    self.sticky_settings.set('path_folder', self.path_folder)
                    self.name_data = os.path.basename(os.path.splitext(dialog.path_file)[0])
                    self.thread_extract(path_meas=dialog.path_file, type_meas='pickle')
            # Load ADCP data
            elif dialog.type_file in ['SonTek', 'TRDI', 'QRev']:
                name_meas = dialog.name_file
                self.add_data = True
                if self.worker_extract is not None:
                    # Check if a similar measurement's name is already present
                    if name_meas in self.worker_extract.measurements_dict:
                        QApplication.restoreOverrideCursor()
                        warning = QtWidgets.QMessageBox()
                        warning.setIcon(QtWidgets.QMessageBox.Warning)
                        warning.setWindowTitle("Close")
                        warning.setText(
                            name_meas + self._translate(
                                'Main',
                                "  is already open. Opening it will overwrite the previous measure. "
                                "Please confirm or rename the folder."
                            )
                        )
                        warning.setStandardButtons(QtWidgets.QMessageBox.Ok | QtWidgets.QMessageBox.Cancel)
                        warning.setDefaultButton(QtWidgets.QMessageBox.Ok)
                        warning = warning.exec()
                        if warning != QtWidgets.QMessageBox.Ok:
                            return
                        else:
                            QApplication.setOverrideCursor(Qt.WaitCursor)
                    # Load data in extract thread if previously launched
                    self.worker_extract.path_meas = [dialog.path_file]
                    self.worker_extract.type_meas = [dialog.type_file]
                    self.worker_extract.name_meas = [dialog.name_file]
                    self.worker_extract.add_data = self.add_data
                    self.worker_extract.start()
                else:
                    # Launch extract thread
                    self.thread_extract(path_meas=[dialog.path_file],
                                        type_meas=[dialog.type_file],
                                        name_meas=[dialog.name_file])
            # Load spreadsheet data
            elif dialog.type_file in ['.ods', '.xlsx', '.csv']:
                # Import spreadsheet
                if dialog.type_file == '.ods':
                    doc_import = pd.read_excel(dialog.path_file, engine="odf", sheet_name=None)
                elif dialog.type_file == '.xlsx':
                    doc_import = pd.read_excel(dialog.path_file, sheet_name=None)
                else:
                    doc_csv = pd.read_csv(dialog.path_file, sep=';')
                    doc_import = {'': doc_csv}
                QApplication.restoreOverrideCursor()
                # Format spreadsheet
                measurements_csv = self.open_import(doc_import)
                if measurements_csv is not None:
                    self.load_csv(measurements_csv)
                    if self.correct_discharge and self.intercomparison_process:
                        self.compute_correction()
                    self.thread_intercomp()
            else:
                self.enable_menu(way=True)
                QApplication.restoreOverrideCursor()
        else:
            self.enable_menu(way=True)
            QApplication.restoreOverrideCursor()

    def load_csv(self, measurements_csv, checked_meas=None):
        """ Load spreadsheet dataframe in QRame environment.
        :param measurements_csv: Pandas DataFrame from imported spreadsheet
        :param checked_meas: List of selected measurement names if import from previous QRame project
        """
        if measurements_csv is not None:
            new_meas_names = np.unique(measurements_csv['meas_name'])

            # Save measurements csv
            if self.results_import_df is None:
                self.results_import_df = measurements_csv
            else:
                # Remove same name rows
                self.results_import_df = self.results_import_df[
                    ~self.results_import_df['meas_name'].isin(new_meas_names)]
                self.transects_df = self.transects_df[
                    ~self.transects_df['meas_name'].isin(new_meas_names)]
                # Add new data
                self.results_import_df = self.results_import_df.append(measurements_csv)
            self.results_import_df = self.results_import_df.reset_index(drop=True)

            # Get all meas name
            if self.meas_name is None:
                self.meas_name = np.unique(measurements_csv['meas_name'])
            else:
                self.meas_name = np.unique(np.append(self.meas_name, new_meas_names))
            self.meas_name = np.sort(self.meas_name)

            # Select only checked meas
            if checked_meas is not None:
                measurements_csv = measurements_csv[measurements_csv['meas_name'].isin(checked_meas)]

            # Merge to current data
            transects_df = pd.concat([self.transects_df, measurements_csv], ignore_index=True, join='outer')
            self.transects_df = transects_df.reset_index(drop=True)

            # Update checked measurements
            if self.meas_checked is None:
                self.meas_checked = new_meas_names
            else:
                self.meas_checked = np.unique(np.append(self.meas_checked, new_meas_names))

        else:
            self.enable_menu(way=True)

    def open_import(self, doc_import):
        """ Open imported spreadsheet and match its columns to QRame dataframe.
        :param doc_import: Pandas DataFrame from imported spreadsheet
        :return df: Pandas DataFrame
        """
        df = None
        self.change_correction = True
        select_columns = ColumnsSelection(self)

        # Connect press button to add combo box
        select_columns.pb_add_col.clicked.connect(
            lambda: self.add_import_column(select_columns)
        )
        select_columns.pb_remove_col.clicked.connect(
            lambda: self.remove_import_column(select_columns)
        )
        try:
            # Initialize Import sheet
            select_columns.combo_import_sheet.clear()
            i = 0
            for key in doc_import.keys():
                i += 1
                select_columns.combo_import_sheet.addItem(key)

            select_columns.combo_import_sheet.currentIndexChanged.connect(
                lambda: self.change_import_sheet(select_columns, doc_import)
            )

            # Initialize Import column
            import_col_idx = list(doc_import.keys())[0]
            select_columns.combo_import_col.clear()
            i = 0
            for key in doc_import[import_col_idx].columns:
                i += 1
                select_columns.combo_import_col.addItem(key)

            # Initialize QRame column
            select_columns.combo_qrame_col.clear()
            col_test = ['meas_name', 'meas_mean_q', 'meas_oursin_95',
                        'date', 'start_time', 'end_time', 'meas_depth', 'meas_width',
                        'meas_area', 'meas_mean_v', 'meas_tool', 'meas_manufacturer', 'meas_model']
            i = -1
            for key in col_test:
                i += 1
                if key in self.columns.keys():
                    select_columns.combo_qrame_col.addItem(self.columns[key][0])
                else:
                    if key == 'meas_name':
                        select_columns.combo_qrame_col.addItem(self._translate("Main", 'Name'))
                    elif key == 'date':
                        select_columns.combo_qrame_col.addItem(self._translate("Main", 'Date'))
                    else:
                        select_columns.combo_qrame_col.addItem(key)
                select_columns.combo_qrame_col.setItemData(i, key)

            select_columns.tbl_import_qrame.setSelectionBehavior(QtWidgets.QTableView.SelectRows)

            self.matching_col = {}
            select_columns_exec = select_columns.exec_()
            if select_columns_exec:
                # Load column name and corresponding import values
                data_import = {}
                for key in self.matching_col:
                    sheet, col = self.matching_col[key]
                    data_import[key] = list(doc_import[sheet][col])

                # Create dataframe
                df = pd.DataFrame(data_import)
                if 'meas_mean_q' not in df.columns:
                    df['meas_mean_q'] = 0
                # Remove empty discharge columns
                df.dropna(subset=['meas_mean_q', 'meas_name'], inplace=True)
                df = df.reset_index(drop=True)
                if 'meas_name' not in df.columns:
                    df['meas_name'] = ['other_measurement_' + str(i) for i in range(len(df))]
                if 'meas_tool' not in df.columns:
                    df['meas_tool'] = 'other'
                if 'meas_oursin_95' in df.columns:
                    df['meas_oursin_95'] = df['meas_oursin_95'] * 100

                # Get labels from measurements name
                df = self.define_labels_value(df)
                df['meas_label_user'] = ''

                # Find corresponding time/date
                selected_time = []
                if 'start_time' in df.columns:
                    selected_time.append('start_time')
                if 'end_time' in df.columns:
                    selected_time.append('end_time')

                if len(selected_time) > 0:
                    # Convert date column to date format
                    if 'date' in df.columns:
                        if isinstance(df['date'][0], str):
                            parser.parse(df['date'][0])
                            dates = [parser.parse(df['date'][i]).date() for i in range(len(df))]
                        else:
                            dates = [datetime.date(df.date[i]) for i in range(len(df))]
                    elif self.transects_df is not None:
                        median_timestamp = np.nanmedian(self.transects_df['start_time'])
                        if np.isnan(median_timestamp):
                            median_timestamp = datetime.now().timestamp()
                        median_date = datetime.fromtimestamp(median_timestamp).date()
                        dates = [median_date] * len(df)
                    else:
                        median_date = datetime.today().date()
                        dates = [median_date] * len(df)

                    # Match date and time
                    for column_time in selected_time:
                        valid_time = ~df[column_time].isnull().values
                        if np.any(valid_time):
                            if isinstance(df[column_time][valid_time].iloc[0], datetime):
                                df[column_time] = df.apply(lambda row: datetime.timestamp(
                                    row[column_time]), axis=1)
                            elif isinstance(df[column_time][valid_time].iloc[0], time):
                                df['date'] = dates
                                df[column_time] = df.apply(lambda row: datetime.timestamp(
                                    datetime.combine(row['date'], row[column_time])
                                ) if pd.notnull(row[column_time]) else row[column_time], axis=1)
                            elif isinstance(df[column_time][valid_time].iloc[0], str):
                                df['date'] = [date.strftime('%Y-%m-%d') for date in dates]
                                df[column_time] = df.apply(lambda row: datetime.timestamp(
                                    parser.parse(' '.join([row['date'], row[column_time]]))), axis=1)
                        else:
                            df[column_time] = np.nan

                # Format Name and Discharge
                df['tr_q_total'] = copy.deepcopy(df['meas_mean_q'])
                df['meas_id'] = ''
                grouped_df = df.groupby('meas_name').agg({'tr_q_total': 'mean', 'meas_name': 'count'})
                for name in np.unique(df['meas_name']):
                    df.loc[df['meas_name'] == name, 'meas_id'] = [
                        name + '_' + str(i) for i in range(grouped_df.loc[name, 'meas_name'])]
                    df.loc[df['meas_name'] == name, 'meas_mean_q'] = grouped_df.loc[name, 'tr_q_total']
                    df.loc[df['meas_name'] == name, 'meas_ntransects'] = grouped_df.loc[name, 'meas_name']

                # Fill missing columns with nan
                missing_columns = [i for i in self.columns.keys() if i not in df.columns]
                for col in missing_columns:
                    df[col] = np.nan
                df['tr_oursin_u_boat'] = np.nan
                df['tr_oursin_u_depth'] = np.nan
                df['tr_oursin_u_water'] = np.nan

        except Exception as e:
            self.reportWarning(f'Invalid format: {e}')
            df = None
            self.enable_menu()

        return df

    def define_labels_value(self, df):
        """ Split measurements name into several labels and add them to main DataFrame.
        :param df: Pandas DataFrame from checked transects data
        :return: Pandas DataFrame with label columns
        """
        # Get name information
        name_split = [re.split(';|,|\*|_|-| ', str(key)) for key in df.meas_name]
        max_sep = max([len(i) for i in name_split]) + 1
        split_label = [f'meas_label_{i + 1}' for i in range(max_sep - 1)]
        for i in name_split:
            if len(i) < max_sep:
                diff_len = max_sep - len(i)
                i.extend(diff_len * [''])
        for i in range(len(split_label)):
            name_label = split_label[i]
            df[name_label] = [name_split[j][i] for j in range(len(name_split))]

        return df

    def add_import_column(self, select_columns):
        """ Add a matching column from spreadsheet import to QRame column
        :param select_columns: ColumnsSelection object
        """
        # Get selected columns
        item_import = select_columns.combo_import_col.currentText()
        item_qrame = select_columns.combo_qrame_col.currentText()
        key_qrame = select_columns.combo_qrame_col.currentData()
        items = [a for b in list(self.matching_col.values()) for a in b]
        if item_import not in items and key_qrame not in self.matching_col.keys():
            # Update table of matching columns
            tbl = select_columns.tbl_import_qrame
            font = QtGui.QFont()
            font.setWeight(75)
            tbl.verticalHeader().hide()
            col_header = tbl.horizontalHeader()
            col_header.setSectionResizeMode(QtWidgets.QHeaderView.Stretch)

            rowPosition = tbl.rowCount()
            tbl.insertRow(rowPosition)

            # Spreadsheet column
            tbl.setItem(rowPosition, 0, QtWidgets.QTableWidgetItem(item_import))
            tbl.item(rowPosition, 0).setFont(font)
            tbl.item(rowPosition, 0).setFlags(QtCore.Qt.ItemIsEnabled)

            # Arrow
            tbl.setItem(rowPosition, 1, QtWidgets.QTableWidgetItem("→"))
            tbl.item(rowPosition, 1).setFont(font)
            tbl.item(rowPosition, 1).setFlags(QtCore.Qt.ItemIsEnabled)

            # QRame column
            tbl.setItem(rowPosition, 2, QtWidgets.QTableWidgetItem(item_qrame))
            tbl.item(rowPosition, 2).setFont(font)
            tbl.item(rowPosition, 2).setFlags(QtCore.Qt.ItemIsEnabled)

            # Disable element in combo box
            idx_import = select_columns.combo_import_col.currentIndex()
            idx_qrame = select_columns.combo_qrame_col.currentIndex()

            # Disable selected columns
            if idx_import > -1:
                select_columns.combo_import_col.model().item(idx_import).setEnabled(False)
                select_columns.combo_qrame_col.model().item(idx_qrame).setEnabled(False)

                self.matching_col[select_columns.combo_qrame_col.currentData()] = [
                    select_columns.combo_import_sheet.currentText(), item_import]

    def remove_import_column(self, select_columns):
        """ Remove spreadsheet - QRame column match
        :param select_columns: ColumnsSelection object
        """
        tbl = select_columns.tbl_import_qrame
        current_row = tbl.currentRow()
        if current_row == -1:
            current_row = tbl.rowCount() - 1
        if current_row > -1:
            item_import = tbl.item(current_row, 0).text()
            item_qrame = tbl.item(current_row, 2).text()
            # Enable element in combo box
            idx_import = select_columns.combo_import_col.findText(item_import)
            idx_qrame = select_columns.combo_qrame_col.findText(item_qrame)

            select_columns.combo_import_col.model().item(idx_import).setEnabled(True)
            select_columns.combo_qrame_col.model().item(idx_qrame).setEnabled(True)

            # Remove item from matching rows
            del self.matching_col[select_columns.combo_qrame_col.itemData(idx_qrame)]

            # Remove selected row
            tbl.removeRow(current_row)

    def change_import_sheet(self, select_columns, doc_import):
        """ Change the selected sheet from imported spreadsheet
        :param select_columns: ColumnsSelection object
        :param doc_import: Pandas DataFrame imported spreadsheet
        """
        # Update import combo box
        import_col_idx = select_columns.combo_import_sheet.currentText()
        select_columns.combo_import_col.clear()
        i = 0
        for key in doc_import[import_col_idx].columns:
            i += 1
            if isinstance(key, str):
                select_columns.combo_import_col.addItem(key)

        # Enable qrame item combo box
        for i in range(select_columns.combo_qrame_col.count()):
            select_columns.combo_qrame_col.model().item(i).setEnabled(True)

        # Clear matching rows
        self.matching_col = {}

        # Remove all rows from table
        tbl = select_columns.tbl_import_qrame
        for i in range(tbl.rowCount() + 1):
            tbl.removeRow(0)

    def thread_extract(self, path_meas=None, type_meas=None, name_meas=None, settings=None,
                       meas_updates=None):
        """ Extract measurements data, thread is used to avoid ui crash during loading time
        :param path_meas: List of measurement paths
        :param type_meas: List of measurement types (SonTek, RDI, QRev)
        :param name_meas: List of measurement names
        :param settings: Dictionary of selected settings
        :param meas_updates: List of measurement names which should be updated
        """
        # Disable menus while loading
        self.enable_menu(way=False)

        # Setup settings
        if settings is None:
            settings_list = ['nav_ref', 'extrap_law_idx', 'extrap_type_idx', 'extrap_exponent', 'extrap_subsection',
                             'use_weighted', 'threshold']
            settings = self.generate_settings(settings_list=settings_list)

        # Initialize thread
        self.worker_extract = LoadMeasurements(parent=self,
                                               path_meas=path_meas,
                                               type_meas=type_meas,
                                               name_meas=name_meas,
                                               settings=settings,
                                               meas_updates=meas_updates,
                                               add_data=self.add_data)

        # Thread connection
        self.pbar_popup = ProgressBar(self, worker=self.worker_extract)

        self.worker_extract.setTerminationEnabled(True)

        self.worker_extract.started.connect(lambda: self.pbar_popup.show())
        self.worker_extract.pbar_value.connect(lambda x: self.pbar_popup.pbar.setValue(x))
        self.worker_extract.pbar_label.connect(lambda x: self.pbar_popup.label.setText(x))
        self.worker_extract.pbar_label2.connect(lambda x: self.pbar_popup.label_2.setText(x))
        self.worker_extract.finished.connect(lambda: self.pbar_popup.close())

        self.worker_extract.finished.connect(self.update_meas)
        self.pbar_popup.close_sig.connect(self.enable_menu)
        self.pbar_popup.close_sig.connect(QApplication.restoreOverrideCursor)

        # Run thread
        self.worker_extract.start()

    def update_meas(self):
        """ Update loaded measurements once extract thread is over
        """
        if self.worker_extract.measurements_dict:
            # Check if a spreadsheet as been saved in a previous QRame session
            measurements_csv = None
            if 'results_import_df' in self.worker_extract.measurements_dict.keys():
                measurements_csv = self.worker_extract.measurements_dict['results_import_df']
                self.worker_extract.measurements_dict.pop('results_import_df', None)

            # Checked measurements
            self.meas_checked = copy.deepcopy(self.worker_extract.meas_checked)

            # Update settings
            self.update_settings(settings=self.worker_extract.settings)

            # Return invalid mesurements
            if len(self.worker_extract.measurement_error) > 0:
                error_txt = ', '.join(word for word in self.worker_extract.measurement_error)
                error = QtWidgets.QMessageBox()
                error.setIcon(QtWidgets.QMessageBox.Critical)
                error.setWindowTitle("Close")
                error.setText(
                    self._translate('Main',
                                    "Error encountered on measurement(s) : "
                                    ) + error_txt
                )
                error.setStandardButtons(QtWidgets.QMessageBox.Ok)
                error.setDefaultButton(QtWidgets.QMessageBox.Ok)
                error.exec()

            # Update dataframe and selected measurements
            if self.add_data:
                # Add data to the current one
                if self.worker_extract.meas_updates is not None:
                    raw_transects_df = self.worker_extract.raw_transects_df[
                        self.worker_extract.raw_transects_df['meas_name'].isin(self.worker_extract.meas_updates)]
                else:
                    raw_transects_df = self.worker_extract.raw_transects_df[
                        self.worker_extract.raw_transects_df['meas_name'].isin(self.meas_checked)]

                self.transects_df = self.transects_df[~self.transects_df['meas_name'].isin(
                    np.unique(raw_transects_df['meas_name']))]
                self.transects_df = self.transects_df.append(raw_transects_df).reset_index(drop=True)

                add_names = np.unique(self.worker_extract.raw_transects_df['meas_name'])
                self.meas_name = np.unique(np.append(self.meas_name, np.array(add_names)))

            elif not self.worker_extract.raw_transects_df.empty:
                if self.transects_df is None:
                    self.transects_df = self.worker_extract.raw_transects_df[
                        self.worker_extract.raw_transects_df['meas_name'].isin(self.meas_checked)]
                    self.meas_name = np.unique(self.worker_extract.raw_transects_df['meas_name'])
                else:
                    if self.worker_extract.meas_updates is not None:
                        new_transects_df = self.worker_extract.raw_transects_df[
                            self.worker_extract.raw_transects_df['meas_name'].isin(self.worker_extract.meas_updates)]
                        raw_transects_df = self.transects_df[
                            self.transects_df['meas_name'].isin(self.worker_extract.meas_updates)]
                        new_transects_df.index = raw_transects_df.index
                        self.transects_df.loc[
                            self.transects_df['meas_name'].isin(self.worker_extract.meas_updates)] = new_transects_df
                    elif self.worker_extract.name_meas is not None:
                        new_transects_df = self.worker_extract.raw_transects_df[
                            self.worker_extract.raw_transects_df['meas_name'].isin(self.worker_extract.name_meas)]
                        raw_transects_df = self.transects_df[
                            ~self.transects_df['meas_name'].isin(self.worker_extract.name_meas)]
                        self.transects_df = raw_transects_df.append(new_transects_df).sort_values(
                            'meas_name').reset_index(drop=True)

            # If a spreadsheet has been saved, add it to the current data
            if measurements_csv is not None:
                self.load_csv(measurements_csv, self.meas_checked)

            # Correct discharge
            self.worker_extract.meas_updates = None
            if self.correct_discharge and self.intercomparison_process:
                self.compute_correction()

            # Run intercomparison process
            self.thread_intercomp()

        else:
            self.reportWarning(text=self._translate('Main', 'No valid ADCP measurement'))
            self.enable_menu(way=True)

        self.add_data = False

    def compute_correction(self):
        """ If discharge stationarity is not reached during an intercomparion, run a discharge correction using
        a LOWESS interpolation.
        """
        if self.correct_discharge and self.intercomparison_process:
            try:
                transects_df = copy.deepcopy(self.transects_df)
                # If a correction has been applied, return to initial discharge
                if 'dq_to_mean' in self.transects_df.columns:
                    transects_df.loc[:, 'dq_to_mean'] = transects_df.loc[:, 'dq_to_mean'].fillna(0)
                    transects_df['tr_q_total'] = transects_df['tr_q_total'] - transects_df.loc[:, 'dq_to_mean']
                    transects_df.loc[:, 'dq_to_mean'] = 0

                transects_df = transects_df.reset_index(drop=True)
                df = transects_df.loc[:, ['tr_q_total', 'meas_mean_v', 'start_time', 'end_time']]

                # Mid time of a transect
                df['mid_time'] = df[['start_time', 'end_time']].mean(axis=1)
                if len(df[~np.isnan(df['mid_time'])]) > 0:
                    # Apply LOWESS interpolation
                    self.lowess_q = sm.nonparametric.lowess(df['tr_q_total'], df['mid_time'],
                                                            frac=0.3, return_sorted=False)
                    # Unpack the lowess smoothed points to their values
                    self.lowess_t = df['mid_time']
                    correction_function = interp1d(self.lowess_t, self.lowess_q, bounds_error=False)

                    # Get the discharge reference
                    if isinstance(self.discharge_reference, str):
                        discharge_reference = self.discharge_mean
                    else:
                        discharge_reference = self.discharge_reference

                    # Define deviation from each transect to reference
                    self.transects_df.loc[:, 'dq_to_mean'] = discharge_reference - \
                                                             correction_function(list(df['mid_time']))
                    self.transects_df.loc[:, 'dq_to_mean'] = self.transects_df.loc[:, 'dq_to_mean'].fillna(0)
                    self.transects_df.loc[:, 'tr_q_total'] = list(transects_df['tr_q_total']) + \
                                                             self.transects_df['dq_to_mean']

                    # Update Meas table
                    meas_df = self.transects_df.groupby(['meas_name'])[['tr_q_total', 'tr_q_bottom', 'tr_q_top',
                                                                        'tr_q_middle', 'tr_q_right',
                                                                        'tr_q_left']].mean()
                    mean_meas = self.transects_df.groupby(['meas_name'])[['meas_mean_q']].mean()
                    q_variation = meas_df['tr_q_total'] / mean_meas['meas_mean_q']
                    self.transects_df['meas_mean_q'] = [
                        meas_df.loc[elem]['tr_q_total'] for elem in self.transects_df.meas_name
                    ]

                    self.transects_df['meas_mean_v'] = self.transects_df['tr_q_total'] / self.transects_df['meas_area']
                    discharge_list = ['tr_q_bottom', 'tr_q_top', 'tr_q_middle', 'tr_q_right', 'tr_q_left']
                    for i in self.transects_df.index:
                        meas_name = self.transects_df.loc[i]['meas_name']
                        for discharge in discharge_list:
                            self.transects_df.loc[i, discharge] = q_variation[meas_name] * \
                                                                  self.transects_df.loc[i][discharge]

                    # Update UI
                    tab_idx = self.tab_all.indexOf(
                        self.tab_all.findChild(QtWidgets.QWidget, "tab_q_correction")
                    )
                    if tab_idx < 0:
                        self.tab_all.addTab(self.tab_q_correction, "Discharge correction")
                else:
                    raise ValueError("No valid data to compute correction.")
            except Exception:
                self.reportWarning(text=self._translate('Main', 'LOWESS correction can not be computed.'))
                self.correct_discharge = False
                # Reset transect dataframe
                self.transects_df = self.transects_df[self.transects_df['meas_name'].isin(self.meas_checked)]
                if 'dq_to_mean' in self.transects_df.columns:
                    self.transects_df.loc[:, 'dq_to_mean'] = self.transects_df.loc[:, 'dq_to_mean'].fillna(0)
                    self.transects_df['tr_q_total'] = self.transects_df['tr_q_total'] - \
                                                      self.transects_df.loc[:, 'dq_to_mean']
                    self.transects_df = self.transects_df.drop('dq_to_mean', axis=1)

                if self.intercomparison_process:
                    # Update UI
                    tab_idx = self.tab_all.indexOf(
                        self.tab_all.findChild(QtWidgets.QWidget, "tab_q_correction")
                    )
                    if tab_idx > 0:
                        self.tab_all.removeTab(tab_idx)
        else:
            # Reset transect dataframe
            self.transects_df = self.transects_df[self.transects_df['meas_name'].isin(self.meas_checked)]
            if 'dq_to_mean' in self.transects_df.columns:
                self.transects_df.loc[:, 'dq_to_mean'] = self.transects_df.loc[:, 'dq_to_mean'].fillna(0)
                self.transects_df['tr_q_total'] = self.transects_df['tr_q_total'] - \
                                                  self.transects_df.loc[:, 'dq_to_mean']
                self.transects_df = self.transects_df.drop('dq_to_mean', axis=1)

            if self.intercomparison_process:
                # Update UI
                tab_idx = self.tab_all.indexOf(
                    self.tab_all.findChild(QtWidgets.QWidget, "tab_q_correction")
                )
                if tab_idx > 0:
                    self.tab_all.removeTab(tab_idx)

    def thread_intercomp(self):
        """ Compute intercomparison calculations using thread.
        """
        # Initialize thread
        self.worker = Intercomparison(
            self.transects_df,
            n_transects=self.empirical_n_transects,
            n_sections=self.empirical_n_sections,
            n_teams=self.empirical_n_teams,
            no_moving_bed=self.no_moving_bed,
            compute_intercomparison=self.intercomparison_process,
            u_bias=self.u_bias,
            u_ref=self.uncertainty_reference,
            columns_grouped=self.columns_grouped[self.column_grouped_idx],
            columns_effects=[self.columns_effects[self.column_effect_section_idx],
                             self.columns_effects[self.column_effect_team_idx]],
        )
        # Connection
        self.worker.finished.connect(self.update_transects)
        # Run thread
        self.worker.start()

    def update_transects(self):
        """ Update data once intercomparison thread is over.
        """
        self.transects_df = copy.deepcopy(self.worker.transects_df)

        self.sorted_name = []
        self.columns = None
        self.change_ui = True

        # Compute mean measurement DataFrame
        self.compute_mean_meas()

        self.transects_df['tr_oursin_u_invalid'] = np.sqrt(self.transects_df['tr_oursin_u_boat'] ** 2 +
                                                           self.transects_df['tr_oursin_u_depth'] ** 2 +
                                                           self.transects_df['tr_oursin_u_water'] ** 2)

        if 'u_bias' in self.mean_meas.columns:
            self.u_bias = np.nanmean(self.mean_meas['u_bias'])

        self.meas_checked = np.unique(self.transects_df['meas_name'])
        self.discharge_mean = self.mean_meas['tr_q_total'].mean()

        self.run_interface()

    def compute_mean_meas(self):
        """ Compute mean measurement DataFrame
        """
        # Groupby on transects DataFrame
        self.mean_meas = self.transects_df.groupby(by='meas_name').mean()
        self.mean_meas['meas_mean_q'] = self.mean_meas['tr_q_total']
        col_names = list(self.transects_df.columns)

        # Add string columns
        labels = [x for x in col_names if 'meas_label' in x]
        labels.extend(['meas_tool', 'meas_manufacturer', 'meas_model', 'meas_navigation', 'meas_top_method',
                       'meas_bot_method', 'meas_serial', 'navref_quality', 'water_quality', 'meas_comments'])
        labels = [i for i in labels if i not in self.mean_meas.columns]
        self.transects_df = self.transects_df.astype(dict.fromkeys(labels, str))
        d = dict.fromkeys(labels, lambda x: pd.Series.mode(x)[0])
        str_df = self.transects_df.groupby(by='meas_name').agg(d)
        self.mean_meas = self.mean_meas.join(str_df, on='meas_name', how='left')

        # Add time columns
        self.mean_meas[['start_time', 'end_time']] = self.transects_df.groupby(by='meas_name').agg(
            {'start_time': 'min', 'end_time': 'max'})

        # Add invalid uncertainty components column
        self.mean_meas['tr_oursin_u_invalid'] = np.sqrt(self.mean_meas['tr_oursin_u_boat'] ** 2 +
                                                        self.mean_meas['tr_oursin_u_depth'] ** 2 +
                                                        self.mean_meas['tr_oursin_u_water'] ** 2)

    @staticmethod
    def disable_other_combo(combo, combo_target, immune=[]):
        """ Avoid that combo and combo_target comboxes use the same value
        :param combo: Combobox
        :param combo_target: Combox thaht should be changed
        :param immune: List of index thath should not be disabled
        """
        # Get current index of combo
        combo_idx = combo.currentIndex()
        # Enable all other values in combo_target
        for i in range(combo_target.count()):
            combo_target.model().item(i).setEnabled(True)
        if combo_target.model().item(combo_idx):
            if combo_idx not in immune:
                combo_target.model().item(combo_idx).setEnabled(False)

    @staticmethod
    def change_effect_team(intercomparison):
        """ If comboboxes from second factor effect are not None, enable sections and teams line edit box
        :param intercomparison: IntercomparisonSettings object
        """
        if intercomparison.combo_effect_section.isEnabled():
            effect_section_index = intercomparison.combo_effect_section.currentIndex()
            if effect_section_index == 0:
                intercomparison.combo_effect_team.setEnabled(False)
                intercomparison.le_number_sections.setEnabled(False)
                intercomparison.le_number_teams.setEnabled(False)
            else:
                intercomparison.combo_effect_team.setEnabled(True)
                intercomparison.le_number_sections.setEnabled(True)
                intercomparison.le_number_teams.setEnabled(True)

    def apply_intercomparison(self):
        """ Interface with current intercomparison settings.
        """
        # Sizing window
        intercomparison = IntercomparisonSettings(self)
        height = int(QtWidgets.QDesktopWidget().screenGeometry(0).height() * 0.55)
        width = int(QtWidgets.QDesktopWidget().screenGeometry(0).width() * 0.30)
        intercomparison.resize(width, height)

        # Connection
        intercomparison.cb_apply_intercomprison_process.clicked.connect(
            lambda: self.change_intercomparison(intercomparison))

        intercomparison.combo_effect_section.currentIndexChanged.connect(
            lambda: self.change_effect_team(intercomparison))

        intercomparison.cb_auto_discharge_ref.clicked.connect(
            lambda: self.change_reference(intercomparison))

        intercomparison.cb_auto_u_bias.clicked.connect(
            lambda: self.change_u_bias(intercomparison))

        intercomparison.combo_effect_section.currentIndexChanged.connect(
            lambda: self.disable_other_combo(intercomparison.combo_effect_section,
                                             intercomparison.combo_effect_team,
                                             immune=[0]))
        intercomparison.combo_effect_team.currentIndexChanged.connect(
            lambda: self.disable_other_combo(intercomparison.combo_effect_team,
                                             intercomparison.combo_effect_section))

        # Initialize if intercomparison process is currently applid or not
        if self.intercomparison_process:
            intercomparison.cb_apply_intercomprison_process.setChecked(True)

            intercomparison.cb_auto_discharge_ref.setEnabled(True)
            if self.auto_discharge_reference:
                intercomparison.cb_auto_discharge_ref.setChecked(True)
            else:
                intercomparison.cb_auto_discharge_ref.setChecked(False)
            if intercomparison.cb_auto_discharge_ref.isChecked():
                intercomparison.le_discharge_ref.setEnabled(False)
            else:
                intercomparison.le_discharge_ref.setEnabled(True)

            intercomparison.le_u_ref.setEnabled(True)
            intercomparison.le_deviation.setEnabled(True)
            intercomparison.le_number_transects.setEnabled(True)

            if self.auto_u_bias:
                intercomparison.cb_auto_u_bias.setChecked(True)
            else:
                intercomparison.cb_auto_u_bias.setChecked(False)

            if intercomparison.cb_auto_u_bias.isChecked():
                intercomparison.le_u_bias.setEnabled(False)
            else:
                intercomparison.le_u_bias.setEnabled(True)

            intercomparison.combo_column_grouped.setEnabled(True)
            intercomparison.cb_correct_discharge.setEnabled(True)

            if self.correct_discharge:
                intercomparison.cb_correct_discharge.setChecked(True)
            else:
                intercomparison.cb_correct_discharge.setChecked(False)

            intercomparison.combo_effect_section.setEnabled(True)

            if intercomparison.combo_effect_section.currentIndex() != 0:
                intercomparison.combo_effect_team.setEnabled(True)
                intercomparison.le_number_sections.setEnabled(True)
                intercomparison.le_number_teams.setEnabled(True)
            else:
                intercomparison.combo_effect_team.setEnabled(False)
                intercomparison.le_number_sections.setEnabled(False)
                intercomparison.le_number_teams.setEnabled(False)
        else:
            intercomparison.cb_apply_intercomprison_process.setChecked(False)

            intercomparison.cb_auto_discharge_ref.setEnabled(False)
            if self.auto_discharge_reference:
                intercomparison.cb_auto_discharge_ref.setChecked(True)
            else:
                intercomparison.cb_auto_discharge_ref.setChecked(False)

            intercomparison.le_discharge_ref.setEnabled(False)
            intercomparison.le_u_ref.setEnabled(False)
            intercomparison.le_deviation.setEnabled(False)
            intercomparison.le_number_transects.setEnabled(False)

            intercomparison.cb_auto_u_bias.setEnabled(False)
            if self.auto_u_bias:
                intercomparison.cb_auto_u_bias.setChecked(True)
            else:
                intercomparison.cb_auto_u_bias.setChecked(False)

            intercomparison.le_u_bias.setEnabled(False)
            intercomparison.combo_column_grouped.setEnabled(False)

            intercomparison.cb_correct_discharge.setEnabled(False)
            if self.correct_discharge:
                intercomparison.cb_correct_discharge.setChecked(True)
            else:
                intercomparison.cb_correct_discharge.setChecked(False)

            intercomparison.combo_effect_section.setEnabled(False)
            intercomparison.combo_effect_team.setEnabled(False)
            intercomparison.le_number_sections.setEnabled(False)
            intercomparison.le_number_teams.setEnabled(False)
            intercomparison.combo_effect_team.setEnabled(False)
            intercomparison.le_number_sections.setEnabled(False)
            intercomparison.le_number_teams.setEnabled(False)

        # Reference discharge
        text = intercomparison.label_3.text().split(' (')[0]
        intercomparison.label_3.setText(text + ' (' + self.units['label_Q'] + ')')
        if isinstance(self.discharge_reference, str):
            if self.discharge_mean is not None:
                intercomparison.le_discharge_ref.setText(str(np.round(self.discharge_mean * self.units['Q'],
                                                                      2)))
            else:
                intercomparison.le_discharge_ref.setText(self.discharge_reference)
        else:
            if intercomparison.cb_auto_discharge_ref.isChecked() and self.discharge_mean is not None:
                intercomparison.le_discharge_ref.setText(str(np.round(self.discharge_mean * self.units['Q']
                                                                      , 2)))
            else:
                intercomparison.le_discharge_ref.setText(str(np.round(self.discharge_reference * self.units['Q']
                                                                      , 2)))

        # Uncertainty reference
        intercomparison.le_u_ref.setText(str(self.uncertainty_reference))

        # Permissible deviation
        intercomparison.le_deviation.setText(str(self.discharge_deviation))

        # Number of transects
        if self.empirical_n_transects is not None:
            intercomparison.le_number_transects.setText(str(self.empirical_n_transects))
        else:
            intercomparison.le_number_transects.setText(str(0))

        # Bias uncertainty
        if self.u_bias is not None:
            intercomparison.le_u_bias.setText(str(np.round(self.u_bias, 2)))
        else:
            intercomparison.le_u_bias.setText("")

        # Grouped analysis
        self.columns_grouped = [None, 'meas_tool', 'meas_manufacturer', 'meas_model', 'meas_serial', 'meas_top_method',
                                'meas_bot_method', 'meas_navigation']
        self.columns_grouped += [k for k, v in self.columns.items() if 'label' in k]
        intercomparison.combo_column_grouped.clear()
        i = 0
        intercomparison.combo_column_grouped.addItem("")
        intercomparison.combo_column_grouped.setItemText(i, 'None')
        for key in self.columns_grouped[1:]:
            i += 1
            intercomparison.combo_column_grouped.addItem("")
            intercomparison.combo_column_grouped.setItemText(i, self.columns[key][0])
        intercomparison.combo_column_grouped.setCurrentIndex(self.column_grouped_idx)

        # Two factor analysis
        intercomparison.le_number_sections.setText(str(self.empirical_n_sections))
        intercomparison.le_number_teams.setText(str(self.empirical_n_teams))
        self.columns_effects = [None]
        self.columns_effects += [k for k, v in self.columns.items() if 'label' in k]
        i = 0
        intercomparison.combo_effect_section.clear()
        intercomparison.combo_effect_team.clear()
        intercomparison.combo_effect_section.addItem("")
        intercomparison.combo_effect_section.setItemText(i, 'None')
        intercomparison.combo_effect_team.addItem("")
        intercomparison.combo_effect_team.setItemText(i, 'None')
        for key in self.columns_effects[1:]:
            i += 1
            intercomparison.combo_effect_section.addItem("")
            intercomparison.combo_effect_section.setItemText(i, self.columns[key][0])
            intercomparison.combo_effect_team.addItem("")
            intercomparison.combo_effect_team.setItemText(i, self.columns[key][0])

        intercomparison.combo_effect_section.setCurrentIndex(self.column_effect_section_idx)
        intercomparison.combo_effect_team.setCurrentIndex(self.column_effect_team_idx)

        # Reset correction
        self.change_correction = False

        # Execute intercomparison
        intercomparison_exec = intercomparison.exec_()
        change_intercomp = False

        # Check for change
        if intercomparison_exec:
            # Discharge reference
            if intercomparison.cb_auto_discharge_ref.isChecked():
                if 'dq_to_mean' in self.transects_df.columns:
                    discharge_reference_temp = np.round(np.nanmean(
                        self.transects_df['tr_q_total'] - self.transects_df['dq_to_mean']), 2)
                else:
                    discharge_reference_temp = np.round(np.nanmean(self.transects_df['tr_q_total']),
                                                        2)
                if self.discharge_reference != discharge_reference_temp:
                    self.change_ui = True
                    if self.correct_discharge:
                        self.change_correction = True
            else:
                discharge_reference_temp = self.discharge_reference
                try:
                    discharge_reference = float(intercomparison.le_discharge_ref.text())
                    if isinstance(discharge_reference_temp, float):
                        discharge_reference_temp = np.round(discharge_reference_temp, 2)
                    if discharge_reference != discharge_reference_temp:
                        discharge_reference_temp = discharge_reference / self.units['Q']
                        self.change_ui = True
                        if self.correct_discharge:
                            self.change_correction = True
                except ValueError:
                    if self.discharge_mean is not None:
                        discharge_reference_temp = copy.deepcopy(self.discharge_mean)
                        intercomparison.cb_auto_discharge_ref.setChecked(True)
                        self.change_ui = True
                        if self.correct_discharge:
                            self.change_correction = True

            # Uncertainty reference
            uncertainty_reference_temp = self.uncertainty_reference
            try:
                uncertainty_reference = float(intercomparison.le_u_ref.text())
                if uncertainty_reference != self.uncertainty_reference:
                    uncertainty_reference_temp = uncertainty_reference
                    self.change_ui = True
            except ValueError:
                uncertainty_reference_temp = 0
                self.change_ui = True

            # Acceptable discharge deviation
            discharge_deviation_temp = self.discharge_deviation
            try:
                discharge_deviation = float(intercomparison.le_deviation.text())
                if discharge_deviation != self.discharge_deviation:
                    discharge_deviation_temp = discharge_deviation
                    self.sticky_settings.set('discharge_deviation', discharge_deviation)
                    self.change_ui = True
            except ValueError:
                discharge_deviation_temp = 0
                self.change_ui = True

            # Empirical n transects
            empirical_n_transects_temp = self.empirical_n_transects
            try:
                n_transects = int(intercomparison.le_number_transects.text())
                if n_transects != self.empirical_n_transects and n_transects >= 1:
                    empirical_n_transects_temp = n_transects
                    change_intercomp = True
            except ValueError:
                if self.empirical_n_transects is not None:
                    empirical_n_transects_temp = None
                    change_intercomp = True

            # Bias uncertainty
            if intercomparison.cb_auto_u_bias.isChecked():
                auto_u_bias_temp = True
                if auto_u_bias_temp != self.auto_u_bias:
                    change_intercomp = True
                    u_bias_temp = None
                    self.auto_u_bias = auto_u_bias_temp
                else:
                    u_bias_temp = self.u_bias
            else:
                auto_u_bias_temp = False
                u_bias_temp = self.u_bias
                try:
                    u_bias = float(intercomparison.le_u_bias.text())
                    if u_bias != self.u_bias or auto_u_bias_temp != self.auto_u_bias:
                        u_bias_temp = u_bias
                        change_intercomp = True
                        self.auto_u_bias = auto_u_bias_temp
                except ValueError:
                    u_bias_temp = self.u_bias

            # Get group selected column
            column_grouped_idx_temp = intercomparison.combo_column_grouped.currentIndex()
            if self.column_grouped_idx != column_grouped_idx_temp:
                self.column_grouped_idx = column_grouped_idx_temp
                change_intercomp = True

            # Get Double effect column
            column_effect_section_idx_temp = intercomparison.combo_effect_section.currentIndex()
            if self.column_effect_section_idx != column_effect_section_idx_temp:
                self.column_effect_section_idx = column_effect_section_idx_temp
                change_intercomp = True
            column_effect_team_idx_temp = intercomparison.combo_effect_team.currentIndex()
            if self.column_effect_team_idx != column_effect_team_idx_temp and \
                    column_effect_team_idx_temp != column_effect_section_idx_temp:
                self.column_effect_team_idx = column_effect_team_idx_temp
                change_intercomp = True

            # Number of sections
            empirical_n_sections_temp = self.empirical_n_sections
            try:
                n_sections = int(intercomparison.le_number_sections.text())
                if n_sections != self.empirical_n_sections:
                    empirical_n_sections_temp = n_sections
                    change_intercomp = True
            except ValueError:
                if self.empirical_n_sections is not None:
                    empirical_n_sections_temp = None
                    change_intercomp = True

            # Number of teams
            empirical_n_teams_temp = self.empirical_n_teams
            try:
                n_teams = int(intercomparison.le_number_teams.text())
                if n_teams != self.empirical_n_teams:
                    empirical_n_teams_temp = n_teams
                    change_intercomp = True
            except ValueError:
                if self.empirical_n_teams is not None:
                    empirical_n_teams_temp = None
                    change_intercomp = True

            # Check intercomparison should be applied
            if intercomparison.cb_apply_intercomprison_process.isChecked():
                if self.intercomparison_process is False:
                    self.intercomparison_process = True
                    change_intercomp = True
            else:
                if self.intercomparison_process is True:
                    self.intercomparison_process = False
                    if self.discharge_mean is not None:
                        discharge_reference_temp = copy.deepcopy(self.discharge_mean)
                    if self.column_grouped_idx != 0:
                        self.column_grouped_idx = 0
                    change_intercomp = True
                else:
                    change_intercomp = False

            # Correct discharge with LOWESS
            correct_discharge_temp = intercomparison.cb_correct_discharge.isChecked() & self.intercomparison_process
            if correct_discharge_temp != self.correct_discharge:
                self.correct_discharge = intercomparison.cb_correct_discharge.isChecked()
                self.change_correction = True
                change_intercomp = True

            # Check if any change
            settings = {'empirical_n_transects': empirical_n_transects_temp,
                        'empirical_n_sections': empirical_n_sections_temp,
                        'empirical_n_teams': empirical_n_teams_temp,
                        'discharge_deviation': discharge_deviation_temp,
                        'discharge_reference': discharge_reference_temp,
                        'uncertainty_reference': uncertainty_reference_temp,
                        'u_bias': u_bias_temp,
                        }

            # Update depending on changes
            if self.change_correction:
                self.enable_menu(way=False)
                self.update_settings(settings)
                self.compute_correction()
                self.thread_intercomp()
            elif change_intercomp and self.transects_df is not None:
                self.enable_menu(way=False)
                self.update_settings(settings)
                self.thread_intercomp()
            elif self.change_ui and self.transects_df is not None:
                self.enable_menu(way=False)
                self.update_settings(settings)
                self.run_interface()
            else:
                self.update_settings(settings)

    @staticmethod
    def change_intercomparison(intercomparison):
        """ Enable/Disable intercomparison interface depending on if cb_apply_intercomprison_process is checked or not.
        :param intercomparison: IntercomparisonSettings object
        """
        if intercomparison.cb_apply_intercomprison_process.isChecked():
            intercomparison.cb_auto_discharge_ref.setEnabled(True)
            if intercomparison.cb_auto_discharge_ref.isChecked():
                intercomparison.le_discharge_ref.setEnabled(False)
            else:
                intercomparison.le_discharge_ref.setEnabled(True)
            intercomparison.le_u_ref.setEnabled(True)
            intercomparison.le_deviation.setEnabled(True)
            intercomparison.le_number_transects.setEnabled(True)
            intercomparison.cb_auto_u_bias.setEnabled(True)
            if intercomparison.cb_auto_u_bias.isChecked():
                intercomparison.le_u_bias.setEnabled(False)
            else:
                intercomparison.le_u_bias.setEnabled(True)
            intercomparison.combo_column_grouped.setEnabled(True)
            intercomparison.cb_correct_discharge.setEnabled(True)
            intercomparison.combo_effect_section.setEnabled(True)
            if intercomparison.combo_effect_section.currentIndex() != 0:
                intercomparison.combo_effect_team.setEnabled(True)
                intercomparison.le_number_sections.setEnabled(True)
                intercomparison.le_number_teams.setEnabled(True)
            else:
                intercomparison.combo_effect_team.setEnabled(False)
                intercomparison.le_number_sections.setEnabled(False)
                intercomparison.le_number_teams.setEnabled(False)
        else:
            intercomparison.cb_auto_discharge_ref.setEnabled(False)
            intercomparison.cb_auto_u_bias.setEnabled(False)
            intercomparison.le_discharge_ref.setEnabled(False)
            intercomparison.le_u_ref.setEnabled(False)
            intercomparison.le_deviation.setEnabled(False)
            intercomparison.le_number_transects.setEnabled(False)
            intercomparison.le_u_bias.setEnabled(False)
            intercomparison.combo_column_grouped.setEnabled(False)
            intercomparison.cb_correct_discharge.setEnabled(False)
            intercomparison.combo_effect_section.setEnabled(False)
            intercomparison.combo_effect_team.setEnabled(False)
            intercomparison.le_number_sections.setEnabled(False)
            intercomparison.le_number_teams.setEnabled(False)

    def change_reference(self, intercomparison):
        """ Check if automatize reference discharge is applied.
        :param intercomparison: IntercomparisonSettings object
        """
        if intercomparison.cb_auto_discharge_ref.isChecked():
            intercomparison.le_discharge_ref.setEnabled(False)
        else:
            intercomparison.le_discharge_ref.setEnabled(True)

        self.auto_discharge_reference = np.logical_not(self.auto_discharge_reference)

    def change_u_bias(self, intercomparison):
        """ Check if automatize uncertainty bias is applied.
        :param intercomparison: IntercomparisonSettings object
        """
        if intercomparison.cb_auto_u_bias.isChecked():
            intercomparison.le_u_bias.setEnabled(False)
        else:
            intercomparison.le_u_bias.setEnabled(True)

        # self.auto_u_bias = np.logical_not(self.auto_u_bias)

    def apply_settings(self):
        """ Main settings interface from Options object.
        """
        # Load Options interface
        options = Options(parent=self)
        height = int(QtWidgets.QDesktopWidget().screenGeometry(0).height() * 0.75)
        width = int(QtWidgets.QDesktopWidget().screenGeometry(0).width() * 0.25)
        options.resize(width, height)

        # Connection
        options.combo_extrap_type.currentIndexChanged.connect(lambda: self.option_type_change(options))
        options.combo_extrap_law.currentIndexChanged.connect(lambda: self.option_automatic_change(options))

        # Language
        if self.language == 'en':
            options.rb_english.setChecked(True)
        else:
            options.rb_french.setChecked(True)

        # Units
        if self.sticky_settings.get('UnitsID') == 'SI':
            options.rb_si_units.setChecked(True)
        else:
            options.rb_english_units.setChecked(True)

        # Navigation reference
        if self.nav_ref == 'default':
            options.rb_default_nav.setChecked(True)
        elif self.nav_ref == 'BT':
            options.rb_bt_nav.setChecked(True)
        else:
            options.rb_gps_nav.setChecked(True)

        # Moving bed
        if self.no_moving_bed:
            options.cb_no_mb.setChecked(True)
        else:
            options.cb_no_mb.setChecked(False)

        # Extrapolation Weighted
        if self.extrap_type_idx == 1:
            options.cb_weighted_extrap.setEnabled(False)
        if self.use_weighted:
            options.cb_weighted_extrap.setChecked(True)
        else:
            options.cb_weighted_extrap.setChecked(False)

        if self.extrap_law_idx == 0:
            options.le_exponent.setEnabled(False)

        options.combo_extrap_type.setCurrentIndex(self.extrap_type_idx)
        options.combo_extrap_law.setCurrentIndex(self.extrap_law_idx)
        options.le_exponent.setText(str(self.extrap_exponent))
        options.le_subsection.setText("{:.0f}:{:.0f}".format(self.extrap_subsection[0], self.extrap_subsection[1]))
        options.le_excluded_dist.setText(str(self.excluded_dist))

        # Execute settings
        options_exec = options.exec_()
        change_extract = False
        change_intercomp = False
        change_nav_ref = False
        if options_exec:
            # Language
            if options.rb_english.isChecked():
                if self.language != 'en':
                    self.language = 'en'
                    self.columns = None
                    self.change_language()
                    self.sticky_settings.set('language', self.language)
                    self.change_ui = True
            else:
                if self.language != 'fr':
                    self.language = 'fr'
                    self.columns = None
                    self.change_language()
                    self.sticky_settings.set('language', self.language)
                    self.change_ui = True
            # Units
            if options.rb_si_units.isChecked():
                if self.sticky_settings.get('UnitsID') != 'SI':
                    self.sticky_settings.set('UnitsID', 'SI')
                    self.units = units_conversion(units_id=self.sticky_settings.get('UnitsID'))
                    self.columns = None
                    self.change_ui = True
            else:
                if self.sticky_settings.get('UnitsID') != 'EN':
                    self.sticky_settings.set('UnitsID', 'EN')
                    self.units = units_conversion(units_id=self.sticky_settings.get('UnitsID'))
                    self.columns = None
                    self.change_ui = True

            # Navigation reference
            nav_ref_temp = self.nav_ref
            if options.rb_default_nav.isChecked():
                if self.nav_ref != 'default':
                    nav_ref_temp = 'default'
                    change_extract = True
                    change_nav_ref = True
            elif options.rb_bt_nav.isChecked():
                if self.nav_ref != 'BT':
                    nav_ref_temp = 'BT'
                    change_extract = True
                    change_nav_ref = True
            else:
                if self.nav_ref != 'GPS':
                    nav_ref_temp = 'GPS'
                    change_extract = True
                    change_nav_ref = True

            # No moving-bed
            no_moving_bed_temp = self.no_moving_bed
            if options.cb_no_mb.isChecked():
                if not self.no_moving_bed:
                    no_moving_bed_temp = True
                    change_intercomp = True
            else:
                if self.no_moving_bed:
                    no_moving_bed_temp = False
                    change_intercomp = True

            # Weighted Extrapolation
            weighted_temp = self.use_weighted
            if options.cb_weighted_extrap.isChecked():
                if not self.use_weighted:
                    weighted_temp = True
                    self.sticky_settings.set('weighted', weighted_temp)
                    change_extract = True
            else:
                if self.use_weighted:
                    weighted_temp = False
                    self.sticky_settings.set('weighted', weighted_temp)
                    change_extract = True

            # Extrapolation Law
            extrap_law_idx_temp = options.combo_extrap_law.currentIndex()
            extrap_type_idx_temp = options.combo_extrap_type.currentIndex()
            le_exponent_txt = options.le_exponent.text()
            le_subsection_txt = options.le_subsection.text()
            le_excluded_dist_txt = options.le_excluded_dist.text()

            # Exponent
            try:
                extrap_exponent_temp = float(le_exponent_txt)
                if np.abs(extrap_exponent_temp) >= 1:
                    extrap_exponent_temp = self.extrap_exponent
            except ValueError:
                if isinstance(self.extrap_exponent, str):
                    extrap_exponent_temp = self.extrap_exponent
                else:
                    extrap_exponent_temp = self._translate('Main', 'Optimized')

            # Extrap law
            if self.extrap_law_idx != extrap_law_idx_temp or \
                    self.extrap_type_idx != extrap_type_idx_temp or \
                    self.extrap_exponent != extrap_exponent_temp:
                change_extract = True

            # Extrap subsection
            try:
                sub_list = le_subsection_txt.split(":")
                subsection = [float(sub_list[0]), float(sub_list[1])]
                if 0 <= subsection[0] <= 100 and subsection[0] < subsection[1] <= 100 and \
                        subsection != self.extrap_subsection:
                    extrap_subsection_temp = subsection
                    change_extract = True
                else:
                    extrap_subsection_temp = copy.deepcopy(self.extrap_subsection)
            except ValueError:
                extrap_subsection_temp = copy.deepcopy(self.extrap_subsection)

            # Excluded distance
            if len(le_excluded_dist_txt) > 0:
                try:
                    excluded_dist = float(le_excluded_dist_txt)
                    if excluded_dist != self.excluded_dist:
                        excluded_dist_temp = excluded_dist
                        change_extract = True
                    else:
                        excluded_dist_temp = copy.deepcopy(self.excluded_dist)
                except ValueError:
                    excluded_dist_temp = copy.deepcopy(self.excluded_dist)
            else:
                excluded_dist_temp = ''
                if excluded_dist_temp != self.excluded_dist:
                    change_extract = True

            # Check if any change
            settings = {'change_nav_ref': change_nav_ref, 'nav_ref': nav_ref_temp,
                        'extrap_law_idx': extrap_law_idx_temp, 'extrap_type_idx': extrap_type_idx_temp,
                        'extrap_exponent': extrap_exponent_temp, 'extrap_subsection': extrap_subsection_temp,
                        'use_weighted': weighted_temp, 'no_moving_bed': no_moving_bed_temp,
                        'excluded_dist': excluded_dist_temp
                        }

            if change_extract and self.worker_extract is not None and self.worker_extract.measurements_dict is not None:
                self.enable_menu(way=False)
                self.worker_extract.meas_updates = [i for i in self.mean_meas.index if i in
                                                    self.worker_extract.measurements_dict.keys()]
                for key in settings:
                    self.worker_extract.settings[key] = settings[key]
                self.worker_extract.start()
                # self.worker_extract.update_measurements(self.meas_checked)
            elif change_intercomp and self.transects_df is not None:
                self.enable_menu(way=False)
                self.update_settings(settings)
                self.thread_intercomp()
            elif self.change_ui and self.transects_df is not None:
                self.enable_menu(way=False)
                self.update_settings(settings)
                self.run_interface()
            else:
                self.update_settings(settings)

    def option_type_change(self, options):
        """ Check if combo_extrap_type index changed, if velocity is selected, disable weighted extrap
        :param options: Options object
        """
        if options.combo_extrap_type.currentIndex() == 0:
            options.cb_weighted_extrap.setEnabled(True)
        else:
            options.cb_weighted_extrap.setEnabled(False)

    def option_automatic_change(self, options):
        """ Check if default QRevint optimized extrapolation law is selected
        :param options: Options object
        """
        # Disable manual extrapolation if optimized law is selected
        if options.combo_extrap_law.currentIndex() == 0:
            options.le_exponent.setEnabled(False)
            options.label_exponent.setEnabled(False)
            options.le_subsection.setEnabled(True)
            options.le_exponent.setText(self._translate('Main', 'Optimized'))
        else:
            options.le_exponent.setEnabled(True)
            options.label_exponent.setEnabled(True)
            options.le_subsection.setEnabled(True)
            options.le_exponent.setText(str(self.extrap_exponent))

    def update_param(self, combo_box, value, fig_to_plot=[0, 1, 2, 3]):
        """ Update boolean attribute depending on if a combo box is checked
        :param combo_box: combobox object
        :param value: string attribute name to change
        :param fig_to_plot: List of figures to plot
        """
        setattr(self, value, combo_box.isChecked())
        self.plot_figure(fig_to_plot)

    def update_settings(self, settings):
        """ Update attributes depending on a settings dictionary
        :param settings: Dictionary of attributes to update
        """
        if self.worker_extract is not None:
            for key, value in settings.items():
                if key == 'extrap_exponent' and isinstance(settings['extrap_exponent'], str):
                    setattr(self, key, self._translate('Options', 'Optimized'))
                    self.worker_extract.settings[key] = self._translate('Options', 'Optimized')
                elif key == 'nav_ref' and isinstance(settings['nav_ref'], int):
                    pass
                else:
                    setattr(self, key, value)
                    self.worker_extract.settings[key] = value
        else:
            for key, value in settings.items():
                setattr(self, key, value)

    def add_meas(self, name_meas):
        """ Add a measurement name to checked measurement and all measurements lists
        :param name_meas: String measurement name to add
        """
        self.meas_checked = np.append(self.meas_checked, name_meas)
        self.meas_name = np.append(self.meas_name, name_meas)

    def generate_settings(self, settings_list):
        """ Get attribute value from a name list
        :param settings_list: List of attribute names
        :return: Dictionary of settings
        """
        settings = {}
        for name in settings_list:
            settings |= {name: getattr(self, name)}
        return settings

    def select_meas(self):
        """ Interface to select/unselect measurement
        """
        if self.meas_checked is not None:
            # Load Measurements2Use window
            meas_2_use = Measurements2Use(self)
            height = int(QtWidgets.QDesktopWidget().screenGeometry(0).height() * 0.6)
            width = int(QtWidgets.QDesktopWidget().screenGeometry(0).width() * 0.3)
            meas_2_use.resize(width, height)

            # Execute measurements2use
            meas_selected = meas_2_use.exec_()
            checked_meas = []
            if meas_selected:
                # Load checked measurements
                for row in range(meas_2_use.tableSelect.rowCount()):
                    if meas_2_use.tableSelect.item(row, 0).checkState() == QtCore.Qt.Checked:
                        checked_meas.append(meas_2_use.tableSelect.item(row, 0).text())

                # Check if selected measurements are the same
                if set(checked_meas) != set(self.meas_checked):
                    QApplication.setOverrideCursor(Qt.WaitCursor)

                    # Measurements to keep
                    keep_meas = list(set(self.meas_checked).intersection(set(checked_meas)))
                    self.transects_df = self.transects_df[self.transects_df['meas_name'].isin(keep_meas)]

                    # Measurements to add
                    load_meas = list(set(checked_meas) - set(self.meas_checked))
                    if self.results_import_df is not None:
                        add_rows = self.results_import_df[self.results_import_df['meas_name'].isin(load_meas)]
                        self.transects_df = self.transects_df.append(add_rows)
                        load_meas = [x for x in load_meas if x not in np.unique(add_rows['meas_name'])]
                        self.add_data = True

                    self.meas_checked = checked_meas

                    # Update data
                    if self.worker_extract is not None and len(load_meas) > 0:
                        self.add_data = True
                        self.worker_extract.meas_checked = checked_meas
                        self.worker_extract.meas_updates = load_meas
                        self.worker_extract.start()
                    else:
                        if self.correct_discharge and self.intercomparison_process:
                            self.compute_correction()

                        self.thread_intercomp()


    def select_col(self):
        """ Select columns of measurements table
        """
        if self.tableMeasurements is not None:
            columns_checked_save = self.columns_checked
            columns_2_use = Columns2Use(self, self.units)
            columns_selected = columns_2_use.exec_()
            if columns_selected:
                # Update selected columns to show
                selected_columns = []
                for row in range(columns_2_use.tableSelect.rowCount()):
                    if columns_2_use.tableSelect.item(row, 0).checkState() == QtCore.Qt.Checked:
                        selected_columns.append(columns_2_use.tableSelect.item(row, 0).data(QtCore.Qt.UserRole))
                columns_checked_new = {key: self.columns[key] for key in selected_columns}

                if set(columns_checked_new) != set(columns_checked_save):
                    self.sorted_name = list(self.mean_meas.index)
                    self.columns_checked = columns_checked_new
                    self.sticky_settings.set('columns_checked', self.columns_checked)
                    self.table_meas()

    def run_interface(self):
        """ Update interface
        """
        QApplication.setOverrideCursor(Qt.WaitCursor)
        self.freeze = True
        # Update tables
        self.load_tables()
        # Get current sorting of measurements table
        self.get_sorted_name()
        # Update graphs
        self.plot_figure()

        # Release UI
        self.menuConnection()
        self.enable_menu(way=True)
        self.change_ui = False
        self.add_data = False
        while QApplication.overrideCursor() is not None:
            QApplication.restoreOverrideCursor()
        self.freeze = False
        print('complete')

    def load_tables(self):
        """ Update tables
        """
        # Left summary table
        self.table_resume()
        # Right measurement table
        self.table_meas()
        # self.figs = [self.discharge_uncertainty_fig, self.discharge_time_fig, self.uncertainty_sources_fig]
        self.tables = [self.tableResume, self.tableMeasurements]
        self.init_tables = False

    def table_resume(self):
        """ Summary table with empirical uncertainty values if intercomparison process is applied
        """
        # Clear table
        self.tableResume.clear()

        # Initialize table
        tbl = self.tableResume
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        if self.intercomparison_process:
            tbl.setRowCount(10)
        else:
            tbl.setRowCount(7)
        tbl.setColumnCount(2)
        tbl.horizontalHeader().hide()
        tbl.verticalHeader().hide()

        # Set up upper 3 rows
        header = tbl.horizontalHeader()
        header.setSectionResizeMode(QtWidgets.QHeaderView.Stretch)
        col_header = tbl.verticalHeader()
        col_header.setSectionResizeMode(QtWidgets.QHeaderView.Stretch)
        col_label = [self._translate("Main", 'Mean Q'),
                     self._translate("Main", 'Standard Deviation Q'),
                     self._translate("Main", 'Mean U(Q) [Oursin]')
                     ]

        # Median Q
        i = 0
        tbl.setItem(i, 0, QtWidgets.QTableWidgetItem(col_label[i]))
        tbl.item(i, 0).setFont(font)
        tbl.item(i, 0).setFlags(QtCore.Qt.ItemIsEnabled)
        tbl.setItem(i, 1,
                    QtWidgets.QTableWidgetItem(
                        '{} {}'.format(scientific_notation(
                            self.discharge_mean * self.units['Q'], 3),
                            self.units['label_Q'])))
        tbl.item(i, 1).setFont(font)
        tbl.item(i, 1).setFlags(QtCore.Qt.ItemIsEnabled)

        # Std Q
        i += 1
        tbl.setItem(i, 0, QtWidgets.QTableWidgetItem(col_label[i]))
        tbl.item(i, 0).setFont(font)
        tbl.item(i, 0).setFlags(QtCore.Qt.ItemIsEnabled)
        tbl.setItem(i, 1,
                    QtWidgets.QTableWidgetItem(
                        '{} {}'.format(
                            scientific_notation(np.nanstd(self.mean_meas['meas_mean_q'] * self.units['Q']), 3),
                            self.units['label_Q'])))
        tbl.item(i, 1).setFont(font)
        tbl.item(i, 1).setFlags(QtCore.Qt.ItemIsEnabled)

        # Mean U OURSIN
        i += 1
        tbl.setItem(i, 0, QtWidgets.QTableWidgetItem(col_label[i]))
        tbl.item(i, 0).setFont(font)
        tbl.item(i, 0).setFlags(QtCore.Qt.ItemIsEnabled)
        tbl.setItem(i, 1, QtWidgets.QTableWidgetItem('{}%'.format(scientific_notation(
            np.nanmean(self.mean_meas['meas_oursin_95']), 3))))
        tbl.item(i, 1).setFont(font)
        tbl.item(i, 1).setFlags(QtCore.Qt.ItemIsEnabled)

        # Bottom rows depending on if intercomparisson process
        if len(self.mean_meas) > 0:
            if self.intercomparison_process:
                if self.columns_effects[self.column_effect_section_idx] is None or \
                        self.columns_effects[self.column_effect_team_idx] is None:
                    col_label.extend([
                        self._translate("Main", 'Empirical : ') + 'N = ' + str(self.empirical_n_transects),
                        self._translate("Main", 'Mean U(Q)'),
                        self._translate("Main", 'Mean U(Q) min'),
                        self._translate("Main", 'Mean U(Q) max'),
                        self._translate("Main", 'Repeatability sr'),
                        self._translate("Main", 'Interlaboratory sL'),
                        self._translate("Main", 'Reproductilibty sR'),
                    ])
                    labels = ['s_L_prct']
                else:
                    empirical_label = self._translate("Main", 'Empirical : ') + \
                                      'N = ' + str(self.empirical_n_transects) + \
                                      ', S = ' + str(self.empirical_n_sections) + \
                                      ', P = ' + str(self.empirical_n_teams)
                    col_label.extend([empirical_label,
                                      self._translate("Main", 'Mean U(Q)'),
                                      self._translate("Main", 'Repeatability sr'),
                                      self._translate("Main", 'Section sS'),
                                      self._translate("Main", 'Participant sP'),
                                      self._translate("Main", 'S-P interaction sSP'),
                                      self._translate("Main", 'Reproductilibty sR'),
                                      ])
                    labels = ['s_S_prct', 's_P_prct', 's_SP_prct']

                # Empirical Title
                i += 1
                tbl.setSpan(i, 0, 1, 2)
                tbl.setItem(i, 0, QtWidgets.QTableWidgetItem(col_label[i]))
                tbl.item(i, 0).setFont(font)
                tbl.item(i, 0).setBackground(QtGui.QColor('lightgray'))
                tbl.item(i, 0).setTextAlignment(QtCore.Qt.AlignCenter)
                tbl.item(i, 0).setFlags(QtCore.Qt.ItemIsEnabled)

                # Empirical mean Uncertainty (95%) for N transects
                i += 1
                tbl.setItem(i, 0, QtWidgets.QTableWidgetItem(col_label[i]))
                tbl.item(i, 0).setFont(font)
                tbl.item(i, 0).setBackground(QtGui.QColor('lightgray'))
                tbl.item(i, 0).setFlags(QtCore.Qt.ItemIsEnabled)
                tbl.setItem(i, 1,
                            QtWidgets.QTableWidgetItem('{}%'.format(scientific_notation(
                                np.nanmean(self.transects_df['U_Q_n']), 3))))
                tbl.item(i, 1).setFlags(QtCore.Qt.ItemIsEnabled)

                if self.columns_effects[self.column_effect_section_idx] is None or self.columns_effects[
                    self.column_effect_team_idx] is None:
                    # Min empirical uncertainty
                    i += 1
                    tbl.setItem(i, 0, QtWidgets.QTableWidgetItem(col_label[i]))
                    tbl.item(i, 0).setFont(font)
                    tbl.item(i, 0).setBackground(QtGui.QColor('lightgray'))
                    tbl.item(i, 0).setFlags(QtCore.Qt.ItemIsEnabled)
                    tbl.setItem(i, 1,
                                QtWidgets.QTableWidgetItem('{}%'.format(scientific_notation(
                                    np.nanmean(self.transects_df['U_Q_n_min']), 3))))
                    tbl.item(i, 1).setFlags(QtCore.Qt.ItemIsEnabled)

                    # Max empirical uncertainty
                    i += 1
                    tbl.setItem(i, 0, QtWidgets.QTableWidgetItem(col_label[i]))
                    tbl.item(i, 0).setFont(font)
                    tbl.item(i, 0).setBackground(QtGui.QColor('lightgray'))
                    tbl.item(i, 0).setFlags(QtCore.Qt.ItemIsEnabled)
                    tbl.setItem(i, 1,
                                QtWidgets.QTableWidgetItem('{}%'.format(scientific_notation(
                                    np.nanmean(self.transects_df['U_Q_n_max']), 3))))
                    tbl.item(i, 1).setFlags(QtCore.Qt.ItemIsEnabled)

                # Empirical uncertainty components
                i += 1
                tbl.setItem(i, 0, QtWidgets.QTableWidgetItem(col_label[i]))
                tbl.item(i, 0).setFont(font)
                tbl.item(i, 0).setFlags(QtCore.Qt.ItemIsEnabled)
                tbl.setItem(i, 1,
                            QtWidgets.QTableWidgetItem('{}%'.format(scientific_notation(
                                np.nanmean(self.transects_df['s_r_prct']), 3))))
                tbl.item(i, 1).setFlags(QtCore.Qt.ItemIsEnabled)

                for j in labels:
                    i += 1
                    tbl.setItem(i, 0, QtWidgets.QTableWidgetItem(col_label[i]))
                    tbl.item(i, 0).setFont(font)
                    tbl.item(i, 0).setFlags(QtCore.Qt.ItemIsEnabled)
                    tbl.setItem(i, 1,
                                QtWidgets.QTableWidgetItem('{}%'.format(scientific_notation(
                                    np.nanmean(self.transects_df[j]), 3))))
                    tbl.item(i, 1).setFlags(QtCore.Qt.ItemIsEnabled)

                i += 1
                tbl.setItem(i, 0, QtWidgets.QTableWidgetItem(col_label[i]))
                tbl.item(i, 0).setFont(font)
                tbl.item(i, 0).setFlags(QtCore.Qt.ItemIsEnabled)
                tbl.setItem(i, 1,
                            QtWidgets.QTableWidgetItem('{}%'.format(scientific_notation(
                                np.nanmean(self.transects_df['s_R_prct']), 3))))
                tbl.item(i, 1).setFlags(QtCore.Qt.ItemIsEnabled)

            else:
                col_label.extend([self._translate("Main", 'Min. Q'),
                                  'Q25',
                                  'Q75',
                                  self._translate("Main", 'Max. Q'),
                                  ])

                # Min Q
                i += 1
                tbl.setSpan(i, 0, 1, 1)
                tbl.setItem(i, 0, QtWidgets.QTableWidgetItem(col_label[i]))
                tbl.item(i, 0).setFont(font)
                tbl.item(i, 0).setBackground(QtGui.QColor('lightgray'))
                tbl.item(i, 0).setFlags(QtCore.Qt.ItemIsEnabled)
                tbl.setItem(i, 1,
                            QtWidgets.QTableWidgetItem(
                                '{} {}'.format(scientific_notation(
                                    np.nanmin(self.mean_meas['meas_mean_q'] * self.units['Q']), 3),
                                    self.units['label_Q'])))
                tbl.item(i, 1).setFont(font)
                tbl.item(i, 1).setFlags(QtCore.Qt.ItemIsEnabled)

                # Quantile 25
                i += 1
                tbl.setItem(i, 0, QtWidgets.QTableWidgetItem(col_label[i]))
                tbl.item(i, 0).setFont(font)
                tbl.item(i, 0).setBackground(QtGui.QColor('lightgray'))
                tbl.item(i, 0).setFlags(QtCore.Qt.ItemIsEnabled)
                tbl.setItem(i, 1,
                            QtWidgets.QTableWidgetItem(
                                '{} {}'.format(scientific_notation(
                                    np.nanquantile(self.mean_meas['meas_mean_q'] * self.units['Q'], 0.25), 3),
                                    self.units['label_Q'])))
                tbl.item(i, 1).setFont(font)
                tbl.item(i, 1).setFlags(QtCore.Qt.ItemIsEnabled)

                # Quantile 75
                i += 1
                tbl.setItem(i, 0, QtWidgets.QTableWidgetItem(col_label[i]))
                tbl.item(i, 0).setFont(font)
                tbl.item(i, 0).setBackground(QtGui.QColor('lightgray'))
                tbl.item(i, 0).setFlags(QtCore.Qt.ItemIsEnabled)
                tbl.setItem(i, 1,
                            QtWidgets.QTableWidgetItem(
                                '{} {}'.format(scientific_notation(
                                    np.nanquantile(self.mean_meas['meas_mean_q'] * self.units['Q'], 0.75), 3),
                                    self.units['label_Q'])))
                tbl.item(i, 1).setFont(font)
                tbl.item(i, 1).setFlags(QtCore.Qt.ItemIsEnabled)

                # Max Q
                i += 1
                tbl.setItem(i, 0, QtWidgets.QTableWidgetItem(col_label[i]))
                tbl.item(i, 0).setFont(font)
                tbl.item(i, 0).setBackground(QtGui.QColor('lightgray'))
                tbl.item(i, 0).setFlags(QtCore.Qt.ItemIsEnabled)
                tbl.setItem(i, 1,
                            QtWidgets.QTableWidgetItem(
                                '{} {}'.format(scientific_notation(
                                    np.nanmax(self.mean_meas['meas_mean_q'] * self.units['Q']), 3),
                                    self.units['label_Q'])))
                tbl.item(i, 1).setFont(font)
                tbl.item(i, 1).setFlags(QtCore.Qt.ItemIsEnabled)

        self.tableResume.resizeColumnToContents(0)
        self.tableResume.setEnabled(True)

    @QtCore.pyqtSlot(int, int)
    def table_meas_double_clicked(self, row, column):
        """ Manages actions caused by the user double clicking in selected columns
            of the table.
        :param row: int row index in table clicked by user
        :param column: int column index in table clicked by user
        """
        if not self.freeze:
            update = False
            # Get selected meas
            clicked_meas = self.tableMeasurements.item(row, 0).text()
            if clicked_meas != self.selected_meas:
                self.selected_meas = clicked_meas
            else:
                self.selected_meas = None

            settings = {}
            # Get column information
            keys = list(self.columns_checked.keys())
            clicked_col = keys[column - 1]
            update_title, _, _, update_type = self.columns_checked[clicked_col]

            if update_type is not None:
                # Update numerical value
                if update_type == 'float':
                    if clicked_col == 'tr_excluded_dist':
                        update_options = 'excluded_dist'
                    elif clicked_col == 'meas_exponent':
                        update_options = 'extrap_exponent'
                    # Ask for float
                    default_value = formatSize(self.mean_meas.loc[clicked_meas, clicked_col], extend='',
                                               n=2)[:-1]
                    update_dialog = TableUpdateValue(self, title=update_title,
                                                     value=default_value)
                    update_entered = update_dialog.exec_()
                    if update_entered:
                        try:
                            if update_dialog.update_value.text() != default_value:
                                value = float(update_dialog.update_value.text())
                                if 0 < value <= 1:
                                    settings |= {update_options: value}
                                    update = True
                        except ValueError:
                            if update_options == 'extrap_exponent':
                                settings |= {'extrap_law_idx': 0}
                                update = True
                        else:
                            pass
                # Update date value
                elif update_type == 'date':
                    if clicked_col == 'start_time':
                        default_value = self.mean_meas.loc[clicked_meas].start_time
                    else:
                        default_value = self.mean_meas.loc[clicked_meas].end_time
                    if np.isnan(default_value):
                        default_value = datetime.utcfromtimestamp(1)
                    else:
                        default_value = datetime.utcfromtimestamp(default_value)
                    update_dialog = TableUpdateDate(self, title=update_title)
                    update_dialog.update_date.setDateTime(default_value)
                    update_entered = update_dialog.exec_()

                    if update_entered:
                        new_date_time = update_dialog.update_date.dateTime()
                        value = (new_date_time.toPyDateTime() - datetime(1970, 1, 1)).total_seconds()
                        if self.worker_extract is not None and clicked_meas in \
                                self.worker_extract.measurements_dict.keys():
                            meas = self.worker_extract.measurements_dict[clicked_meas]
                            if clicked_col == 'start_time':
                                id_transect = meas.checked_transect_idx[0]
                                delta_t = value - meas.transects[id_transect].date_time.start_serial_time
                            else:
                                id_transect = meas.checked_transect_idx[-1]
                                delta_t = value - meas.transects[id_transect].date_time.end_serial_time

                            start_time = []
                            end_time = []
                            for id_transect in meas.checked_transect_idx:
                                meas.transects[id_transect].date_time.start_serial_time += delta_t
                                meas.transects[id_transect].date_time.end_serial_time += delta_t
                                start_time.append(meas.transects[id_transect].date_time.start_serial_time)
                                end_time.append(meas.transects[id_transect].date_time.end_serial_time)

                            self.worker_extract.measurements_dict[clicked_meas] = meas
                            self.update_data(clicked_meas, 'start_time', start_time)
                            self.update_data(clicked_meas, 'end_time', end_time)
                        else:
                            self.transects_df.loc[self.transects_df['meas_name'] == clicked_meas,
                                                  clicked_col] = value
                            self.results_import_df.loc[self.results_import_df['meas_name'] == clicked_meas,
                                                       clicked_col] = value
                        if self.intercomparison_process and self.correct_discharge:
                            self.compute_correction()
                        self.compute_mean_meas()
                        self.run_interface()

                # Update value from a combo box
                elif update_type == 'combo':
                    if self.worker_extract is not None and \
                            clicked_meas in self.worker_extract.measurements_dict.keys():
                        # Init combo values
                        cb_values = None
                        # Meas data
                        meas = self.worker_extract.measurements_dict[clicked_meas]
                        meas_settings = meas.current_settings()
                        # Extrap law
                        if clicked_col in ['meas_top_method', 'meas_bot_method']:
                            update_options = 'extrap_law_idx'
                            cb_values = [self._translate("Options", "Automatic"),
                                         self._translate("Options", "Power - Power"),
                                         self._translate("Options", "Constant - No Slip"),
                                         self._translate("Options", "3-Point - No Slip")]
                            default_value = 0
                            if meas.extrap_fit.fit_method == 'Automatic':
                                default_value = 0
                            elif meas_settings['extrapTop'] == 'Power' and meas_settings['extrapBot'] == 'Power':
                                default_value = 1
                            elif meas_settings['extrapTop'] == 'Constant' and meas_settings[
                                'extrapBot'] == 'No Slip':
                                default_value = 2
                            elif meas_settings['extrapTop'] == '3-Point' and meas_settings[
                                'extrapBot'] == 'No Slip':
                                default_value = 3
                        # Navigation Reference
                        elif clicked_col == 'meas_navigation':
                            update_options = 'nav_ref'
                            gps = True
                            for transect_idx in meas.checked_transect_idx:
                                if meas.transects[transect_idx].boat_vel.gga_vel is None or np.all(
                                        meas.transects[transect_idx].gps.diff_qual_ens[
                                            ~np.isnan(meas.transects[transect_idx].gps.diff_qual_ens)
                                        ] < 2):
                                    gps = False
                                    break
                            if gps:
                                cb_values = ["BT", "GPS"]
                                settings |= {'change_nav_ref': True}

                            if meas_settings['NavRef'] in ['gga_vel', 'vtg_vel']:
                                default_value = 1
                            else:
                                default_value = 0

                        # Change ADCP model
                        elif clicked_col == 'meas_model':
                            update_options = 'meas_model'
                            cb_models = ["M9", "StreamPro", "Rio Grande", "RS5", "RiverPro", "RiverRay", "Unknown"]
                            current_model = meas.transects[meas.checked_transect_idx[0]].adcp.model
                            if current_model in cb_models:
                                default_value = cb_models.index(current_model)
                            else:
                                default_value = 0

                            update_dialog = TableUpdateCombo(values=cb_models, title=update_title)
                            update_dialog.cb_update.setCurrentIndex(default_value)
                            update_entered = update_dialog.exec_()
                            if update_entered:
                                value = cb_models[update_dialog.cb_update.currentIndex()]
                                for id_transect in meas.checked_transect_idx:
                                    meas.transects[id_transect].adcp.model = value

                                self.worker_extract.measurements_dict[clicked_meas] = meas
                                self.update_data(clicked_meas, 'meas_model', value)
                                self.table_meas()
                                if self.columns_grouped == 'meas_model':
                                    self.plot_figure()

                        # Change settings and compute measurement
                        if cb_values is not None:
                            # Update combo box
                            update_dialog = TableUpdateCombo(values=cb_values, title=update_title)
                            update_dialog.cb_update.setCurrentIndex(default_value)
                            update_entered = update_dialog.exec_()
                            if update_entered:
                                value = update_dialog.cb_update.currentIndex()
                                if value != default_value:
                                    settings |= {update_options: value}
                                    update = True

                    else:
                        update_type = 'str'

                # Update string value
                if update_type == 'str' and column > 0:
                    default_value = self.mean_meas.loc[clicked_meas, clicked_col]
                    update_dialog = TableUpdateValue(self, title=update_title, type='str', value=default_value)
                    update_entered = update_dialog.exec_()
                    if update_entered:
                        value = update_dialog.update_value.text()
                        if value != default_value:
                            if self.worker_extract is not None:
                                # Update label user
                                if clicked_col == 'meas_label_user':
                                    # If '=' first, update whole column
                                    if len(value) > 0 and value[0] == '=':
                                        if '+' in value:
                                            cols = value[1:].replace(" ", "").split('+')
                                            for key in self.meas_checked:
                                                key_val = '_'.join(self.mean_meas.loc[key, cols])
                                                if key in self.worker_extract.measurements_dict.keys():
                                                    self.worker_extract.measurements_dict[key].user_label = key_val
                                                self.update_data(key, clicked_col, key_val)
                                            update = True
                                        else:
                                            for key in self.meas_checked:
                                                key_val = value[1:]
                                                if key in self.worker_extract.measurements_dict.keys():
                                                    self.worker_extract.measurements_dict[key].user_label = key_val
                                                self.update_data(key, clicked_col, key_val)
                                            update = True
                                    else:
                                        if clicked_meas in self.worker_extract.measurements_dict.keys():
                                            self.worker_extract.measurements_dict[clicked_meas].user_label = value
                                        self.update_data(clicked_meas, clicked_col, value)
                                # Update serial num
                                elif clicked_col == 'meas_serial':
                                    for transect in self.worker_extract.measurements_dict[clicked_meas].transects:
                                        transect.adcp.serial_num = value
                                    self.update_data(clicked_meas, clicked_col, value)
                            else:
                                self.transects_df.loc[self.transects_df['meas_name'] == clicked_meas,
                                                      clicked_col] = str(value)
                                if self.results_import_df is not None and \
                                        clicked_meas in np.unique(self.results_import_df.meas_name):
                                    self.results_import_df.loc[self.results_import_df['meas_name'] == clicked_meas,
                                                               clicked_col] = str(value)

                        if self.columns_grouped[self.column_grouped_idx] == 'meas_label_user':
                            self.thread_intercomp()
                        else:
                            self.tableMeasurements.setItem(
                                row, column, QtWidgets.QTableWidgetItem(value)
                            )

            # Apply if any change
            if update:
                QApplication.setOverrideCursor(Qt.WaitCursor)
                if clicked_meas is not None:
                    self.worker_extract.meas_updates = [clicked_meas]
                    for key in settings:
                        self.worker_extract.settings[key] = settings[key]
                else:
                    self.worker_extract.meas_updates = []
                self.worker_extract.start()
            else:
                self.plot_figure(fig_to_plot=[0, 1])

    def update_data(self, clicked_meas, col_name, value):
        """ Apply a new value to a specified measurement for a selected column from measurements table
        :param clicked_meas: str Measurement's name
        :param col_name: str Measurements table column's name
        :param value: New value to attribute
        """
        # Update transects DataFrame
        self.worker_extract.raw_transects_df.loc[
            self.worker_extract.raw_transects_df['meas_name'] == clicked_meas, col_name] = value
        self.transects_df.loc[
            self.transects_df['meas_name'] == clicked_meas, col_name] = value
        # Update mean meas DataFrame
        if col_name == 'start_time':
            self.mean_meas.loc[clicked_meas, col_name] = np.nanmin(value)
        elif col_name == 'end_time':
            self.mean_meas.loc[clicked_meas, col_name] = np.nanmax(value)
        else:
            self.mean_meas.loc[clicked_meas, col_name] = value

    def table_meas(self):
        """ Table with measurements information
        """
        # Initialize measurements table
        tbl = self.tableMeasurements
        tbl.clear()
        tbl.setSortingEnabled(False)

        # Compute deviation to reference
        if isinstance(self.discharge_reference, str):
            discharge_reference = self.discharge_mean
        else:
            discharge_reference = self.discharge_reference
        self.mean_meas['deviation_to_ref'] = 100 * (self.mean_meas['meas_mean_q'] - discharge_reference) / \
                                             discharge_reference

        # Compute Z score
        if self.intercomparison_process and discharge_reference * self.discharge_deviation > 0:
            self.mean_meas['score'] = np.abs(300 * (self.mean_meas['meas_mean_q'] - discharge_reference) / \
                                             (discharge_reference * self.discharge_deviation))
        else:
            self.mean_meas['score'] = None

        # Compute normalized deviation
        self.mean_meas['normalized_deviation'] = (self.mean_meas['meas_mean_q'] - discharge_reference) / np.sqrt(
            self.uncertainty_reference ** 2 + self.mean_meas['meas_oursin_95'] ** 2)

        # Columns features
        if self.columns is None:
            self.define_columns()

        # Initialize columns
        if self.columns_checked is None:
            keys = ['meas_model', 'meas_mean_q', 'meas_oursin_95', 'meas_ntransects', 'start_time', 'end_time',
                    'meas_top_method', 'meas_bot_method', 'meas_exponent']
            self.columns_checked = {x: self.columns[x] for x in keys}
        elif self.change_ui:
            checked_col = {}
            for key in self.columns_checked.keys():
                if key in self.columns.keys():
                    checked_col[key] = self.columns[key]
            self.columns_checked = checked_col
            self.change_ui = False

        # Table specifications
        summary_header = {'meas_name': [self._translate("Main", 'Name'), None]}
        summary_header |= self.columns_checked
        header_title = [value[0] for key, value in summary_header.items()]
        ncols = len(summary_header)
        nrows = len(self.mean_meas)
        tbl.setRowCount(nrows)
        tbl.setColumnCount(ncols)
        tbl.setHorizontalHeaderLabels(header_title)

        header = tbl.horizontalHeader()
        header.setSectionResizeMode(0, QtWidgets.QHeaderView.ResizeToContents)
        # header.setSectionResizeMode(0, QtWidgets.QHeaderView.Stretch)
        for i in range(1, ncols):
            header.setSectionResizeMode(i, QtWidgets.QHeaderView.Interactive)

        tbl.setVerticalHeaderLabels(self.mean_meas.index)
        tbl.setSelectionBehavior(QtWidgets.QTableView.SelectRows)
        tbl.verticalHeader().hide()

        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)

        tbl.horizontalHeader().setFont(font)
        tbl.setEditTriggers(QtWidgets.QTableWidget.NoEditTriggers)
        # self.tableMeasurements.horizontalHeader().sortIndicatorOrder()

        # Update table
        if len(self.mean_meas) > 0:
            for row in range(nrows):
                tbl.setItem(row, 0, QtWidgets.QTableWidgetItem(self.mean_meas.index[row]))
                tbl.item(row, 0).setFont(font)
                tbl.item(row, 0).setFlags(QtCore.Qt.ItemIsEnabled)
                col = 1
                for key, values in self.columns_checked.items():
                    # Numerical value and its unit
                    if values[1] is not None:
                        if key == 'tr_oursin_u_movbed' and self.no_moving_bed and self.mean_meas.iloc[row][key] > 0:
                            value = 0.5
                        else:
                            if self.mean_meas.iloc[row][key] is not None:
                                value = self.mean_meas.iloc[row][key] * values[2]
                            else:
                                value = 0
                        text = formatSize(value, extend=values[1])
                        item = NumericItem(text)
                        item.setData(QtCore.Qt.UserRole, value)
                    else:
                        if 'time' in key:
                            if not np.isnan(self.mean_meas.iloc[row][key]):
                                item = QtWidgets.QTableWidgetItem(datetime.utcfromtimestamp(
                                    self.mean_meas.iloc[row][key]).strftime('%Y-%m-%d %H:%M'))
                            else:
                                item = QtWidgets.QTableWidgetItem('0000-00-00 00:00')
                        elif key == 'Z-score':
                            item = QtWidgets.QTableWidgetItem(self.mean_meas.iloc[row][key])
                        elif key in ['cochran_test', 'grubbs_test']:
                            if self.mean_meas.iloc[row][key] == 1:
                                item = QtWidgets.QTableWidgetItem(self._translate("Main", 'Valid'))
                                item.setBackground(QtGui.QColor(54, 237, 0))
                            elif self.mean_meas.iloc[row][key] == 0:
                                item = QtWidgets.QTableWidgetItem(self._translate("Main", 'Isolated'))
                                item.setBackground(QtGui.QColor(255, 204, 0))
                            elif self.mean_meas.iloc[row][key] == -1:
                                item = QtWidgets.QTableWidgetItem(self._translate("Main", 'Invalid'))
                                item.setBackground(QtGui.QColor(255, 0, 77))
                            else:
                                item = QtWidgets.QTableWidgetItem(self._translate("Main", 'None'))
                        elif key in ['navref_quality', 'water_quality']:
                            if self.mean_meas.iloc[row][key] == 'good':
                                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(self._translate("Main", 'Good')))
                                tbl.item(row, col).setBackground(QtGui.QColor(54, 237, 0))
                            elif self.mean_meas.iloc[row][key] == 'caution':
                                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(self._translate("Main", 'Caution')))
                                tbl.item(row, col).setBackground(QtGui.QColor(255, 204, 0))
                            elif self.mean_meas.iloc[row][key] == 'warning':
                                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(self._translate("Main", 'Warning')))
                                tbl.item(row, col).setBackground(QtGui.QColor(255, 0, 77))
                            else:
                                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(self._translate("Main", 'None')))
                        else:
                            if pd.isnull(self.mean_meas.iloc[row][key]):
                                item = QtWidgets.QTableWidgetItem(self._translate("Main", 'Unknown'))
                            else:
                                item = QtWidgets.QTableWidgetItem(self.mean_meas.iloc[row][key])
                    tbl.setItem(row, col, item)

                    # Color cell beackground
                    if key in ['meas_mean_q', 'deviation_to_ref', 'score']:
                        if self.mean_meas.iloc[row]['score'] is not None:
                            if self.mean_meas.iloc[row]['score'] <= 2:
                                tbl.item(row, col).setBackground(QtGui.QColor(54, 237, 0))
                            elif self.mean_meas.iloc[row]['score'] <= 3:
                                tbl.item(row, col).setBackground(QtGui.QColor(255, 204, 0))
                            else:
                                tbl.item(row, col).setBackground(QtGui.QColor(255, 0, 77))
                        if key == 'meas_mean_q':
                            tbl.item(row, col).setFont(font)
                    # tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)
                    col += 1

        tbl.setSortingEnabled(True)

    def define_columns(self):
        """ Initialize columns name and settings as a dict
        """
        # Dict columns [translate col name, label, units, menu type for double clicked]
        self.columns = {
            'meas_manufacturer': [self._translate("Main", 'Manufacturer'), None, 1, None],
            'meas_model': [self._translate("Main", 'Model'), None, 1, 'combo'],
            'meas_tool': [self._translate("Main", 'Tool'), None, 1, 'str'],
            'meas_serial': [self._translate("Main", 'Serial Numb.'), None, 1, 'str'],
            'meas_navigation': [self._translate("Main", 'Nav. Ref.'), None, 1, 'combo'],
            'meas_mean_q': [self._translate("Main", 'Discharge'), self.units['label_Q'], self.units['Q'], None],
            'deviation_to_ref': [self._translate("Main", 'Deviation to ref.'), '%', 1, None],
            'score': [self._translate("Main", 'Z-Score'), '', 1, None],
            'normalized_deviation': [self._translate("Main", 'Normalized deviation'), '', 1, None],
            'meas_oursin_95': [self._translate("Main", 'U Total'), '%', 1, None],
            'meas_std_q': [self._translate("Main", 'Std discharge'), self.units['label_Q'], self.units['Q'], None],
            'meas_mean_v': [self._translate("Main", 'Mean V'), self.units['label_V'], self.units['V'], None],
            'meas_ntransects': [self._translate("Main", 'Nb. transects'), '', 1, None],
            'navref_quality': [self._translate("Main", 'Nav. Quality'), None, 1, None],
            'water_quality': [self._translate("Main", 'Water Quality'), None, 1, None],
            'cochran_test': [self._translate("Main", 'Cochran test'), None, 1, None],
            'grubbs_test': [self._translate("Main", 'Grubbs test'), None, 1, None],
            'start_time': [self._translate("Main", 'Start time'), None, 1, 'date'],
            'end_time': [self._translate("Main", 'End time'), None, 1, 'date'],
            'meas_width': [self._translate("Main", 'Width'), self.units['label_L'], self.units['L'], None],
            'meas_area': [self._translate("Main", 'Area'), self.units['label_A'], self.units['A'], None],
            'meas_depth': [self._translate("Main", 'Depth'), self.units['label_L'], self.units['L'], None],
            'tr_excluded_dist': [self._translate("Main", 'Excluded Dist.'), self.units['label_L'], self.units['L'],
                                 'float'],
            'tr_perimeter': [self._translate("Main", 'Perimeter'), self.units['label_L'], self.units['L'], None],
            'meas_top_method': [self._translate("Main", 'Top law'), None, 1, 'combo'],
            'meas_bot_method': [self._translate("Main", 'Bot law'), None, 1, 'combo'],
            'meas_exponent': [self._translate("Main", 'Exponent'), '', 1, 'float'],
            'meas_alpha': [self._translate("Main", 'Alpha'), '', 1, None],
            'meas_coef': [self._translate("Main", 'Coefficient'), '', 1, None],
            'tr_oursin_u_syst': [self._translate("Main", 'u System'), '%', 1, None],
            'tr_oursin_u_compass': [self._translate("Main", 'u Compass'), '%', 1, None],
            'tr_oursin_u_movbed': [self._translate("Main", 'u MB'), '%', 1, None],
            'tr_oursin_u_ens': [self._translate("Main", 'u Ensembles'), '%', 1, None],
            'tr_oursin_u_meas': [self._translate("Main", 'u Meas Q'), '%', 1, None],
            'tr_oursin_u_invalid': [self._translate("Main", 'u Invalid data'), '%', 1, None],
            'tr_oursin_u_top': [self._translate("Main", 'u Top Q'), '%', 1, None],
            'tr_oursin_u_bot': [self._translate("Main", 'u Bottom Q'), '%', 1, None],
            'tr_oursin_u_left': [self._translate("Main", 'u Left Q'), '%', 1, None],
            'tr_oursin_u_right': [self._translate("Main", 'u Right Q'), '%', 1, None],
            'tr_oursin_u_cov': [self._translate("Main", 'u CV'), '%', 1, None],
            'tr_q_bottom': [self._translate("Main", 'Bottom Q'), self.units['label_Q'], self.units['Q'], None],
            'tr_q_top': [self._translate("Main", 'Top Q'), self.units['label_Q'], self.units['Q'], None],
            'tr_q_middle': [self._translate("Main", 'Mid Q'), self.units['label_Q'], self.units['Q'], None],
            'tr_q_left': [self._translate("Main", 'Left Q'), self.units['label_Q'], self.units['Q'], None],
            'tr_q_right': [self._translate("Main", 'Right Q'), self.units['label_Q'], self.units['Q'], None],
        }

        # Add empirical columns
        if self.intercomparison_process:
            if self.columns_effects[self.column_effect_section_idx] and \
                    self.columns_effects[self.column_effect_team_idx]:
                self.columns |= {
                    'U_Q_n': [self._translate("Main", 'Empirical U(Q, N)'), '%', 1, None],
                    'U_Q_n_min': [self._translate("Main", 'Empirical min. U(Q, N)'), '%', 1, None],
                    'U_Q_n_max': [self._translate("Main", 'Empirical max. U(Q, N)'), '%', 1, None],
                    's_S_prct': [self._translate("Main", 'sS'), '%', 1, None],
                    's_P_prct': [self._translate("Main", 'sP'), '%', 1, None],
                    's_SP_prct': [self._translate("Main", 'sSP'), '%', 1, None],
                    's_r_prct': [self._translate("Main", 'sr'), '%', 1, None],
                    's_R_prct': [self._translate("Main", 'sR'), '%', 1, None],
                    'A_r': [self._translate("Main", 'Ar'), '%', 1, None],
                    'A_R': [self._translate("Main", 'AR'), '%', 1, None]
                }
            else:
                self.columns |= {
                    'U_Q_n': [self._translate("Main", 'Empirical U(Q, N)'), '%', 1, None],
                    'U_Q_n_min': [self._translate("Main", 'Empirical min. U(Q, N)'), '%', 1, None],
                    'U_Q_n_max': [self._translate("Main", 'Empirical max. U(Q, N)'), '%', 1, None],
                    's_L_prct': [self._translate("Main", 'sL'), '%', 1, None],
                    's_r_prct': [self._translate("Main", 'sr'), '%', 1, None],
                    's_R_prct': [self._translate("Main", 'sR'), '%', 1, None],
                    'A_r': [self._translate("Main", 'Ar'), '%', 1, None],
                    'A_R': [self._translate("Main", 'AR'), '%', 1, None]
                }

        # Add label columns
        if self.mean_meas is not None:
            col_names = list(self.mean_meas.columns)
            labels = [x for x in col_names if 'meas_label' in x]
            col_labels = [x[5:] for x in labels]

            for i in range(len(col_labels) - 1):
                self.columns |= {labels[i]: [col_labels[i], None, 1, None]}
        self.columns |= {'meas_label_user': [self._translate("Main", 'User Label'), None, 1, 'str']}
        self.columns |= {'meas_comments': [self._translate("Main", 'Comments'), None, 1, None]}

    def plot_figure(self, fig_to_plot=[0, 1, 2, 3]):
        """ Plot figures in bottom tabs.
        :param fig_to_plot: List of int
        """
        self.tab_all.setEnabled(True)

        # Get sorted names
        if len(self.sorted_name) > 0:
            sorted_name = []
            for index in self.sorted_name:
                if index in self.mean_meas.index:
                    sorted_name.append(index)
            self.sorted_name = sorted_name
            sorted_df = self.mean_meas.reindex(self.sorted_name)
        else:
            sorted_df = self.mean_meas

        # Plot figures
        if 0 in fig_to_plot:
            self.plot_q_uncertainty(sorted_df)
        if 1 in fig_to_plot:
            self.plot_q_time()
        if 2 in fig_to_plot:
            self.plot_uncertainty_decomposition(sorted_df)
        if 3 in fig_to_plot:
            self.plot_q_distribution(sorted_df)

        # Plot LOWESS correction discharge
        if self.intercomparison_process and self.correct_discharge:
            self.plot_q_correction(sorted_df)

        # Save current graphs
        self.canvases = [self.discharge_uncertainty_canvas, self.discharge_time_canvas, self.uncertainty_sources_canvas,
                         self.discharge_distribution_canvas]
        self.figs = [self.discharge_uncertainty_fig, self.discharge_time_fig, self.uncertainty_sources_fig,
                     self.discharge_distribution_fig]
        self.toolbars = [self.discharge_uncertainty_toolbar, self.discharge_time_toolbar,
                         self.uncertainty_sources_toolbar, self.discharge_distribution_toolbar]

        if self.correct_discharge:
            self.canvases.append(self.discharge_correction_canvas)
            self.figs.append(self.discharge_correction_fig)
            self.toolbars.append(self.discharge_correction_toolbar)

        self.actionHomeGraph.setEnabled(True)
        self.actionZoomGraph.setEnabled(True)
        self.actionPanGraph.setEnabled(True)

    def plot_q_uncertainty(self, sorted_df):
        """ Plot measurements discharge and Oursin uncertainty.
        :param sorted_df: Pandas DataFrame sorted mean measurements DataFrame
        """
        if self.discharge_uncertainty_canvas is None:
            self.tab_q_u.setEnabled(True)
            self.discharge_uncertainty_canvas = MplCanvas(parent=self.graphics_discharge_uncertainty,
                                                          width=5, height=5, dpi=80)
            layout = QtWidgets.QVBoxLayout(self.graphics_discharge_uncertainty)
            # Adjust margins of layout to maximize graphic area
            layout.setContentsMargins(1, 1, 1, 1)
            # Add the canvas
            layout.addWidget(self.discharge_uncertainty_canvas)
            # Initialize hidden toolbar for use by graphics controls
            self.discharge_uncertainty_toolbar = NavigationToolbar(self.discharge_uncertainty_canvas, self)
            self.discharge_uncertainty_toolbar.hide()

        # Initialize the discharge figure and assign to the canvas
        self.discharge_uncertainty_fig = FigDischargeUncertainty(canvas=self.discharge_uncertainty_canvas,
                                                                 units=self.units)
        if isinstance(self.discharge_reference, str):
            discharge_reference = self.discharge_mean
        else:
            discharge_reference = self.discharge_reference
        self.discharge_uncertainty_fig.create(sorted_df, deviation=self.discharge_deviation,
                                              intercomparison_process=self.intercomparison_process,
                                              discharge_ref=discharge_reference,
                                              column_grouped=self.columns_grouped[self.column_grouped_idx],
                                              column_sorted=self.column_sorted,
                                              ascending=self.ascending,
                                              selected_meas=self.selected_meas)
        # Draw canvas
        self.discharge_uncertainty_canvas.draw()

    def plot_q_time(self):
        """ Plot chronic discharge
        """
        if self.discharge_time_canvas is None:
            self.tab_q_time.setEnabled(True)
            self.discharge_time_canvas = MplCanvas(parent=self.graphics_discharge_time,
                                                   width=15, height=6, dpi=80)
            layout = QtWidgets.QVBoxLayout(self.graphics_discharge_time)
            # Adjust margins of layout to maximize graphic area
            layout.setContentsMargins(1, 1, 1, 1)
            # Add the canvas
            layout.addWidget(self.discharge_time_canvas)
            # Initialize hidden toolbar for use by graphics controls
            self.discharge_time_toolbar = NavigationToolbar(self.discharge_time_canvas, self)
            self.discharge_time_toolbar.hide()

        # Initialize the chronic figure and assign to the canvas
        self.discharge_time_fig = FigTimeDischarge(canvas=self.discharge_time_canvas, units=self.units)
        self.discharge_time_fig.create(self.transects_df, selected_meas=self.selected_meas)
        # Draw canvas
        self.discharge_time_canvas.draw()

    def plot_uncertainty_decomposition(self, sorted_df):
        """ Plot OURSIN uncertainty decomposition (Despax et al., 2023)
        :param sorted_df: Pandas DataFrame sorted mean measurements DataFrame
        """
        if self.uncertainty_sources_canvas is None:
            self.tab_q_u.setEnabled(True)
            self.uncertainty_sources_canvas = MplCanvas(parent=self.graphics_uncertainty_sources,
                                                        width=15, height=6, dpi=80)
            layout = QtWidgets.QVBoxLayout(self.graphics_uncertainty_sources)
            # Adjust margins of layout to maximize graphic area
            layout.setContentsMargins(1, 1, 1, 1)
            # Add the canvas
            layout.addWidget(self.uncertainty_sources_canvas)
            # Initialize hidden toolbar for use by graphics controls
            self.uncertainty_sources_toolbar = NavigationToolbar(self.uncertainty_sources_canvas, self)
            self.uncertainty_sources_toolbar.hide()

        # Initialize the uncertainty figure and assign to the canvas
        self.uncertainty_sources_fig = FigUncertaintySources(canvas=self.uncertainty_sources_canvas)
        self.uncertainty_sources_fig.create(sorted_df, self.no_moving_bed,
                                            column_grouped=self.columns_grouped[self.column_grouped_idx],
                                            column_sorted=self.column_sorted,
                                            ascending=self.ascending)
        # Draw canvas
        self.uncertainty_sources_canvas.draw()

    def plot_q_distribution(self, sorted_df):
        """ Plot transect distribution according to name or selected grouped column
        :param sorted_df: Pandas DataFrame sorted mean measurements DataFrame
        """
        if self.discharge_distribution_canvas is None:
            self.tab_q_u.setEnabled(True)
            self.discharge_distribution_canvas = MplCanvas(parent=self.graphics_discharge_uncertainty,
                                                           width=5, height=5, dpi=80)
            layout = QtWidgets.QVBoxLayout(self.graphics_discharge_distribution)
            # Adjust margins of layout to maximize graphic area
            layout.setContentsMargins(1, 1, 1, 1)
            # Add the canvas
            layout.addWidget(self.discharge_distribution_canvas)
            # Initialize hidden toolbar for use by graphics controls
            self.discharge_distribution_toolbar = NavigationToolbar(self.discharge_distribution_canvas, self)
            self.discharge_distribution_toolbar.hide()

        # Initialize the distribution figure and assign to the canvas
        self.discharge_distribution_fig = FigDischargeDistribution(canvas=self.discharge_distribution_canvas,
                                                                   units=self.units)
        if isinstance(self.discharge_reference, str):
            discharge_reference = self.discharge_mean
        else:
            discharge_reference = self.discharge_reference
        self.discharge_distribution_fig.create(self.transects_df, sorted_df, deviation=self.discharge_deviation,
                                               discharge_ref=discharge_reference,
                                               column_grouped=self.columns_grouped[self.column_grouped_idx],
                                               column_sorted=self.column_sorted,
                                               ascending=self.ascending,
                                               violin=self.q_distribution_violin,
                                               show_nb_transects=self.q_distribution_nb_transects,
                                               score_color=self.q_distribution_color_score)
        # Draw canvas
        self.discharge_distribution_canvas.draw()

    def plot_q_correction(self):
        """ Plot LOWESS discharge correction
        """
        if self.discharge_correction_canvas is None:
            self.tab_q_correction.setEnabled(True)
            self.discharge_correction_canvas = MplCanvas(parent=self.graphics_discharge_correction,
                                                         width=15, height=6, dpi=80)
            layout = QtWidgets.QVBoxLayout(self.graphics_discharge_correction)
            # Adjust margins of layout to maximize graphic area
            layout.setContentsMargins(1, 1, 1, 1)
            # Add the canvas
            layout.addWidget(self.discharge_correction_canvas)
            # Initialize hidden toolbar for use by graphics controls
            self.discharge_correction_toolbar = NavigationToolbar(self.discharge_correction_canvas, self)
            self.discharge_correction_toolbar.hide()

        # Initialize the correction figure and assign to the canvas
        self.discharge_correction_fig = FigDischargeCorrection(canvas=self.discharge_correction_canvas,
                                                               units=self.units, )
        # self.discharge_time_fig.create(sorted_df)
        self.discharge_correction_fig.create(self.transects_df, self.lowess_t, self.lowess_q,
                                             reference_discharge=self.discharge_reference,
                                             selected_meas=self.selected_meas,
                                             )
        # Draw canvas
        self.discharge_correction_canvas.draw()

    def sort_figure_x(self, col_idx):
        """ Sort figures depending on the selected column
        :param col_idx: Int column index
        """
        if col_idx > 0:
            if self.column_sorted_idx == col_idx:
                self.ascending = not self.ascending
            else:
                self.column_sorted_idx = col_idx
                self.ascending = True
            self.column_sorted = list(self.columns_checked.keys())[col_idx - 1]
        else:
            if self.column_sorted_idx:
                self.column_sorted_idx = None
                self.ascending = True
            else:
                self.ascending = np.logical_not(self.ascending)
            self.column_sorted = None

        self.get_sorted_name()

        self.plot_figure()

    def get_sorted_name(self):
        """ Get sorted names following sorted measurements table
        """
        self.sorted_name = []
        for i in range(self.tableMeasurements.rowCount()):
            self.sorted_name.append(self.tableMeasurements.item(i, 0).text())

    # Menu button
    def enable_menu(self, way=True):
        """ Enable and disable actions and menus
        :param way: bool If True enable button, if False disable
        """
        if way:
            self.freeze = False
            self.menuFile.setEnabled(True)
            self.actionOpen_data.setEnabled(True)
            self.actionOpen_results.setEnabled(True)

            self.actionOpen.setEnabled(True)
            self.actionSettings.setEnabled(True)

            if self.transects_df is not None:
                self.actionIntercomparison.setEnabled(True)
                self.actionAddMeasurement.setEnabled(True)
                self.actionChecked.setEnabled(True)
                self.actionTable.setEnabled(True)
                self.actionHomeGraph.setEnabled(True)
                self.actionZoomGraph.setEnabled(True)
                self.actionPanGraph.setEnabled(True)

                # self.actionImport_hydro2.setEnabled(True)
                self.actionExport_csv.setEnabled(True)
                # self.actionExport_bareme.setEnabled(True)
                # self.actionExport_baratinage.setEnabled(True)
                self.actionSave_pickle.setEnabled(True)
                self.actionKML.setEnabled(True)
        else:
            self.freeze = True
            self.menuFile.setEnabled(False)
            self.actionOpen_data.setEnabled(False)
            self.actionOpen_results.setEnabled(False)

            self.actionOpen.setEnabled(False)
            self.actionSettings.setEnabled(False)
            self.actionIntercomparison.setEnabled(False)
            self.actionAddMeasurement.setEnabled(False)
            self.actionChecked.setEnabled(False)
            self.actionTable.setEnabled(False)

            self.actionImport_hydro2.setEnabled(False)
            self.actionExport_csv.setEnabled(False)
            self.actionExport_bareme.setEnabled(False)
            self.actionExport_baratinage.setEnabled(False)
            self.actionSave_pickle.setEnabled(False)
            self.actionKML.setEnabled(False)

            self.actionHomeGraph.setEnabled(False)
            self.actionZoomGraph.setEnabled(False)
            self.actionPanGraph.setEnabled(False)

    def renameMeas(self):
        """ Rename a measurement
        """
        # Initialize dialog
        name = self.tableMeasurements.currentItem().text()
        update_dialog = TableUpdateValue(self, title=self._translate("Main", "Update name"), type='str', value=name)
        update_entered = update_dialog.exec_()
        if update_entered:
            new_name = update_dialog.update_value.text()
            self.selected_meas = None

            # Check if name is already present
            if new_name in self.worker_extract.measurements_dict.keys() or \
                    ('meas_name' in self.results_import_df.columns and new_name in set(
                        self.results_import_df.meas_name)):
                self.reportWarning(text=self._translate('Main', 'Measurement name is already present. '
                                                                'Please change it first.'))

            else:
                self.transects_df.loc[self.transects_df.meas_name == name, 'meas_name'] = new_name
                self.transects_df = self.define_labels_value(self.transects_df)
                self.compute_mean_meas()

                if name in self.worker_extract.measurements_dict.keys():
                    self.worker_extract.measurements_dict[new_name] = self.worker_extract.measurements_dict.pop(name)

                if 'meas_name' in self.worker_extract.raw_transects_df.columns and \
                        name in set(self.worker_extract.raw_transects_df.meas_name):
                    self.worker_extract.raw_transects_df.loc[self.worker_extract.raw_transects_df.meas_name == name,
                                                             'meas_name'] = new_name
                    self.worker_extract.raw_transects_df = self.define_labels_value(
                        self.worker_extract.raw_transects_df)
                elif 'meas_name' in self.results_import_df.columns and name in set(self.results_import_df.meas_name):
                    self.results_import_df.loc[self.results_import_df.meas_name == name, 'meas_name'] = new_name
                    self.results_import_df = self.define_labels_value(self.results_import_df)

                if name in self.meas_checked:
                    index = np.argwhere(self.meas_checked == name)
                    self.meas_checked = np.delete(self.meas_checked, index)
                    self.meas_checked = np.append(self.meas_checked, new_name)
                    self.meas_checked = np.sort(self.meas_checked)

                index = np.argwhere(self.meas_name == name)
                self.meas_name = np.delete(self.meas_name, index)
                self.meas_name = np.append(self.meas_name, new_name)
                self.meas_name = np.sort(self.meas_name)
                self.run_interface()

    def updateMeas(self):
        """ Update a measurement from another file (*.mmt, *.mat)
        """
        clicked_meas = self.tableMeasurements.currentItem().text()
        if self.worker_extract is not None and clicked_meas in self.worker_extract.measurements_dict:
            dialog = SelectFileDialog(open_type=['adcp'], parent=self)
            if dialog.path_file is not None:
                self.worker_extract.path_meas = [dialog.path_file]
                self.worker_extract.type_meas = [dialog.type_file]
                self.worker_extract.name_meas = [clicked_meas]
                self.worker_extract.start()

    def deleteMeas(self):
        """ Delete a measurement
        """
        name = self.tableMeasurements.currentItem().text()
        self.transects_df = self.transects_df[self.transects_df.meas_name != name]
        self.transects_df.reset_index(drop=True)
        self.compute_mean_meas()

        if name in self.worker_extract.measurements_dict.keys():
            self.worker_extract.measurements_dict.pop(name)

        if name in set(self.worker_extract.raw_transects_df.meas_name):
            self.worker_extract.raw_transects_df = self.worker_extract.raw_transects_df[
                self.worker_extract.raw_transects_df.meas_name != name]
            self.worker_extract.raw_transects_df.reset_index(drop=True)
        elif name in set(self.results_import_df.meas_name):
            self.results_import_df = self.results_import_df[self.results_import_df.meas_name != name]
            self.results_import_df.reset_index(drop=True)

        if name in self.meas_checked:
            index = np.argwhere(self.meas_checked == name)
            self.meas_checked = np.delete(self.meas_checked, index)

        index = np.argwhere(self.meas_name == name)
        self.meas_name = np.delete(self.meas_name, index)

        self.run_interface()

    def import_hydro2(self):
        """ Import HYDRO2 file as .dat,
        """
        select_save = SelectFileDialog(parent=self, open_type=['dat'])
        if select_save.path_file is not None and len(select_save.path_file) > 0:
            try:
                with open(select_save.path_file) as f:
                    lines = f.readlines()

                row = []
                for line in lines[2:-1]:
                    row.append(line.rstrip('\n').split(';')[:23])

                data_brm = pd.DataFrame(row, columns=['C', 'JGG', 'station_name',
                                                      'num', 'date', 'start',
                                                      'end', 'cote', 'cote_start',
                                                      'cote_end', 'code_station',
                                                      'debit', 'incertitude',
                                                      'distance_station', 'sect_mouillee',
                                                      'peri_mouille', 'larg_miroir',
                                                      'vitesse_moyenne', 'vitesse_maxi',
                                                      'vitesse_maxi_surface', 'deniv',
                                                      'commentaire', 'mode']
                                        )
                data_brm = data_brm[data_brm['mode'] == 'PC']
                data_brm['time_start'] = pd.to_datetime(data_brm['date'] + ' ' + data_brm['start'],
                                                        format='%Y/%m/%d %H:%M').astype('datetime64[ns]')
                data_brm['time_end'] = pd.to_datetime(data_brm['date'] + ' ' + data_brm['end'],
                                                      format='%Y/%m/%d %H:%M').astype('datetime64[ns]')
                data_brm['time_mid'] = [data_brm.iloc[i]['time_start'] + (data_brm.iloc[i]['time_end'] -
                                                                          data_brm.iloc[i]['time_start']) / 2 for i in
                                        range(len(data_brm['time_start']))]
                self.data_bareme = data_brm
                self.name_bareme = (select_save.path_file).split(os.sep)[-1]
            except:
                print('invalid bareme import')

    def export_csv(self):
        """ Save current results as .csv
        """
        # Open dialog
        select_save = SaveDialog(save_type='csv', parent=self)
        if len(select_save.full_Name) > 0:
            self.path_save = os.path.abspath(os.path.dirname(select_save.full_Name))
            self.sticky_settings.set('path_save', self.path_save)
            # Get DataFrame
            df = copy.deepcopy(self.mean_meas[self.columns.keys()])
            try:
                col_names = []
                for col in self.columns.keys():
                    col_names.append(self.columns[col][0])
                    if col == 'tr_oursin_u_movbed' and self.no_moving_bed:
                        for row in df.index:
                            if self.mean_meas.loc[row]['meas_navigation'] == 'BT':
                                df.loc[row, 'tr_oursin_u_movbed'] = 0.5
                df.index.names = [self._translate("Main", 'Name')]
                df = df.set_axis(col_names, axis=1, copy=False)
                # Save DataFrame
                df.to_csv(select_save.full_Name, sep=';', index=True, encoding="utf-8-sig")
            except PermissionError:
                self.reportWarning(text=self._translate('Main',
                                                        'Permission denied : check if csv file is not already open.'))

    def export_bareme(self):
        """ Function from actionSave : save current results as .csv
        """
        select_save = SaveDialog(save_type='bareme', parent=self)
        print('bareme')

    def export_baratinage(self):
        """ Function from actionSave : save current results as .csv
        """
        select_save = SaveDialog(save_type='baratinage', parent=self)
        print('Baratinage')

    def save_pickle(self):
        """ Save class and its subclass as pickle
        """
        # Open dialog
        select_save = SaveDialog(save_type='pickle', parent=self)
        if len(select_save.full_Name) > 0:
            QApplication.setOverrideCursor(Qt.WaitCursor)
            self.path_save = os.path.abspath(os.path.dirname(select_save.full_Name))
            self.sticky_settings.set('path_save', self.path_save)

            # Get measurement dictionary from worker extract
            measurements_dict = {}
            if self.worker_extract is not None:
                measurements_dict = copy.deepcopy(self.worker_extract.measurements_dict)
                measurements_dict['meas_checked'] = self.meas_checked

            # Get spreadsheet
            if self.results_import_df is not None:
                measurements_dict['results_import_df'] = self.results_import_df

            # Save current settings
            settings = ['nav_ref', 'extrap_law_idx', 'extrap_type_idx', 'extrap_exponent', 'extrap_subsection',
                        'use_weighted', 'threshold', 'no_moving_bed', 'intercomparison_process',
                        'auto_discharge_reference', 'discharge_reference', 'uncertainty_reference', 'u_bias',
                        'auto_u_bias', 'empirical_n_transects', 'empirical_n_sections', 'empirical_n_teams',
                        'discharge_deviation', 'correct_discharge']
            measurements_dict['meas_settings'] = self.generate_settings(settings_list=settings)

            # Save as pickle
            with open(select_save.full_Name, 'wb') as handle:
                pickle.dump(measurements_dict, handle, protocol=pickle.HIGHEST_PROTOCOL)

            self.setWindowTitle(self.QRame_version + ' : ' + select_save.full_Name)
            QApplication.restoreOverrideCursor()

    def export_kml(self):
        """ Export ADCP track for measurement with GPS as kml
        """
        # Open dialog
        select_save = SaveDialog(save_type='kml', parent=self)
        if len(select_save.full_Name) > 0:
            any_line = False
            kml = simplekml.Kml(open=1)
            for name, meas in self.worker_extract.measurements_dict.items():
                gps = True
                for transect_idx in meas.checked_transect_idx:
                    if meas.transects[transect_idx].boat_vel.gga_vel is None or np.all(
                            meas.transects[transect_idx].gps.diff_qual_ens[
                                ~np.isnan(meas.transects[transect_idx].gps.diff_qual_ens)
                            ] < 2):
                        gps = False
                        break
                if gps:
                    any_line = True
                    meas_folder = kml.newfolder(name=name)
                    for transect_idx in meas.checked_transect_idx:
                        if meas.transects[transect_idx].gps is not None:
                            lon = meas.transects[transect_idx].gps.gga_lon_ens_deg
                            lon = lon[np.logical_not(np.isnan(lon))]
                            lat = meas.transects[transect_idx].gps.gga_lat_ens_deg
                            lat = lat[np.logical_not(np.isnan(lat))]
                            line_name = meas.transects[transect_idx].file_name[:-4]
                            lon_lat = tuple(zip(lon, lat))
                            _ = meas_folder.newlinestring(name=line_name, coords=lon_lat)

            if any_line:
                kml.save(select_save.full_Name)

                try:
                    os.startfile(select_save.full_Name)
                except os.error:
                    self.reportWarning(
                        text=self._translate(
                            "Google Earth is not installed or is not associated with "
                            "kml files."
                        )
                    )
            else:
                self.reportWarning(
                    text=self._translate(
                        "No measurements equipped with GPS."
                    )
                )


    @staticmethod
    def reportWarning(text):
        """ Display a message box with messages specified in text
        :param text: str Message to be displayed.
        """
        if text:
            msg = QtWidgets.QMessageBox()
            msg.setIcon(QtWidgets.QMessageBox.Warning)
            msg.setText("Error")
            msg.setInformativeText(text)
            msg.setWindowTitle("Error")
            msg.exec_()

    def home(self):
        """ Global graph home action.
        """
        for tb in self.toolbars:
            tb.home()

    def zoom(self):
        """ Global graph zoom action.
        """
        for tb in self.toolbars:
            tb.zoom()
        self.actionHomeGraph.setChecked(False)
        self.actionPanGraph.setChecked(False)

    def pan(self):
        """ Global graph pan action.
        """
        for tb in self.toolbars:
            tb.pan()
        self.actionHomeGraph.setChecked(False)
        self.actionZoomGraph.setChecked(False)

    def menuConnection(self):
        """ Connect ui to event filter for current figures.
        """
        for fig in self.figs:
            fig.canvas.parent().installEventFilter(self)
        self.tableMeasurements.viewport().installEventFilter(self)

    def eventFilter(self, source, event):
        """ Load events from application
        :param source: Location of the event
        :param event: Event applied
        """
        if event.type() == QtCore.QEvent.ContextMenu:
            if source in [i.parent() for i in self.canvases]:
                self.current_fig = self.figs[self.tab_all.currentIndex()]
                self.current_canvas = self.canvases[self.tab_all.currentIndex()]
                self.figsMenu.exec_(event.globalPos())
                return True
            elif source == self.tableMeasurements.viewport():
                item = self.tableMeasurements.itemAt(event.pos())
                if item.column() == 0:
                    self.tableMenu.exec_(event.globalPos())
                return True
        return super().eventFilter(source, event)

    def saveFig(self):
        """ Save selected figure.
        """
        if self.current_fig is not None:
            # Get the current folder setting.
            save_fig = SaveDialog(parent=self, save_type='fig')
            # Save figure in user format
            if len(save_fig.full_Name) > 0 or self.current_fig == self.figs[1]:
                fig_to_save = copy.deepcopy(self.current_fig.fig)
                if fig_to_save.ax.get_xlim()[1] - fig_to_save.ax.get_xlim()[0] <= 100:
                    fig_to_save.set_size_inches(12, 6)
                # fig_to_save.tight_layout()
                self.path_save = os.path.abspath(os.path.dirname(save_fig.full_Name))
                self.sticky_settings.set('path_save', self.path_save)
                fig_to_save.savefig(save_fig.full_Name, dpi=300)  # , bbox_inches='tight'

    def change_scale(self):
        """ Change figures scale
        """
        if self.current_fig is not None:
            # Change y-scale
            if self.current_fig.fig.ax.get_yscale() == 'log':
                self.current_fig.fig.ax.set_yscale('linear')
            else:
                self.current_fig.fig.ax.set_yscale('log')
            self.current_canvas.draw()

    def change_language(self):
        """ Change language
        """
        if self.language == 'fr':
            # Load translator file from current directory for script execution
            # self.trans.load('en-fr')
            # Load translator file from resource path for executable
            self.trans.load(resource_path('en-fr'))
            QApplication.instance().installTranslator(self.trans)
        else:
            QApplication.instance().removeTranslator(self.trans)

    def changeEvent(self, event):
        """ Get changing event and translate UI
        :param event: event from application
        """
        if event.type() == QtCore.QEvent.LanguageChange:
            self.retranslateUi(self)
        super(ApplicationWindow, self).changeEvent(event)

    def closeEvent(self, event):
        """ Close application
        :param event: closing event
        """
        if self.worker_extract is not None:
            close = QtWidgets.QMessageBox()
            close.setIcon(QtWidgets.QMessageBox.Warning)
            close.setWindowTitle(self._translate('Main', "Close QRame"))
            close.setText(
                self._translate('Main',
                                "If you haven't saved your data, "
                                "changes will be lost. \n Are you sure you want to Close? "
                                )
            )
            close.setStandardButtons(
                QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.Cancel
            )
            close.setDefaultButton(QtWidgets.QMessageBox.Cancel)
            close.button(close.Yes).setText(self._translate("Main", "Yes"))
            close.button(close.Cancel).setText(self._translate("Main", "Cancel"))
            close = close.exec()

            if close == QtWidgets.QMessageBox.Yes:
                if self.pbar_popup:
                    self.pbar_popup.close()
                event.accept()
            else:
                event.ignore()
        else:
            event.accept()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    application = ApplicationWindow()
    application.show()
    sys.exit(app.exec_())
    app.exec()
