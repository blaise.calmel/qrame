"""
QRame
Copyright (C) 2023  INRAE

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from PyQt5 import QtWidgets, QtGui, QtCore
import wProgressBar


class ProgressBar(QtWidgets.QDialog, wProgressBar.Ui_ProgressBar):
    """Dialog to allow users to change QRev options.

    Parameters
    ----------
    wOptions.Ui_Options : QDialog
        Dialog window with options for users

    Attributes
    ----------
    parent:
        Identifies parent GUI.
    worker: QThread object
        Parent worker object
    """

    def __init__(self, parent=None, worker=None):
        """Initialize options dialog.
        """
        super(ProgressBar, self).__init__()
        self.setupUi(self)
        self.parent = parent

        self.parent = parent
        self.worker = worker
        self.isBar = True
        self._translate = parent._translate

        # self.setGeometry(300, 300, width, height)
        # self.setWindowTitle(self._translate('Main', 'Processing...'))
        # self.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
        self.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)

    close_sig = QtCore.pyqtSignal()

    def closeEvent(self, event):
        """ Stop thread if progress bar is closed
        event:
            Closing event
        """
        if not self.worker._complete:
            close = QtWidgets.QMessageBox()
            close.setIcon(QtWidgets.QMessageBox.Warning)
            close.setWindowTitle(self._translate("Main", "Stop extraction"))
            close.setText(
                self._translate("Main", "You are about to cancel ADCP measurements extraction. Are you sure ?")
            )
            close.setStandardButtons(
                QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.Cancel
            )
            close.setDefaultButton(QtWidgets.QMessageBox.Yes)
            close.button(close.Yes).setText(self._translate("Main", "Yes"))
            close.button(close.Cancel).setText(self._translate("Main", "Cancel"))
            close.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
            close = close.exec()

            if close == QtWidgets.QMessageBox.Yes:
                try:
                    self.worker.timer.timeout.disconnect()
                    self.worker.terminate()
                    self.close_sig.emit()
                except TypeError:
                    pass
                event.accept()

            else:
                event.ignore()
        else:
            event.accept()
