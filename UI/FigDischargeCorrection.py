"""
QRame
Copyright (C) 2023  INRAE

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import numpy as np
from PyQt5 import QtCore
from datetime import datetime
import matplotlib.dates as mdates
from scipy.interpolate import interp1d
import seaborn as sns

class FigDischargeCorrection(object):
    """Class to plot discharge correction from LOWESS 1D linear interpolation.

        Attributes
        ----------
        canvas: MplCanvas
            Object of MplCanvas a FigureCanvas
        fig: Object
            Figure object of the canvas
        units: dict
            Dictionary of units from units_conversion
        _translate: QCoreApplication.translate object
            Save words which need to be translated
        hover_connection: int
            Index to data cursor connection
        annot: Annotation
            Annotation object for data cursor
        """

    def __init__(self, canvas, units):
        """Initialize object using the specified canvas.

        Parameters
        ----------
        canvas: MplCanvas
            Object of MplCanvas
        """

        # Initialize attributes
        self.canvas = canvas
        self.fig = canvas.fig
        self.units = units
        self.hover_connection = None
        self.annot = None
        self._translate = QtCore.QCoreApplication.translate

    def create(self, df, lowess_t, lowess_q, reference_discharge, selected_meas=None):
        """Create the axes and lines for the figure.

        Parameters
        ----------
        df: pandas DataFrame
            Selected transects DataFrame
        lowess_t: numpy array
            Middle time of each transect to compute LOWESS correction
        lowess_q: numpy array
            Correction discharge after LOWESS correction
        reference_discharge: float
            Reference discharge
        selected_meas: str
            Name of the selected measurement
        """

        # Clear the plot
        self.fig.clear()

        # Configure axis
        self.fig.ax = self.fig.add_subplot(1, 1, 1)

        # Set up data
        df['raw_discharge'] = df['tr_q_total'] - df['dq_to_mean']
        valid_value = ~np.isnan(lowess_t)
        df = df.reset_index()[valid_value]
        lowess_t = lowess_t[valid_value]
        lowess_q = lowess_q[valid_value]
        lowess_datetime = [datetime.utcfromtimestamp(i) for i in lowess_t]
        df['lowess_t'] = lowess_datetime
        df['lowess_q'] = lowess_q
        palette = sns.color_palette("rocket")

        # Transect discharge
        self.fig.ax.scatter(df['lowess_t'],  df['raw_discharge'] * self.units['Q'],
                            color='royalblue', zorder=0, alpha=0.6,
                            label=self._translate('Main', 'Transect discharge'))
        # Lowess discharge
        self.fig.ax.plot(df['lowess_t'], df['lowess_q'] * self.units['Q'],
                         '*', color='darkorange', zorder=10, alpha=0.9,
                         label=self._translate('Main', 'Lowess corrected discharge'))
        # Linear interpolation
        f = interp1d(lowess_t, lowess_q, bounds_error=False)
        t_new = np.linspace(min(lowess_t), max(lowess_t), 1000)
        dq_new = f(t_new)
        date_time = [datetime.utcfromtimestamp(i) for i in t_new]
        self.fig.ax.plot(date_time, dq_new * self.units['Q'],
                         color='k', zorder=15, alpha=0.9,
                         label=self._translate('Main', 'Lowess interpolation'))

        self.fig.ax.axhline(reference_discharge, linestyle=':', linewidth=2, color=palette[2],
                            label=self._translate("Main", 'Reference discharge'), zorder=15)
        # Selected measurement
        if selected_meas and selected_meas in df.meas_name.values:
            df_selected_meas = df[df['meas_name'] == selected_meas]
            # Transect discharge
            self.fig.ax.scatter(df_selected_meas['lowess_t'], df_selected_meas['raw_discharge'] * self.units['Q'],
                                color='r', zorder=20, alpha=1)
            # Lowess discharge
            self.fig.ax.plot(df_selected_meas['lowess_t'], df_selected_meas['lowess_q'] * self.units['Q'],
                             '*', color='r', zorder=20, alpha=1)

        # Format date
        self.fig.ax.xaxis.set_major_formatter(mdates.DateFormatter('%d %b %H:%M'))
        self.fig.ax.tick_params(which="major", axis="x", pad=14, size=2)
        self.fig.autofmt_xdate()
        self.fig.ax.legend(fontsize=11, ncol=4, loc='upper center',
                           fancybox=True, shadow=True, bbox_to_anchor=(0.5, 1), )

        self.fig.ax.set_xlabel(self._translate('Main', 'Time'))
        self.fig.ax.set_ylabel(self._translate('Main', 'Discharge') + ' (' + self.units['label_Q'] + ')')
        self.fig.ax.xaxis.label.set_fontsize(12)
        self.fig.ax.yaxis.label.set_fontsize(12)
        self.fig.ax.tick_params(axis='both', direction='in', bottom=True, top=True, left=True, right=True)
        self.fig.ax.grid()


    def hover(self, event):
        """Determines if the user has selected a location with temperature data and makes
        annotation visible and calls method to update the text of the annotation. If the
        location is not valid the existing annotation is hidden.

        Parameters
        ----------
        event: MouseEvent
            Triggered when mouse button is pressed.
        """

        # Set annotation to visible
        vis = self.annot.get_visible()

        # Determine if mouse location references a data point in the plot and update the annotation.
        if event.inaxes == self.fig.ax and event.button != 3:
            cont = False
            ind = None
            plotted_line = None

            # Find the transect(line) that contains the mouse click
            for plotted_line in self.fig.ax.lines:
                cont, ind = plotted_line.contains(event)
                if cont:
                    break
            if cont:
                self.update_annot(ind, plotted_line)
                self.annot.set_visible(True)
                self.canvas.draw_idle()
            else:
                # If the cursor location is not associated with the plotted data hide the annotation.
                if vis:
                    self.annot.set_visible(False)
                    self.canvas.draw_idle()

    def update_annot(self, ind, plt_ref):
        """Updates the location and text and makes visible the previously initialized and hidden annotation.

        Parameters
        ----------
        ind: dict
            Contains data selected.
        plt_ref: Line2D
            Reference containing plotted data
        vector_ref: Quiver
            Refernece containing plotted data
        ref_label: str
            Label used to ID data type in annotation
        """

        pos = plt_ref._xy[ind["ind"][0]]

        # Shift annotation box left or right depending on which half of the axis the pos x is located and the
        # direction of x increasing.
        if plt_ref.axes.viewLim.intervalx[0] < plt_ref.axes.viewLim.intervalx[1]:
            if pos[0] < (plt_ref.axes.viewLim.intervalx[0] + plt_ref.axes.viewLim.intervalx[1]) / 2:
                self.annot._x = -20
            else:
                self.annot._x = -80
        else:
            if pos[0] < (plt_ref.axes.viewLim.intervalx[0] + plt_ref.axes.viewLim.intervalx[1]) / 2:
                self.annot._x = -80
            else:
                self.annot._x = -20

        # Shift annotation box up or down depending on which half of the axis the pos y is located and the
        # direction of y increasing.
        if plt_ref.axes.viewLim.intervaly[0] < plt_ref.axes.viewLim.intervaly[1]:
            if pos[1] > (plt_ref.axes.viewLim.intervaly[0] + plt_ref.axes.viewLim.intervaly[1]) / 2:
                self.annot._y = -40
            else:
                self.annot._y = 20
        else:
            if pos[1] > (plt_ref.axes.viewLim.intervaly[0] + plt_ref.axes.viewLim.intervaly[1]) / 2:
                self.annot._y = 20
            else:
                self.annot._y = -40
        self.annot.xy = pos

        text = 'x: {:.2f}, y: {:.2f}'.format(pos[0], pos[1])
        self.annot.set_text(text)

    def set_hover_connection(self, setting):
        """Turns the connection to the mouse event on or off.

        Parameters
        ----------
        setting: bool
            Boolean to specify whether the connection for the mouse event is active or not.
        """

        if setting and self.hover_connection is None:
            self.hover_connection = self.canvas.mpl_connect('button_press_event', self.hover)
        elif not setting:
            self.canvas.mpl_disconnect(self.hover_connection)
            self.hover_connection = None
            self.annot.set_visible(False)
            self.canvas.draw_idle()

