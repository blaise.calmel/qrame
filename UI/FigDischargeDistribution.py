"""
QRame
Copyright (C) 2023  INRAE

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import seaborn as sns
import numpy as np
import pandas as pd
from PyQt5 import QtCore
import matplotlib.patches as mpatches


class FigDischargeDistribution(object):
    """Class to plot discharge distribution graph.

        Attributes
        ----------
        canvas: MplCanvas
            Object of MplCanvas a FigureCanvas
        fig: Object
            Figure object of the canvas
        units: dict
            Dictionary of units from units_conversion
        _translate: QCoreApplication.translate object
            Save words which need to be translated
        hover_connection: int
            Index to data cursor connection
        annot: Annotation
            Annotation object for data cursor
        """

    def __init__(self, canvas, units):
        """Initialize object using the specified canvas.

        Parameters
        ----------
        canvas: MplCanvas
            Object of MplCanvas
        """

        # Initialize attributes
        self.canvas = canvas
        self.fig = canvas.fig
        self.units = units
        self.hover_connection = None
        self.annot = None
        self._translate = QtCore.QCoreApplication.translate

    def create(self, selected_transects, mean_selected_meas, deviation, discharge_ref, column_grouped=None,
               column_sorted=None, ascending=True, violin=True, show_nb_transects=True, score_color=False):
        """Create the axes and lines for the figure.

        Parameters
        ----------
        selected_transects: pandas DataFrame
            Selected transects DataFrame
        mean_selected_meas: pandas DataFrame
            Measurement results dataframe
        deviation: float
            Discharge maximum tolerated deviation from reference
        discharge_ref: float
            Reference discharge
        column_grouped: str
            Column name that could be used for grouped analysis
        column_sorted: str
            Name of the selected column to sort measurement
        ascending: bool
            Specify if current data are sorted ascending or descending
        violin: bool
            Plot distribution graph errorbox as violin
        show_nb_transects: bool
            Plot distribution graph number of transects per measurement

        """
        # Clear the plot
        self.fig.clear()

        # Configure axis
        self.fig.ax = self.fig.add_subplot(1, 1, 1)

        # Set margins and padding for figure
        # self.fig.subplots_adjust(left=0.03, bottom=0.25, right=0.99, top=0.99, wspace=0.1, hspace=0)
        self.fig.ax.xaxis.label.set_fontsize(12)
        self.fig.ax.yaxis.label.set_fontsize(12)

        # Set up labels
        palette = sns.color_palette("rocket")

        label_txt = [self._translate("Main", 'Reference discharge') + " ±" + str(deviation) + "%",
                     self._translate("Main", 'Discharge distribution')]
        if deviation == 0:
            label_txt[0] = self._translate("Main", 'Reference discharge')

        label_y = self._translate("Main", 'Discharge') + ' (' + self.units['label_Q'] + ')'
        label_ref = self._translate("Main", 'Global discharge\ndistribution')
        legends = list()

        # Group column
        if column_grouped:
            col_group_by = column_grouped
        else:
            col_group_by = 'meas_name'
        selected_transects[col_group_by] = selected_transects[col_group_by].fillna(
            self._translate("Main", 'Unknown'))
        grouped_meas = selected_transects.groupby([col_group_by]).agg({'tr_q_total': list})

        # Sort data
        if column_sorted:
            if mean_selected_meas.dtypes[column_sorted] in (float, int):
                grouped_sort = mean_selected_meas.groupby([col_group_by]).agg({
                    column_sorted: 'mean'})
            else:
                grouped_sort = mean_selected_meas.groupby([col_group_by]).agg({
                    column_sorted: lambda x: x.value_counts().index[0]})
            grouped_sort.rename({column_sorted: 'column_sorted'}, axis=1, inplace=True)
            grouped_meas = pd.concat([grouped_meas, grouped_sort], axis=1)
            grouped_meas = grouped_meas.sort_values('column_sorted', ascending=ascending)
        else:
            # grouped_meas = selected_transects.groupby([col_group_by]).agg({'tr_q_total': list})
            grouped_meas = grouped_meas.sort_values([col_group_by], ascending=ascending)
        grouped_meas.tr_q_total = [[x for x in y if not np.isnan(x)] for y in grouped_meas.tr_q_total]

        my_xticks = [l for l in grouped_meas.index]
        len_meas = len(my_xticks)
        my_xticks.insert(0, label_ref)

        # Reference discharge
        self.fig.ax.axhline(y=discharge_ref * self.units['Q'],  xmin=-2, xmax=len_meas + 1,
                            linestyle='--', color=palette[2])
        p4 = self.fig.ax.plot([np.nan, np.nan], [np.nan, np.nan], color=palette[2], linestyle='--')
        legends.append(p4[0])

        # Tolerated deviation from reference
        self.fig.ax.axhline(y=discharge_ref * self.units['Q'] * (1 - deviation / 100), linestyle=':', color=palette[2])
        self.fig.ax.axhline(y=discharge_ref * self.units['Q'] * (1 + deviation / 100), linestyle=':', color=palette[2])

        # Q global distribution
        q_all = selected_transects['tr_q_total'][~np.isnan(selected_transects['tr_q_total'])]
        self.fig.ax.boxplot(q_all * self.units['Q'], positions=[-1], patch_artist=True,
                            boxprops=dict(facecolor='darkcyan', edgecolor='k'), medianprops=dict(color='k'))

        data = [[self.units['Q'] * i for i in inner] for inner in grouped_meas['tr_q_total']]
        nanmean = [np.nanmean(i) for i in data]

        # Select color
        colors = []
        if score_color:
            # score = np.abs(300 * (nanmean - discharge_ref) / (discharge_ref * deviation))
            for name in list(grouped_meas.index):
                score = mean_selected_meas.loc[name, 'score']
                if not score:
                    colors.append('darkorange')
                elif score <= 2:
                    colors.append('#36ed00')
                elif score <= 3:
                    colors.append('#ffcc00')
                else:
                    colors.append('#ff004d')
        else:
            colors = ['darkorange'] * len(nanmean)

        # Add boxplot to legend
        legends.append(mpatches.Patch(facecolor=colors[0], edgecolor="k", linewidth=1))

        # Plot boxplot/violinplot
        if len(data) > 0:
            if violin:
                violin_plot = self.fig.ax.violinplot(data, positions=np.arange(len_meas), showmeans=False,
                                                     showmedians=False, showextrema=False
                                                     )

                for violin, color in zip(violin_plot['bodies'], colors):
                    violin.set_facecolor(color)
                    violin.set_edgecolor('black')
                    violin.set_alpha(1)

                p5 = self.fig.ax.scatter(np.arange(len_meas), nanmean, marker='o', color='k', s=30, zorder=3)
                legends.append(p5)
                label_txt.append(self._translate("Main", 'Mean value'))
            else:
                bp = self.fig.ax.boxplot(data, positions=np.arange(len_meas),
                                    whis=[0, 100], patch_artist=True,
                                    boxprops=dict(edgecolor='k'),
                                    medianprops=dict(color='k'), whiskerprops=dict(color='k', linewidth=1),
                                    capprops=dict(color='k', linewidth=1),
                                    )
                for patch, color in zip(bp['boxes'], colors):
                    patch.set_facecolor(color)
        # Show number of transects
        if show_nb_transects:
            for i in range(len_meas):
                self.fig.ax.text(i, max(data[i]), len(data[i]), fontsize=12)
        # Legend
        self.fig.ax.legend(legends, label_txt, fontsize=11, ncol=3, loc='upper center',
                           fancybox=True, shadow=True, bbox_to_anchor=(0.5, 1), )
        self.fig.ax.set_ylabel(label_y, fontsize=11)
        self.fig.ax.set_xticks(np.arange(-1, len_meas))

        if sum(len(s) for s in my_xticks[1:]) < 80:
            self.fig.ax.set_xticklabels(my_xticks, rotation=0, fontsize=10)
        else:
            self.fig.ax.set_xticklabels(my_xticks, rotation=80, fontsize=10, ha='right')
        self.fig.ax.tick_params(axis='y', labelsize=10)
        self.fig.ax.set_xlim(-1.5, len_meas - 0.5)
        self.fig.ax.grid(linestyle='--')
        # self.fig.set_size_inches(12, 6)

    def hover(self, event):
        """Determines if the user has selected a location with temperature data and makes
        annotation visible and calls method to update the text of the annotation. If the
        location is not valid the existing annotation is hidden.

        Parameters
        ----------
        event: MouseEvent
            Triggered when mouse button is pressed.
        """

        # Set annotation to visible
        vis = self.annot.get_visible()

        # Determine if mouse location references a data point in the plot and update the annotation.
        if event.inaxes == self.fig.ax and event.button != 3:
            cont = False
            ind = None
            plotted_line = None

            # Find the transect(line) that contains the mouse click
            for plotted_line in self.fig.ax.lines:
                cont, ind = plotted_line.contains(event)
                if cont:
                    break
            if cont:
                self.update_annot(ind, plotted_line)
                self.annot.set_visible(True)
                self.canvas.draw_idle()
            else:
                # If the cursor location is not associated with the plotted data hide the annotation.
                if vis:
                    self.annot.set_visible(False)
                    self.canvas.draw_idle()

    def update_annot(self, ind, plt_ref):
        """Updates the location and text and makes visible the previously initialized and hidden annotation.

        Parameters
        ----------
        ind: dict
            Contains data selected.
        plt_ref: Line2D
            Reference containing plotted data
        vector_ref: Quiver
            Refernece containing plotted data
        ref_label: str
            Label used to ID data type in annotation
        """

        pos = plt_ref._xy[ind["ind"][0]]

        # Shift annotation box left or right depending on which half of the axis the pos x is located and the
        # direction of x increasing.
        if plt_ref.axes.viewLim.intervalx[0] < plt_ref.axes.viewLim.intervalx[1]:
            if pos[0] < (plt_ref.axes.viewLim.intervalx[0] + plt_ref.axes.viewLim.intervalx[1]) / 2:
                self.annot._x = -20
            else:
                self.annot._x = -80
        else:
            if pos[0] < (plt_ref.axes.viewLim.intervalx[0] + plt_ref.axes.viewLim.intervalx[1]) / 2:
                self.annot._x = -80
            else:
                self.annot._x = -20

        # Shift annotation box up or down depending on which half of the axis the pos y is located and the
        # direction of y increasing.
        if plt_ref.axes.viewLim.intervaly[0] < plt_ref.axes.viewLim.intervaly[1]:
            if pos[1] > (plt_ref.axes.viewLim.intervaly[0] + plt_ref.axes.viewLim.intervaly[1]) / 2:
                self.annot._y = -40
            else:
                self.annot._y = 20
        else:
            if pos[1] > (plt_ref.axes.viewLim.intervaly[0] + plt_ref.axes.viewLim.intervaly[1]) / 2:
                self.annot._y = 20
            else:
                self.annot._y = -40
        self.annot.xy = pos

        text = 'x: {:.2f}, y: {:.2f}'.format(pos[0], pos[1])
        self.annot.set_text(text)

    def set_hover_connection(self, setting):
        """Turns the connection to the mouse event on or off.

        Parameters
        ----------
        setting: bool
            Boolean to specify whether the connection for the mouse event is active or not.
        """

        if setting and self.hover_connection is None:
            self.hover_connection = self.canvas.mpl_connect('button_press_event', self.hover)
        elif not setting:
            self.canvas.mpl_disconnect(self.hover_connection)
            self.hover_connection = None
            self.annot.set_visible(False)
            self.canvas.draw_idle()
