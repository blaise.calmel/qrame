"""
QRame
Copyright (C) 2023  INRAE

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from PyQt5 import QtWidgets, QtGui, QtCore
import wColumnsSelection


class ColumnsSelection(QtWidgets.QDialog, wColumnsSelection.Ui_ColumnsSelection):
    """Dialog to allow users to change QRev options.

    Parameters
    ----------
    wOptions.Ui_Options : QDialog
        Dialog window with options for users
    """

    def __init__(self, parent=None):
        """Initialize options dialog.
        """
        super(ColumnsSelection, self).__init__(parent)
        self.setupUi(self)
        self.parent = parent
