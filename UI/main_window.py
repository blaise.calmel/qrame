# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'UI\main_window.ui'
#
# Created by: PyQt5 UI code generator 5.15.9
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1925, 1055)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.MinimumExpanding, QtWidgets.QSizePolicy.MinimumExpanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(MainWindow.sizePolicy().hasHeightForWidth())
        MainWindow.setSizePolicy(sizePolicy)
        MainWindow.setBaseSize(QtCore.QSize(0, 15))
        MainWindow.setWindowTitle("")
        MainWindow.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout_4 = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout_4.setObjectName("gridLayout_4")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.tableResume = QtWidgets.QTableWidget(self.centralwidget)
        self.tableResume.setObjectName("tableResume")
        self.tableResume.setColumnCount(0)
        self.tableResume.setRowCount(0)
        self.horizontalLayout.addWidget(self.tableResume)
        self.tableMeasurements = QtWidgets.QTableWidget(self.centralwidget)
        self.tableMeasurements.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
        self.tableMeasurements.setSizeAdjustPolicy(QtWidgets.QAbstractScrollArea.AdjustIgnored)
        self.tableMeasurements.setEditTriggers(QtWidgets.QAbstractItemView.AllEditTriggers)
        self.tableMeasurements.setObjectName("tableMeasurements")
        self.tableMeasurements.setColumnCount(0)
        self.tableMeasurements.setRowCount(0)
        self.horizontalLayout.addWidget(self.tableMeasurements)
        self.horizontalLayout.setStretch(0, 1)
        self.horizontalLayout.setStretch(1, 4)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.tab_all = QtWidgets.QTabWidget(self.centralwidget)
        self.tab_all.setMouseTracking(False)
        self.tab_all.setObjectName("tab_all")
        self.tab_q_u = QtWidgets.QWidget()
        self.tab_q_u.setObjectName("tab_q_u")
        self.gridLayout = QtWidgets.QGridLayout(self.tab_q_u)
        self.gridLayout.setObjectName("gridLayout")
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.graphics_discharge_uncertainty = QtWidgets.QWidget(self.tab_q_u)
        self.graphics_discharge_uncertainty.setObjectName("graphics_discharge_uncertainty")
        self.horizontalLayout_3.addWidget(self.graphics_discharge_uncertainty)
        self.gridLayout.addLayout(self.horizontalLayout_3, 0, 0, 1, 1)
        self.tab_all.addTab(self.tab_q_u, "")
        self.tab_q_time = QtWidgets.QWidget()
        self.tab_q_time.setObjectName("tab_q_time")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.tab_q_time)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.graphics_discharge_time = QtWidgets.QWidget(self.tab_q_time)
        self.graphics_discharge_time.setObjectName("graphics_discharge_time")
        self.horizontalLayout_4.addWidget(self.graphics_discharge_time)
        self.gridLayout_2.addLayout(self.horizontalLayout_4, 0, 0, 1, 1)
        self.tab_all.addTab(self.tab_q_time, "")
        self.tab_u_sources = QtWidgets.QWidget()
        self.tab_u_sources.setObjectName("tab_u_sources")
        self.gridLayout_3 = QtWidgets.QGridLayout(self.tab_u_sources)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.horizontalLayout_5 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_5.setObjectName("horizontalLayout_5")
        self.graphics_uncertainty_sources = QtWidgets.QWidget(self.tab_u_sources)
        self.graphics_uncertainty_sources.setObjectName("graphics_uncertainty_sources")
        self.horizontalLayout_5.addWidget(self.graphics_uncertainty_sources)
        self.gridLayout_3.addLayout(self.horizontalLayout_5, 0, 0, 1, 1)
        self.tab_all.addTab(self.tab_u_sources, "")
        self.tab_q_distribution = QtWidgets.QWidget()
        self.tab_q_distribution.setObjectName("tab_q_distribution")
        self.gridLayout_5 = QtWidgets.QGridLayout(self.tab_q_distribution)
        self.gridLayout_5.setObjectName("gridLayout_5")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.cb_violin = QtWidgets.QCheckBox(self.tab_q_distribution)
        self.cb_violin.setObjectName("cb_violin")
        self.verticalLayout_2.addWidget(self.cb_violin)
        self.cb_nb_transect = QtWidgets.QCheckBox(self.tab_q_distribution)
        self.cb_nb_transect.setObjectName("cb_nb_transect")
        self.verticalLayout_2.addWidget(self.cb_nb_transect)
        self.cb_color_score = QtWidgets.QCheckBox(self.tab_q_distribution)
        self.cb_color_score.setObjectName("cb_color_score")
        self.verticalLayout_2.addWidget(self.cb_color_score)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_2.addItem(spacerItem)
        self.horizontalLayout_2.addLayout(self.verticalLayout_2)
        self.graphics_discharge_distribution = QtWidgets.QWidget(self.tab_q_distribution)
        self.graphics_discharge_distribution.setObjectName("graphics_discharge_distribution")
        self.horizontalLayout_2.addWidget(self.graphics_discharge_distribution)
        self.horizontalLayout_2.setStretch(1, 10)
        self.gridLayout_5.addLayout(self.horizontalLayout_2, 0, 0, 1, 1)
        self.tab_all.addTab(self.tab_q_distribution, "")
        self.tab_q_correction = QtWidgets.QWidget()
        self.tab_q_correction.setObjectName("tab_q_correction")
        self.gridLayout_6 = QtWidgets.QGridLayout(self.tab_q_correction)
        self.gridLayout_6.setObjectName("gridLayout_6")
        self.horizontalLayout_6 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_6.setObjectName("horizontalLayout_6")
        self.graphics_discharge_correction = QtWidgets.QWidget(self.tab_q_correction)
        self.graphics_discharge_correction.setObjectName("graphics_discharge_correction")
        self.horizontalLayout_6.addWidget(self.graphics_discharge_correction)
        self.gridLayout_6.addLayout(self.horizontalLayout_6, 0, 0, 1, 1)
        self.tab_all.addTab(self.tab_q_correction, "")
        self.verticalLayout.addWidget(self.tab_all)
        self.verticalLayout.setStretch(0, 1)
        self.verticalLayout.setStretch(1, 1)
        self.gridLayout_4.addLayout(self.verticalLayout, 0, 0, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1925, 21))
        self.menubar.setTabletTracking(False)
        self.menubar.setAcceptDrops(True)
        self.menubar.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))
        self.menubar.setDefaultUp(False)
        self.menubar.setNativeMenuBar(True)
        self.menubar.setObjectName("menubar")
        self.menuFile = QtWidgets.QMenu(self.menubar)
        self.menuFile.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))
        self.menuFile.setObjectName("menuFile")
        self.menuOpen = QtWidgets.QMenu(self.menuFile)
        self.menuOpen.setObjectName("menuOpen")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.toolbar = QtWidgets.QToolBar(MainWindow)
        self.toolbar.setEnabled(True)
        self.toolbar.setBaseSize(QtCore.QSize(0, 15))
        self.toolbar.setContextMenuPolicy(QtCore.Qt.PreventContextMenu)
        self.toolbar.setWindowTitle("")
        self.toolbar.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))
        self.toolbar.setMovable(False)
        self.toolbar.setFloatable(True)
        self.toolbar.setObjectName("toolbar")
        MainWindow.addToolBar(QtCore.Qt.TopToolBarArea, self.toolbar)
        self.actionOpen_data = QtWidgets.QAction(MainWindow)
        self.actionOpen_data.setObjectName("actionOpen_data")
        self.actionOpen_results = QtWidgets.QAction(MainWindow)
        self.actionOpen_results.setObjectName("actionOpen_results")
        self.actionExport_csv = QtWidgets.QAction(MainWindow)
        self.actionExport_csv.setObjectName("actionExport_csv")
        self.actionOpen = QtWidgets.QAction(MainWindow)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/images/images/open.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.actionOpen.setIcon(icon)
        self.actionOpen.setShortcut("")
        self.actionOpen.setShortcutVisibleInContextMenu(True)
        self.actionOpen.setObjectName("actionOpen")
        self.actionSettings = QtWidgets.QAction(MainWindow)
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(":/images/images/option.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.actionSettings.setIcon(icon1)
        self.actionSettings.setObjectName("actionSettings")
        self.actionChecked = QtWidgets.QAction(MainWindow)
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap(":/images/images/check_mark_green.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.actionChecked.setIcon(icon2)
        self.actionChecked.setObjectName("actionChecked")
        self.actionHomeGraph = QtWidgets.QAction(MainWindow)
        icon3 = QtGui.QIcon()
        icon3.addPixmap(QtGui.QPixmap(":/images/images/home.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.actionHomeGraph.setIcon(icon3)
        self.actionHomeGraph.setObjectName("actionHomeGraph")
        self.actionZoomGraph = QtWidgets.QAction(MainWindow)
        self.actionZoomGraph.setCheckable(True)
        icon4 = QtGui.QIcon()
        icon4.addPixmap(QtGui.QPixmap(":/images/images/zoom.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.actionZoomGraph.setIcon(icon4)
        self.actionZoomGraph.setObjectName("actionZoomGraph")
        self.actionPanGraph = QtWidgets.QAction(MainWindow)
        self.actionPanGraph.setCheckable(True)
        icon5 = QtGui.QIcon()
        icon5.addPixmap(QtGui.QPixmap(":/images/images/Move.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.actionPanGraph.setIcon(icon5)
        self.actionPanGraph.setObjectName("actionPanGraph")
        self.actionTable = QtWidgets.QAction(MainWindow)
        icon6 = QtGui.QIcon()
        icon6.addPixmap(QtGui.QPixmap(":/images/images/note.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.actionTable.setIcon(icon6)
        self.actionTable.setObjectName("actionTable")
        self.actionExport_bareme = QtWidgets.QAction(MainWindow)
        self.actionExport_bareme.setObjectName("actionExport_bareme")
        self.actionExport_baratinage = QtWidgets.QAction(MainWindow)
        self.actionExport_baratinage.setObjectName("actionExport_baratinage")
        self.actionSave_pickle = QtWidgets.QAction(MainWindow)
        self.actionSave_pickle.setObjectName("actionSave_pickle")
        self.actionImport_hydro2 = QtWidgets.QAction(MainWindow)
        self.actionImport_hydro2.setObjectName("actionImport_hydro2")
        self.actionIntercomparison = QtWidgets.QAction(MainWindow)
        icon7 = QtGui.QIcon()
        icon7.addPixmap(QtGui.QPixmap(":/images/images/intercomparison.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.actionIntercomparison.setIcon(icon7)
        self.actionIntercomparison.setObjectName("actionIntercomparison")
        self.actionAddMeasurement = QtWidgets.QAction(MainWindow)
        icon8 = QtGui.QIcon()
        icon8.addPixmap(QtGui.QPixmap(":/images/images/plus.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.actionAddMeasurement.setIcon(icon8)
        self.actionAddMeasurement.setIconText("AddMeasurement")
        self.actionAddMeasurement.setObjectName("actionAddMeasurement")
        self.actionKML = QtWidgets.QAction(MainWindow)
        icon9 = QtGui.QIcon()
        icon9.addPixmap(QtGui.QPixmap(":/images/images/Globe.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.actionKML.setIcon(icon9)
        self.actionKML.setObjectName("actionKML")
        self.menuOpen.addAction(self.actionOpen_data)
        self.menuOpen.addAction(self.actionOpen_results)
        self.menuFile.addAction(self.menuOpen.menuAction())
        self.menuFile.addAction(self.actionImport_hydro2)
        self.menuFile.addSeparator()
        self.menuFile.addAction(self.actionExport_csv)
        self.menuFile.addAction(self.actionExport_bareme)
        self.menuFile.addAction(self.actionExport_baratinage)
        self.menuFile.addAction(self.actionSave_pickle)
        self.menubar.addAction(self.menuFile.menuAction())
        self.toolbar.addAction(self.actionOpen)
        self.toolbar.addAction(self.actionSettings)
        self.toolbar.addAction(self.actionAddMeasurement)
        self.toolbar.addAction(self.actionIntercomparison)
        self.toolbar.addAction(self.actionChecked)
        self.toolbar.addAction(self.actionTable)
        self.toolbar.addSeparator()
        self.toolbar.addAction(self.actionHomeGraph)
        self.toolbar.addAction(self.actionZoomGraph)
        self.toolbar.addAction(self.actionPanGraph)
        self.toolbar.addSeparator()
        self.toolbar.addAction(self.actionKML)

        self.retranslateUi(MainWindow)
        self.tab_all.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        self.tableMeasurements.setSortingEnabled(True)
        self.tab_all.setTabText(self.tab_all.indexOf(self.tab_q_u), _translate("MainWindow", "Uncertainty results"))
        self.tab_all.setTabText(self.tab_all.indexOf(self.tab_q_time), _translate("MainWindow", "Discharge over time"))
        self.tab_all.setTabText(self.tab_all.indexOf(self.tab_u_sources), _translate("MainWindow", "Uncertainty sources"))
        self.cb_violin.setText(_translate("MainWindow", "Violin"))
        self.cb_nb_transect.setText(_translate("MainWindow", "# Transects"))
        self.cb_color_score.setText(_translate("MainWindow", "Color score"))
        self.tab_all.setTabText(self.tab_all.indexOf(self.tab_q_distribution), _translate("MainWindow", "Discharge distribution"))
        self.tab_all.setTabText(self.tab_all.indexOf(self.tab_q_correction), _translate("MainWindow", "Discharge correction"))
        self.menuFile.setTitle(_translate("MainWindow", "File"))
        self.menuOpen.setTitle(_translate("MainWindow", "Open"))
        self.actionOpen_data.setText(_translate("MainWindow", "ADCP data"))
        self.actionOpen_data.setShortcut(_translate("MainWindow", "Ctrl+O"))
        self.actionOpen_results.setText(_translate("MainWindow", "Previous results"))
        self.actionOpen_results.setShortcut(_translate("MainWindow", "Ctrl+R"))
        self.actionExport_csv.setText(_translate("MainWindow", "Export current data as .csv"))
        self.actionExport_csv.setShortcut(_translate("MainWindow", "Ctrl+S"))
        self.actionOpen.setText(_translate("MainWindow", "Open"))
        self.actionOpen.setToolTip(_translate("MainWindow", "Open ADCP data (select folder)"))
        self.actionSettings.setText(_translate("MainWindow", "Settings"))
        self.actionSettings.setToolTip(_translate("MainWindow", "Current settings"))
        self.actionChecked.setText(_translate("MainWindow", "Checked"))
        self.actionChecked.setToolTip(_translate("MainWindow", "Selected measurement"))
        self.actionHomeGraph.setText(_translate("MainWindow", "HomeGraph"))
        self.actionHomeGraph.setToolTip(_translate("MainWindow", "Reset zoom/pan of graph"))
        self.actionZoomGraph.setText(_translate("MainWindow", "Zoom"))
        self.actionZoomGraph.setToolTip(_translate("MainWindow", "Zoom"))
        self.actionPanGraph.setText(_translate("MainWindow", "PanGraph"))
        self.actionPanGraph.setToolTip(_translate("MainWindow", "Move graph"))
        self.actionTable.setText(_translate("MainWindow", "Table"))
        self.actionTable.setToolTip(_translate("MainWindow", "Select table\'s columns"))
        self.actionExport_bareme.setText(_translate("MainWindow", "Export for Bareme (.dat)"))
        self.actionExport_baratinage.setText(_translate("MainWindow", "Export for BaRatinAGE (.csv, .BAD)"))
        self.actionSave_pickle.setText(_translate("MainWindow", "Save to open later (.pickle)"))
        self.actionImport_hydro2.setText(_translate("MainWindow", "Import HYDRO2 file"))
        self.actionIntercomparison.setText(_translate("MainWindow", "Intercomparison"))
        self.actionIntercomparison.setToolTip(_translate("MainWindow", "Intercomparison settings"))
        self.actionAddMeasurement.setText(_translate("MainWindow", "AddMeasurement"))
        self.actionAddMeasurement.setToolTip(_translate("MainWindow", "Add measurement"))
        self.actionAddMeasurement.setShortcut(_translate("MainWindow", "Ctrl+F"))
        self.actionKML.setText(_translate("MainWindow", "KML"))
        self.actionKML.setToolTip(_translate("MainWindow", "Export as KML"))
import resources_rc


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
