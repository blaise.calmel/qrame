from PyQt5 import QtWidgets, QtGui, QtCore
from UI import wTableUpdateCombo


class TableUpdateCombo(QtWidgets.QDialog, wTableUpdateCombo.Ui_updateCombo):
    """Dialog to allow users to change salinity.

    Parameters
    ----------
    wSalinity.Ui_salinity : QDialog
        Dialog window to allow users to change salinity
    """

    def __init__(self, values, parent=None, title=None):
        super(TableUpdateCombo, self).__init__(parent)
        self._translate = QtCore.QCoreApplication.translate
        self.setupUi(self)
        if title is not None:
            self.label_value.setText(title)

        for i in range(len(values)):
            self.cb_update.addItem("")
            self.cb_update.setItemText(i, values[i])

        # set qlineedit to numbers only, 2 decimals, and 0 to 69.99 ppt
        # rx = QtCore.QRegExp("^([0-9]|[1-6][0-9])(\.\d{1,2})$")
        # validator = QtGui.QRegExpValidator(rx, self)
        # self.value.setValidator(validator)
