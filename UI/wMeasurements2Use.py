# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'wMeasurements2Use.ui'
#
# Created by: PyQt5 UI code generator 5.15.9
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Measurements2Use(object):
    def setupUi(self, Measurements2Use):
        Measurements2Use.setObjectName("Measurements2Use")
        Measurements2Use.setWindowModality(QtCore.Qt.ApplicationModal)
        Measurements2Use.resize(405, 804)
        self.gridLayout = QtWidgets.QGridLayout(Measurements2Use)
        self.gridLayout.setObjectName("gridLayout")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.tableSelect = QtWidgets.QTableWidget(Measurements2Use)
        self.tableSelect.setObjectName("tableSelect")
        self.tableSelect.setColumnCount(0)
        self.tableSelect.setRowCount(0)
        self.verticalLayout.addWidget(self.tableSelect)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.pb_check_all = QtWidgets.QPushButton(Measurements2Use)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.pb_check_all.setFont(font)
        self.pb_check_all.setObjectName("pb_check_all")
        self.horizontalLayout.addWidget(self.pb_check_all)
        self.pb_uncheck_invalid = QtWidgets.QPushButton(Measurements2Use)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.pb_uncheck_invalid.setFont(font)
        self.pb_uncheck_invalid.setObjectName("pb_uncheck_invalid")
        self.horizontalLayout.addWidget(self.pb_uncheck_invalid)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.buttonBox = QtWidgets.QDialogButtonBox(Measurements2Use)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.buttonBox.setFont(font)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.horizontalLayout.addWidget(self.buttonBox)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.gridLayout.addLayout(self.verticalLayout, 0, 0, 1, 1)

        self.retranslateUi(Measurements2Use)
        self.buttonBox.accepted.connect(Measurements2Use.accept) # type: ignore
        self.buttonBox.rejected.connect(Measurements2Use.reject) # type: ignore
        QtCore.QMetaObject.connectSlotsByName(Measurements2Use)

    def retranslateUi(self, Measurements2Use):
        _translate = QtCore.QCoreApplication.translate
        Measurements2Use.setWindowTitle(_translate("Measurements2Use", "Measurements to Use"))
        self.pb_check_all.setText(_translate("Measurements2Use", "Check All"))
        self.pb_uncheck_invalid.setText(_translate("Measurements2Use", "Uncheck Invalid"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Measurements2Use = QtWidgets.QDialog()
    ui = Ui_Measurements2Use()
    ui.setupUi(Measurements2Use)
    Measurements2Use.show()
    sys.exit(app.exec_())
