"""
QRame
Copyright (C) 2023  INRAE

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import copy

import pandas as pd
import numpy as np
from PyQt5.QtCore import pyqtSignal, QThread
from statsmodels.formula.api import ols
from statsmodels.stats.anova import anova_lm
from scipy.optimize.minpack import curve_fit
import scipy.stats


class Intercomparison(QThread):
    """ Class of intercomparison computation : statistical tests (Cochran & Grubbs), measurements uncertainty and
    empirical uncertainty computation.

    Attributes
    ----------
    transects_df: pandas DataFrame
        Dataframe with checked transects data
    n_transects: float
        Number of transects to empirical uncertainty computation
    n_sections: int
        Number of sections for two factors analysis
    n_teams: int
        Number of teams for two factors analysis
    no_moving_bed: bool
        Specify if there is no moving bed observed
    compute_intercomparison: bool
        Specify if QRame should apply intercomparison computation
    u_bias: float
        Uncertainty bias value
    u_ref: float
        Uncertainty on the discharge reference
    columns_grouped: str
        Column name used for grouped analysis
    effect_section: str
        Name of the section column
    effect_team: str
        Name of the team column
    """

    def __init__(self, transects_df, parent=None, n_transects=1, n_sections=1, n_teams=1, no_moving_bed=False,
                 compute_intercomparison=True, u_bias=None, u_ref=0, columns_grouped=None, columns_effects=[None, None],
                 ):
        """Initialize object using the specified canvas.

        Parameters
        ----------
        transects_df: pandas DataFrame
            Dataframe with checked transects data
        parent:
            Identifies parent GUI
        n_transects: float
            Number of transects to empirical uncertainty computation
        n_sections: int
            Number of sections for two factors analysis
        n_teams: int
            Number of teams for two factors analysis
        no_moving_bed: bool
            Specify if there is no moving bed observed
        compute_intercomparison: bool
            Specify if QRame should apply intercomparison computation
        u_bias: float
            Uncertainty bias value
        u_ref: float
            Uncertainty on the discharge reference
        columns_grouped: str
            Column name used for grouped analysis
        columns_effects: list[str, str]
            List containing names of the section and team columns
        """
        QThread.__init__(self)
        self.transects_df = transects_df
        self.transects_df['cochran_test'] = np.nan
        self.transects_df['grubbs_test'] = np.nan
        self.n_transects = n_transects
        self.n_sections = n_sections
        self.n_teams = n_teams
        self.no_moving_bed = no_moving_bed

        self.compute_intercomparison = compute_intercomparison
        self.u_bias = u_bias
        self.u_ref = u_ref
        self.columns_grouped = columns_grouped
        self.effect_section, self.effect_team = columns_effects

    finished = pyqtSignal()

    def run(self):
        """ Process intercomparison thread
        """
        if self.transects_df is not None:
            # Check if no moving bed observed by the user
            if 'ADCP' in list(self.transects_df.meas_tool):
                self.update_moving_bed()

            # Compute empirical uncertainty
            if self.compute_intercomparison:
                # Apply Cochran test
                self.compute_cochran()
                # Apply Grubbs test
                self.compute_grubbs()
                self.return_nan(self.transects_df.index)
                if self.columns_grouped is not None:
                    col = self.transects_df[self.columns_grouped].fillna('Unknown')
                    sections = np.unique(col)
                    for section in sections:
                        dataframe = self.transects_df[self.transects_df[self.columns_grouped] == section]
                        self.empirical_uncertainty(dataframe=dataframe, n_transects=self.n_transects,
                                                   u_bias=self.u_bias, u_ref=self.u_ref,
                                                   effect_section=self.effect_section,
                                                   effect_team=self.effect_team)
                else:
                    self.empirical_uncertainty(dataframe=self.transects_df, n_transects=self.n_transects,
                                               u_bias=self.u_bias, u_ref=self.u_ref, effect_section=self.effect_section,
                                               effect_team=self.effect_team)
        self.finished.emit()

    def update_moving_bed(self):
        """ Update uncertainty values following no moving bed observed
        """

        # Compute transects uncertainty
        u_2 = self.transects_df['tr_oursin_u_meas']**2 + \
              self.transects_df['tr_oursin_u_compass']**2 + 0.5**2 + \
              self.transects_df['tr_oursin_u_ens']**2 + self.transects_df['tr_oursin_u_syst']**2 + \
              self.transects_df['tr_oursin_u_boat']**2 + self.transects_df['tr_oursin_u_depth']**2 + \
              self.transects_df['tr_oursin_u_water']**2 + self.transects_df['tr_oursin_u_top']**2 + \
              self.transects_df['tr_oursin_u_bot']**2 + self.transects_df['tr_oursin_u_left']**2 + \
              self.transects_df['tr_oursin_u_right']**2 + (self.transects_df['tr_oursin_u_cov'])**2

        U_95 = 2 * u_2**0.5

        self.transects_df['tr_oursin_u_total'] = U_95

        def f(x):
            return np.mean(x**2)

        # Compute measurement uncertainty
        u_2_meas = self.transects_df.groupby(['meas_name'])['tr_oursin_u_meas'].agg(f)
        u_2_syst = self.transects_df.groupby(['meas_name'])['tr_oursin_u_syst'].agg(f)
        u_2_compass = self.transects_df.groupby(['meas_name'])['tr_oursin_u_compass'].agg(f)
        if self.no_moving_bed:
            bt_movbed = copy.deepcopy(self.transects_df[['meas_name', 'meas_navigation', 'tr_oursin_u_movbed']])
            bt_movbed.loc[bt_movbed['meas_navigation'] == 'BT', 'tr_oursin_u_movbed'] = 0.5
            u_2_movb = bt_movbed.groupby(['meas_name'])['tr_oursin_u_movbed'].agg(f)
        else:
            u_2_movb = self.transects_df.groupby(['meas_name'])['tr_oursin_u_movbed'].agg(f)
        u_2_ens = self.transects_df.groupby(['meas_name'])['tr_oursin_u_ens'].agg(f)
        u_2_badb = self.transects_df.groupby(['meas_name'])['tr_oursin_u_boat'].agg(f)
        u_2_badd = self.transects_df.groupby(['meas_name'])['tr_oursin_u_depth'].agg(f)
        u_2_badw = self.transects_df.groupby(['meas_name'])['tr_oursin_u_water'].agg(f)
        u_2_top = self.transects_df.groupby(['meas_name'])['tr_oursin_u_top'].agg(f)
        u_2_bot = self.transects_df.groupby(['meas_name'])['tr_oursin_u_bot'].agg(f)
        u_2_left = self.transects_df.groupby(['meas_name'])['tr_oursin_u_left'].agg(f)
        u_2_rght = self.transects_df.groupby(['meas_name'])['tr_oursin_u_right'].agg(f)
        u_2_cov = self.transects_df.groupby(['meas_name'])['tr_oursin_u_cov'].agg(f)
        n_tr = self.transects_df.groupby(['meas_name'])['meas_ntransects'].mean()

        u_2_meas = (1/n_tr) * u_2_meas + u_2_syst + u_2_compass + u_2_movb + u_2_ens + u_2_badb + u_2_badd + u_2_badw +\
                   u_2_top + u_2_bot + u_2_left + u_2_rght + (1/n_tr) * u_2_cov

        U_95_meas = 2 * u_2_meas**0.5
        adcp_meas = list(self.transects_df[self.transects_df.meas_tool == 'ADCP'].meas_name)
        for meas_name in adcp_meas:
            self.transects_df.loc[self.transects_df.meas_name == meas_name, 'meas_oursin_95'] = U_95_meas[meas_name]
        # self.transects_df['meas_oursin_95'] = [U_95_meas.to_dict()[elem] for elem in self.transects_df.meas_name]

    def compute_cochran(self):
        """ Apply cochran statistical test
        """

        # transects_df = self.transects_df[self.transects_df['meas_tool'] == 'ADCP']
        merged_group = self.transects_df.groupby('meas_name', as_index=False).agg({'tr_q_total': 'std',
                                                                                   'meas_ntransects': 'mean'})
        self.transects_df['cochran_test'] = 1
        nan_names = merged_group[merged_group['tr_q_total'].isnull()]['meas_name']
        self.transects_df.loc[self.transects_df['meas_name'].isin(nan_names), 'cochran_test'] = -1
        merged_group = merged_group[~merged_group['meas_name'].isin(nan_names)]
        # self.transects_df.loc[self.transects_df['meas_tool'] != 'ADCP', 'cochran_test'] = 0
        while len(merged_group) >= 2:
            # Extreme deviation measurement
            name_max, std_max, _ = merged_group.iloc[merged_group['tr_q_total'].idxmax()].values
            std_sum = np.nansum(merged_group['tr_q_total']**2)
            cochran = std_max ** 2 / std_sum
            p = len(merged_group)
            n = np.nanmean(merged_group['meas_ntransects'])
            # Cochran table interpolated values
            vcc1 = (1 + (p - 1) / scipy.stats.f.ppf(q=1 - 0.01 / p, dfn=n, dfd=(p - 1) * n))**-1
            vcc5 = (1 + (p - 1) / scipy.stats.f.ppf(q=1 - 0.05 / p, dfn=n, dfd=(p - 1) * n)) ** -1

            # Check Cochran test validity
            if cochran > vcc1:
                merged_group = merged_group[merged_group['meas_name'] != name_max].reset_index(drop=True)
                self.transects_df.loc[self.transects_df['meas_name'] == name_max, 'cochran_test'] = -1
            elif cochran > vcc5:
                self.transects_df.loc[self.transects_df['meas_name'] == name_max, 'cochran_test'] = 0
                break
            else:
                break

    def compute_grubbs(self):
        """ Apply cochran statistical test
        """
        merged_group = self.transects_df.groupby('meas_name', as_index=False).agg({'tr_q_total': 'mean'})
        self.transects_df['grubbs_test'] = 1
        while 2 < len(merged_group):
            change = False
            p = len(merged_group)
            # Grubbs table interpolated values
            vcg1 = (p - 1) / (np.sqrt(p)) * np.sqrt(scipy.stats.t.ppf(q=1 - 0.01 / (2 * p), df=p - 2) ** 2 / (
                    p - 2 + scipy.stats.t.ppf(q=1 - 0.01 / (2 * p), df=p - 2) ** 2))
            vcg5 = (p - 1) / (np.sqrt(p)) * np.sqrt(scipy.stats.t.ppf(q=1 - 0.05 / (2 * p), df=p - 2) ** 2 / (
                    p - 2 + scipy.stats.t.ppf(q=1 - 0.05 / (2 * p), df=p - 2) ** 2))
            # Extreme discharge measurement
            name_max, q_max = merged_group.iloc[merged_group['tr_q_total'].idxmax()].values
            name_min, q_min = merged_group.iloc[merged_group['tr_q_total'].idxmin()].values
            q_mean = merged_group['tr_q_total'].mean()
            q_std = merged_group['tr_q_total'].std()
            grubbs_max = (q_max - q_mean) / q_std
            grubbs_min = (q_mean - q_min) / q_std
            # Check Grubbs test validity
            if grubbs_max > vcg1:
                merged_group = merged_group[merged_group['meas_name'] != name_max].reset_index(drop=True)
                self.transects_df.loc[self.transects_df['meas_name'] == name_max, 'grubbs_test'] = -1
                change = True
            elif grubbs_max > vcg5:
                self.transects_df.loc[self.transects_df['meas_name'] == name_max, 'grubbs_test'] = 0

            if grubbs_min > vcg1:
                merged_group = merged_group[merged_group['meas_name'] != name_max].reset_index(drop=True)
                self.transects_df.loc[self.transects_df['meas_name'] == name_max, 'grubbs_test'] = -1
                change = True
            elif grubbs_min > vcg5:
                self.transects_df.loc[self.transects_df['meas_name'] == name_max, 'grubbs_test'] = 0

            if not change:
                break

    def meas_uncertainty(self):
        """ Compute measurements uncertainty with OURSIN method.
        """
        dataframe = self.data
        n_transects = dataframe.groupby('Name')['Nb_transects'].mean()

        # u² 68%
        dataframe['tr_oursin_u_2_meas'] = dataframe['tr_oursin_u_meas'] ** 2
        dataframe['tr_oursin_u_2_syst'] = dataframe['tr_oursin_u_syst'] ** 2
        dataframe['tr_oursin_u_2_compass'] = dataframe['tr_oursin_u_compass'] ** 2
        if self.no_moving_bed:
            self.data['tr_oursin_u_2_movbed'] = 0.5 ** 2
        else:
            dataframe['tr_oursin_u_2_movbed'] = dataframe['tr_oursin_u_movbed'] ** 2
        dataframe['tr_oursin_u_2_ens'] = dataframe['tr_oursin_u_ens'] ** 2
        dataframe['tr_oursin_u_2_boat'] = dataframe['tr_oursin_u_boat'] ** 2
        dataframe['tr_oursin_u_2_depth'] = dataframe['tr_oursin_u_depth'] ** 2
        dataframe['tr_oursin_u_2_water'] = dataframe['tr_oursin_u_water'] ** 2
        dataframe['tr_oursin_u_2_top'] = dataframe['tr_oursin_u_top'] ** 2
        dataframe['tr_oursin_u_2_bot'] = dataframe['tr_oursin_u_bot'] ** 2
        dataframe['tr_oursin_u_2_left'] = dataframe['tr_oursin_u_left'] ** 2
        dataframe['tr_oursin_u_2_right'] = dataframe['tr_oursin_u_right'] ** 2
        dataframe['tr_oursin_u_2_cov'] = dataframe['tr_oursin_u_cov'] ** 2

        # Mean u² 68% by measurement
        u_2_meas = dataframe.groupby(['Name'])['tr_oursin_u_2_meas'].mean()
        u_2_syst = dataframe.groupby(['Name'])['tr_oursin_u_2_syst'].mean()
        u_2_compass = dataframe.groupby(['Name'])['tr_oursin_u_2_compass'].mean()
        u_2_movb = dataframe.groupby(['Name'])['tr_oursin_u_2_movbed'].mean()
        u_2_ens = dataframe.groupby(['Name'])['tr_oursin_u_2_ens'].mean()
        u_2_badb = dataframe.groupby(['Name'])['tr_oursin_u_2_boat'].mean()
        u_2_badd = dataframe.groupby(['Name'])['tr_oursin_u_2_depth'].mean()
        u_2_badw = dataframe.groupby(['Name'])['tr_oursin_u_2_water'].mean()
        u_2_top = dataframe.groupby(['Name'])['tr_oursin_u_2_top'].mean()
        u_2_bot = dataframe.groupby(['Name'])['tr_oursin_u_2_bot'].mean()
        u_2_left = dataframe.groupby(['Name'])['tr_oursin_u_2_left'].mean()
        u_2_rght = dataframe.groupby(['Name'])['tr_oursin_u_2_right'].mean()
        u_2_cov = dataframe.groupby(['Name'])['tr_oursin_u_2_cov'].mean()

        # Compute variance
        dataframe['o_2_meas'] = [(1 / n_transects.to_dict()[elem]) * u_2_meas.to_dict()[elem] for elem in
                                 dataframe.Name]
        dataframe['o_2_cov'] = [(1 / n_transects.to_dict()[elem]) * u_2_cov.to_dict()[elem] for elem in
                                dataframe.Name]
        dataframe['o_2_syst'] = [u_2_syst.to_dict()[elem] for elem in dataframe.Name]
        dataframe['o_2_compass'] = [u_2_compass.to_dict()[elem] for elem in dataframe.Name]
        dataframe['o_2_movbed'] = [u_2_movb.to_dict()[elem] for elem in dataframe.Name]
        dataframe['o_2_ens'] = [u_2_ens.to_dict()[elem] for elem in dataframe.Name]
        dataframe['o_2_badb'] = [u_2_badb.to_dict()[elem] for elem in dataframe.Name]
        dataframe['o_2_badd'] = [u_2_badd.to_dict()[elem] for elem in dataframe.Name]
        dataframe['o_2_badw'] = [u_2_badw.to_dict()[elem] for elem in dataframe.Name]
        dataframe['o_2_bot'] = [u_2_bot.to_dict()[elem] for elem in dataframe.Name]
        dataframe['o_2_left'] = [u_2_left.to_dict()[elem] for elem in dataframe.Name]
        dataframe['o_2_rght'] = [u_2_rght.to_dict()[elem] for elem in dataframe.Name]
        dataframe['o_2_top'] = [u_2_top.to_dict()[elem] for elem in dataframe.Name]

        # Compute absolute variance
        dataframe['o_2_meas_abs'] = dataframe.o_2_meas * (0.01 ** 2) * (
                dataframe.Discharge ** 2)
        dataframe['o_2_syst_abs'] = dataframe.o_2_syst * (0.01 ** 2) * (
                dataframe.Discharge ** 2)
        dataframe['o_2_compass_abs'] = dataframe.o_2_compass * (0.01 ** 2) * (
                dataframe.Discharge ** 2)
        dataframe['o_2_ens_abs'] = dataframe.o_2_ens * (0.01 ** 2) * (dataframe.Discharge ** 2)
        dataframe['o_2_movbed_abs'] = dataframe.o_2_movbed * (0.01 ** 2) * (
                dataframe.Discharge ** 2)
        dataframe['o_2_badb_abs'] = dataframe.o_2_badb * (0.01 ** 2) * (
                dataframe.Discharge ** 2)
        dataframe['o_2_badd_abs'] = dataframe.o_2_badd * (0.01 ** 2) * (
                dataframe.Discharge ** 2)
        dataframe['o_2_badw_abs'] = dataframe.o_2_badw * (0.01 ** 2) * (
                dataframe.Discharge ** 2)
        dataframe['o_2_top_abs'] = dataframe.o_2_top * (0.01 ** 2) * (dataframe.Discharge ** 2)
        dataframe['o_2_bot_abs'] = dataframe.o_2_bot * (0.01 ** 2) * (dataframe.Discharge ** 2)
        dataframe['o_2_left_abs'] = dataframe.o_2_left * (0.01 ** 2) * (
                dataframe.Discharge ** 2)
        dataframe['o_2_rght_abs'] = dataframe.o_2_rght * (0.01 ** 2) * (
                dataframe.Discharge ** 2)
        dataframe['o_2_cov_abs'] = dataframe.o_2_cov * (0.01 ** 2) * (dataframe.Discharge ** 2)

        # Compute U95%
        u_2_oursin = (1 / n_transects) * u_2_meas + u_2_badb + u_2_badd + u_2_badw + u_2_bot + u_2_ens + u_2_movb + \
                     u_2_left + u_2_rght + u_2_syst + u_2_compass + u_2_top + (1 / n_transects) * u_2_cov
        u_c_oursin = u_2_oursin ** 0.5
        U_95_oursin = 2 * u_c_oursin

        u_2_oursin_dict = u_2_oursin.to_dict()
        u_c_oursin_dict = u_c_oursin.to_dict()
        U_95_oursin_dict = U_95_oursin.to_dict()
        dataframe['u2_oursin'] = [u_2_oursin_dict[elem] for elem in dataframe.Name]
        dataframe['u_c_oursin'] = [u_c_oursin_dict[elem] for elem in dataframe.Name]
        dataframe['U_95_oursin'] = [U_95_oursin_dict[elem] for elem in dataframe.Name]

    def empirical_uncertainty(self, dataframe, n_labs=1, n_transects=None, u_bias=None, u_ref=0,
                              effect_section=None, effect_team=None):
        """
        dataframe: pandas DataFrame
            Dataframe with checked transects data
        n_labs: int
            Number of laboratories
        n_transects: float
            Number of transects to empirical uncertainty computation
        u_bias: float
            Uncertainty bias value
        u_ref: float
            Uncertainty on the discharge reference
        effect_section: str
            Name of the section column
        effect_team: str
            Name of the team column
        """
        if n_transects is not None and n_transects > 1:
            try:
                # Define average number of transects
                # N = len(self.transects_df['tr_q_total'])  # Total number of transects
                nb_measurements = len(np.unique(dataframe['meas_name']))
                # nb_participants = len(np.unique(dataframe['meas_label_4'])) # Number of measurements
                nb_transects = dataframe.groupby('meas_name')['meas_ntransects'].mean().values
                n_transect_mean = (1 / (nb_measurements - 1)) * (sum(nb_transects) - sum(nb_transects ** 2) /
                                                                     sum(nb_transects))

                if n_transects is None:
                    if nb_measurements > 1:
                        n_transects = (1 / (nb_measurements - 1)) * (sum(nb_transects) - sum(nb_transects ** 2) /
                                                                     sum(nb_transects))
                    else:
                        n_transects = nb_transects[0]

                if n_transects < 1:
                    return

                # Mean discharge
                mean_discharge = dataframe['tr_q_total'].mean()

                if effect_section is not None and effect_team is not None:
                    formula = f'tr_q_total ~ C({effect_section}) + C({effect_team}) + C({effect_section}):C({effect_team})'
                    s_2_list = self.double_effect_intercomparison(dataframe, formula, n_transect_mean)
                    div_list = [self.n_sections, self.n_teams, self.n_sections * self.n_teams,
                                self.n_sections * self.n_teams * self.n_transects]
                else:
                    formula = 'tr_q_total ~ C(meas_name)'
                    model = ols(formula, data=dataframe).fit()
                    aov_table = anova_lm(model, typ=1)
                    aov_table = pd.DataFrame(aov_table)
                    aov_table.columns = ['df', "ss", "ms", "F", "prf"]
                    # Mean square of residuals
                    s_2_e = aov_table.ms[1]
                    if aov_table.ms[0] > aov_table.ms[1]:
                        s_2_A = (aov_table.ms[0] - aov_table.ms[1]) / n_transect_mean
                    else:
                        s_2_A = 0

                    s_2_list = [s_2_A, s_2_e]
                    div_list = [n_labs, n_labs * n_transects]

                s_2_comb = sum(s_2_list)
                # Reproducibility
                s_R_prct = 100 * (s_2_comb ** 0.5) / mean_discharge

                s_prct_list = [100 * (i ** 0.5) / mean_discharge for i in s_2_list]
                # Repeatability
                s_r_prct = s_prct_list[-1]

                # Uncertainty bias
                if u_bias is None:
                    u_bias = np.sqrt(1.96 * np.sqrt(u_ref ** 2 + s_r_prct ** 2 / (n_transect_mean * nb_measurements) +
                                                    sum([i**2 for i in s_prct_list[:-1]])/nb_measurements))

                # Mean empirical uncertainty
                U_Q_n = 1.96 * np.sqrt(
                    u_bias**2 + sum([s_prct_list[i]**2/div_list[i] for i in range(len(s_prct_list))])
                )

                # 95% confidence interval on empirical uncertainty value
                gamma = s_2_comb ** 0.5 / s_2_list[-1] ** 0.5
                if n_transect_mean > 1:
                    A_r = 1.96 * (1 / (2 * nb_measurements * (n_transect_mean - 1))) ** 0.5
                else:
                    A_r = 0
                if nb_measurements > 1:
                    A_R = 1.96 * (((nb_measurements * (1 + n_transect_mean * (gamma ** 2 - 1)) ** 2) +
                                   ((n_transect_mean - 1) * (nb_measurements - 1))) / (
                                          2 * (gamma ** 4) * (n_transect_mean ** 2) * (
                                          nb_measurements - 1) * nb_measurements)) ** 0.5
                else:
                    A_R = 0

                U_Q_minn = np.nan
                U_Q_maxn = np.nan
                if effect_section is None or effect_team is None:
                    U_Q_minn = 1.96 * (np.nanmax([0,
                                                  (n_transects * (s_R_prct / (1 + A_R)) ** 2 - (n_transects - 1) *
                                                   (s_r_prct / (1 - A_r)) ** 2) / (n_transects * n_labs) +
                                                  u_bias ** 2])) ** 0.5
                    U_Q_maxn = 1.96 * (np.nanmax([0,
                                                  (n_transects * (s_R_prct / (1 - A_R)) ** 2 - (n_transects - 1) *
                                                   (s_r_prct / (1 + A_r)) ** 2) / (n_transects * n_labs)
                                                  + u_bias ** 2])) ** 0.5

                total_sum = sum([(s_prct_list[i] ** 2) * (0.01 ** 2) * (mean_discharge ** 2) / div_list[i]
                                 for i in range(len(s_prct_list))]) + (u_bias ** 2) * (0.01 ** 2) * (mean_discharge ** 2)

                self.transects_df.loc[dataframe.index, 'U_Q_n'] = U_Q_n
                self.transects_df.loc[dataframe.index, 'U_Q_n_min'] = U_Q_minn
                self.transects_df.loc[dataframe.index, 'U_Q_n_max'] = U_Q_maxn
                self.transects_df.loc[dataframe.index, 'u_bias'] = u_bias
                self.transects_df.loc[dataframe.index, 'Sum_n'] = total_sum
                self.transects_df.loc[dataframe.index, 'n transects U'] = n_transects
                self.transects_df.loc[dataframe.index, 'n measurements U'] = nb_measurements

                if len(s_prct_list) == 2:
                    self.transects_df.loc[dataframe.index, 's_L_prct'] = s_prct_list[0]
                else:
                    self.transects_df.loc[dataframe.index, 's_S_prct'] = s_prct_list[0]
                    self.transects_df.loc[dataframe.index, 's_P_prct'] = s_prct_list[1]
                    self.transects_df.loc[dataframe.index, 's_SP_prct'] = s_prct_list[2]

                self.transects_df.loc[dataframe.index, 's_r_prct'] = s_prct_list[-1]
                self.transects_df.loc[dataframe.index, 's_R_prct'] = s_R_prct
                self.transects_df.loc[dataframe.index, 'A_r'] = A_r
                self.transects_df.loc[dataframe.index, 'A_R'] = A_R

            except Exception:
                print(f'Intercomparison error')
                self.return_nan(dataframe.index)
        else:
            self.return_nan(dataframe.index)


    @staticmethod
    def double_effect_intercomparison(dataframe, formula, n_transects):
        """
        dataframe: pandas DataFrame
            Dataframe with checked transects data
        formula: str
            Anova formula for the selected section/team columns
        n_transects: float
            Number of transects to empirical uncertainty computation
        """
        # Apply anova
        model = ols(formula, data=dataframe).fit()
        aov_table = anova_lm(model, typ=1)
        aov_table = pd.DataFrame(aov_table)
        aov_table.columns = ['df', "ss", "ms", "F", "prf"]
        nb_s = aov_table.df[0] + 1
        nb_p = aov_table.df[1] + 1
        MS_S = aov_table.ms[0]
        MS_P = aov_table.ms[1]
        MS_SP = aov_table.ms[2]
        MS_r = aov_table.ms[3]

        # Check if square values are positive, zero otherwise
        if MS_SP > MS_r:
            s_2_SP = (1 / n_transects) * ((MS_SP) - (MS_r))
        else:
            s_2_SP = 0

        if MS_S > MS_SP:
            s_2_S = (1 / (nb_p * n_transects)) * ((MS_S) - (MS_SP))
        else:
            s_2_S = 0

        if MS_P > MS_SP:
            s_2_P = (1 / (nb_s * n_transects)) * ((MS_P) - (MS_SP))
        else:
            s_2_P = 0

        s_2_e = MS_r

        s_2_list = [s_2_S, s_2_P, s_2_SP, s_2_e]

        return s_2_list

    def return_nan(self, index_value):
        """ Fill uncertainty column from the selected row with nan
        index_value: int
            Row index which should be change
        """
        self.transects_df.loc[index_value, 'U_Q'] = np.nan
        self.transects_df.loc[index_value, 'U_Q_min'] = np.nan
        self.transects_df.loc[index_value, 'U_Q_max'] = np.nan
        self.transects_df.loc[index_value, 'U_Q_abs'] = np.nan
        self.transects_df.loc[index_value, 'U_Q_n'] = np.nan
        self.transects_df.loc[index_value, 'U_Q_n_min'] = np.nan
        self.transects_df.loc[index_value, 'U_Q_n_max'] = np.nan
        self.transects_df.loc[index_value, 'u_bias'] = self.u_bias
        self.transects_df.loc[index_value, 'Sum'] = np.nan
        self.transects_df.loc[index_value, 'Sum_n'] = np.nan
        self.transects_df.loc[index_value, 'n transects U'] = np.nan
        self.transects_df.loc[index_value, 'n measurements U'] = np.nan

        self.transects_df.loc[index_value, 's_L_prct'] = np.nan
        self.transects_df.loc[index_value, 's_S_prct'] = np.nan
        self.transects_df.loc[index_value, 's_P_prct'] = np.nan
        self.transects_df.loc[index_value, 's_SP_prct'] = np.nan
        self.transects_df.loc[index_value, 's_r_prct'] = np.nan
        self.transects_df.loc[index_value, 's_R_prct'] = np.nan
        self.transects_df.loc[index_value, 'A_r'] = np.nan
        self.transects_df.loc[index_value, 'A_R'] = np.nan
