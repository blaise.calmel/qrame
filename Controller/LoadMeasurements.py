"""
QRame
Copyright (C) 2023  INRAE

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import re
import numpy as np
import pandas as pd
import copy
from PyQt5.QtCore import pyqtSignal, QThread, QTimer
from qrev.Classes.Oursin import Oursin
from qrev.Classes.Measurement import Measurement
from qrev.Classes.Uncertainty import Uncertainty
import time
import os
import pickle


class LoadMeasurements(QThread):
    """Class to plot discharge distribution graph.

        Attributes
        ----------
        path_meas: list[str]
            List of measurement paths
        type_meas: list[str]
            List of measurement types (SonTek, RDI, QRev)
        name_meas: list[str]
            List of measurement names
        settings: dict
            Dictionary of selected settings
        measurements_dict: dict
            Dictionary of measurements objects
        measurement_error: list[str]
            List of measurements name that encountered error
        meas_checked: list[str]
            Selected measurements name
        raw_transects_df: pandas DataFraime
            DataFrame with every measurements
        meas_updates: list[str]
            List of measurement names which should be updated
        add_data: bool
            Indicate whether the new data should be added to the current data or overwritten them
        count_meas: int
            Number of measurements
        change: bool
            Specify if any change
        start_time: time object
            Start time of computation
        timer: QTimer object

        _complete: bool
            Check if process is completed or not
        _translate: QCoreApplication.translate object
            Save words which need to be translate
        """

    def __init__(self, parent=None, path_meas=None, type_meas=None, name_meas=None, settings=None,
                 meas_updates=None, add_data=False):
        """Initialize object using the specified canvas.

        Parameters
        ----------
        parent:
            Identifies parent GUI.
        path_meas: list[str]
            List of measurement paths
        type_meas: list[str]
            List of measurement types (SonTek, RDI, QRev)
        name_meas: list[str]
            List of measurement names
        settings: dict
            Dictionary of selected settings
        meas_updates: list[str]
            List of measurement names which should be updated
        add_data: bool
            Indicate whether the new data should be added to the current data or overwritten them
        """
        QThread.__init__(self)
        self.path_meas = path_meas
        self.type_meas = type_meas
        self.name_meas = name_meas
        self.settings = settings
        self.measurements_dict = {}
        self.measurement_error = []
        self.meas_checked = []
        self.raw_transects_df = None
        self.meas_updates = meas_updates
        self.add_data = add_data
        self.count_meas = None
        self.change = False
        self.start_time = time.time()
        self.timer = QTimer()
        self.timer.start(1000)

        self._complete = False
        self._translate = parent._translate

    finished = pyqtSignal()
    # Progress bar signals
    pbar_value = pyqtSignal(int)
    pbar_label = pyqtSignal(str)
    pbar_label2 = pyqtSignal(str)

    def run(self):
        """ Process load measurement thread
        """
        # Setup progress bar
        self.pbar_label.emit(self._translate("Main", 'Loading data...'))
        self.pbar_value.emit(0)
        self.start_time = time.time()

        self.timer.timeout.connect(self.update_pbar_timer)

        if self.meas_updates is None:
            # Open measurements according type
            if self.type_meas == 'pickle':
                self.load_pickle()
            else:
                self.extracting()

        if self.meas_updates is not None and len(self.meas_updates) > 0:
            self.update_measurements()
        else:
            # Create DataFrame
            self.pbar_value.emit(90)
            self.pbar_label.emit(self._translate("Main", f'Data formatting... '))
            self.raw_transects_df, error_meas = self.create_df(self.measurements_dict)

            # Report any error
            if len(error_meas) > 0:
                self.measurement_error.extend(error_meas)
                for error_name in error_meas:
                    del self.measurements_dict[error_name]

        self.add_data = False

        # Finish process
        self.pbar_value.emit(100)
        self.pbar_label.emit(self._translate("Main", f'Finish... ') +
                             f' {self.time_converter(time.time() - self.start_time)}')
        self._complete = True
        self.timer.timeout.disconnect()
        self.finished.emit()

    def load_pickle(self):
        """ Load pickle data
        """
        self.count_meas = 1
        self.pbar_label.emit(self._translate("Main", 'Loading pickle..'))
        self.pbar_value.emit(1)
        try:
            with open(os.path.abspath(self.path_meas), 'rb') as handle:
                self.measurements_dict |= pickle.load(handle)
        except Exception:
            pass

        # Load previous settings
        if 'meas_settings' in self.measurements_dict.keys():
            pickle_settings = self.measurements_dict['meas_settings']
            self.measurements_dict.pop('meas_settings', None)
        else:
            pickle_settings = None

        # Load previous meas checked
        if 'meas_checked' in self.measurements_dict.keys():
            new_meas_checked = list(self.measurements_dict['meas_checked'])
            self.measurements_dict.pop('meas_checked', None)
        else:
            new_meas_checked = [*self.measurements_dict]
        self.meas_checked.extend(list(set(new_meas_checked) - set(self.meas_checked)))

        # Check if pickle is added to current data
        if self.add_data:
            self.meas_updates = new_meas_checked
        else:
            if pickle_settings is not None:
                self.settings = pickle_settings

    def extracting(self):
        """ Extract ADCP moving boat data from folder
        """
        # Number of measurements
        self.count_meas = len(self.name_meas)
        # Measurement error list
        self.measurement_error = []
        if self.add_data:
            self.meas_updates = []

        # Loop over measurement
        i = 0
        for (path_meas, type_meas, name_meas) in zip(self.path_meas, self.type_meas, self.name_meas):
            self.pbar_label.emit(self._translate("Main", f'Open measurement... ') + name_meas +
                                 f' [{i}/{self.count_meas}]')
            # Load measurement data
            self.extract_meas(path_meas, type_meas, name_meas)

            self.meas_checked.append(name_meas)
            if self.add_data:
                self.meas_updates.append(name_meas)
            i += 1
            self.pbar_value.emit(int(i * 90 / self.count_meas))

    def update_pbar_timer(self):
        """ Update timer of progress bar
        """
        self.pbar_label2.emit(f'{self.time_converter(time.time() - self.start_time)}')

    @staticmethod
    def time_converter(value):
        """ Convert current time to duration
        value: float
            Time value (second)
        """
        hours, rest = divmod(value, 3600)
        minutes, seconds = divmod(rest, 60)
        if hours > 0:
            temps_string = "{:02}:{:02}:{:02}".format(int(hours), int(minutes), int(seconds))
        else:
            temps_string = "{:02}:{:02}".format(int(minutes), int(seconds))
        return temps_string

    def extract_meas(self, path_meas, type_meas, name_meas):
        """ Extract measurement file
        path_meas: str
            Path to the selected measurement
        type_meas: str
            Type of the selected measurement (TRDI, SonTek, QRev)
        name_meas:
            Name of the folder
        """
        try:
            # Load measurement data
            meas = Measurement(in_file=path_meas, source=type_meas, proc_type='QRev',
                               checked=True, run_oursin=False, use_weighted=self.settings['use_weighted'])

            # Remove invalid transect
            if self.settings['threshold'] is not None:
                discharge = meas.mean_discharges(meas)
                if not np.isnan(discharge['total_mean']):
                    remove = []
                    for transect_id in meas.checked_transect_idx:
                        per_diff = np.abs(((meas.discharge[transect_id].total - discharge['total_mean']) /
                                           discharge['total_mean']) * 100)
                        if per_diff > self.settings['threshold']:
                            remove.append(meas.checked_transect_idx.index(transect_id))

                    if remove:
                        for n in remove:
                            meas.checked_transect_idx.remove(n)
                        meas.selected_transects_changed(meas.checked_transect_idx)
                else:
                    raise Exception("Discharge is None")

            # Compute Oursin uncertainty
            meas.oursin = Oursin()
            meas.oursin.compute_oursin(meas)
            meas.initial_settings['NavRef'] = meas.current_settings()['NavRef']
            meas, _ = self.check_for_change(meas)
            meas.source = type_meas

            # Add the measurement to dict
            self.measurements_dict[f'{name_meas}'] = meas

        except Exception:
            self.measurement_error.append(name_meas)
            pass

    def update_measurements(self):
        """ Update measurements with selected settings
        """
        self._complete = False
        # Setup progress bar
        self.pbar_value.emit(0)
        self.count_meas = len(self.meas_updates)
        self.change = False

        # Loop over selected measurements to update
        id_meas = -1
        meas_change = {}
        for key in self.meas_updates:
            id_meas += 1
            self.pbar_value.emit(int((id_meas) * 90 / self.count_meas))
            self.pbar_label.emit(self._translate("Main", f'Update measurement ') + key +
                                 f' [{str(id_meas + 1)}/{self.count_meas}]')

            if key in self.measurements_dict:
                # Check if any change in measurement settings
                meas, change = self.check_for_change(copy.deepcopy(self.measurements_dict[key]))

                # If any change, update the measurement
                if change or self.add_data:
                    self.change = True
                    meas_change[key] = meas

        if len(meas_change) > 0:
            for key in meas_change.keys():
                self.measurements_dict[key] = meas_change[key]

        if self.change:
            self.raw_transects_df, _ = self.create_df(self.measurements_dict)

    def check_for_change(self, meas):
        """ Check if there is nay change in ADCP settings
        meas: Measurement object
            ADCP moving boat measurement object
        """
        change = False
        extrap_law = ['Auto - Auto', 'Power - Power', 'Constant - No Slip', '3-Point - No Slip']
        extrap_method = ['q', 'v']

        current_settings = meas.current_settings()
        if 'change_nav_ref' in self.settings.keys():
            # Change navigation reference
            if self.settings['change_nav_ref']:
                if self.settings['nav_ref'] in ['BT', 0]:
                    if current_settings['NavRef'] != 'bt_vel':
                        current_settings['NavRef'] = 'BT'
                        change = True
                elif self.settings['nav_ref'] in ['GPS', 1] and \
                        current_settings['NavRef'] not in ['gga_vel', 'vtg_vel']:
                    gps = True
                    for transect_idx in meas.checked_transect_idx:
                        if meas.transects[transect_idx].boat_vel.gga_vel is None or np.all(
                                meas.transects[transect_idx].gps.diff_qual_ens[
                                    ~np.isnan(meas.transects[transect_idx].gps.diff_qual_ens)
                                ] < 2):
                            gps = False
                            break
                    if gps:
                        current_settings['NavRef'] = 'GGA'
                        change = True
                else:
                    if current_settings['NavRef'] != meas.initial_settings['NavRef']:
                        current_settings['NavRef'] = meas.initial_settings['NavRef']
                        change = True

        # Change extrapolation method
        if 'extrap_type_idx' in self.settings.keys():
            selected_type = extrap_method[self.settings['extrap_type_idx']]
            if meas.extrap_fit.sel_fit[-1].data_type.lower() != selected_type:
                meas.extrap_fit.change_data_type(transects=meas.transects, data_type=selected_type)
                meas.extrap_fit.change_fit_method(meas.transects, 'automatic', len(meas.transects))
                change = True

        # Change Weighted extrapolation
        if 'use_weighted' in self.settings.keys():
            if selected_type == 'q' and self.settings['use_weighted'] != meas.extrap_fit.norm_data[-1].use_weighted:
                meas.current_settings()['UseWeighted'] = self.settings['use_weighted']
                current_settings['UseWeighted'] = self.settings['use_weighted']
                change = True

        if 'extrap_subsection' in self.settings.keys():
            if meas.extrap_fit.subsection[0] != self.settings['extrap_subsection'][0] or \
                    meas.extrap_fit.subsection[1] != self.settings['extrap_subsection'][1]:
                meas.extrap_fit.change_extents(
                    transects=meas.transects,
                    data_type=meas.extrap_fit.sel_fit[-1].data_type,
                    extents=self.settings['extrap_subsection'],
                    use_q=True,
                    sub_from_left=True,
                )
                change = True

        if 'excluded_dist' in self.settings.keys():
            excluded_dist = self.settings['excluded_dist']
            if isinstance(excluded_dist, float):
                if current_settings['WTExcludedDistance'] != excluded_dist:
                    current_settings['WTExcludedDistance'] = excluded_dist
                    change = True
            else:
                excluded_dist = meas.transects[meas.checked_transect_idx[0]].w_vel.orig_excluded_dist_m
                if excluded_dist != current_settings['WTExcludedDistance']:
                    current_settings['WTExcludedDistance'] = excluded_dist
                    change = True

        if change:
            meas.apply_settings(current_settings)

        # Change extrapolation
        change_extrap = False
        extrap_type = 'Manual'
        if 'extrap_law_idx' in self.settings.keys() or 'extrap_exponent' in self.settings.keys():
            if 'extrap_law_idx' in self.settings.keys():
                top_method, bot_method = extrap_law[self.settings['extrap_law_idx']].split(' - ')
            else:
                top_method = current_settings['extrapTop']
                bot_method = current_settings['extrapBot']
            if 'extrap_exponent' in self.settings.keys():
                exponent = self.settings['extrap_exponent']
            else:
                exponent = current_settings['extrapExp']

            if top_method == 'Auto':
                if meas.extrap_fit.fit_method.lower() != 'automatic':
                    meas.extrap_fit.change_fit_method(meas.transects, 'automatic', len(meas.transects))
                    change_extrap = True
                elif not isinstance(exponent, str) and exponent != current_settings['extrapExp']:
                    meas.extrap_fit.change_fit_method(transects=meas.transects, new_fit_method="Manual",
                                                      idx=len(meas.transects), top=current_settings['extrapTop'],
                                                      bot=current_settings['extrapBot'], exponent=exponent)
                    change_extrap = True
            elif top_method != current_settings['extrapTop'] or bot_method != current_settings['extrapBot'] or \
                    exponent != current_settings['extrapExp']:
                if isinstance(exponent, str):
                    # Get sensitivity data
                    q_sen = meas.extrap_fit.q_sensitivity
                    if bot_method == 'Power':
                        exponent = q_sen.pp_exp
                    else:
                        exponent = q_sen.ns_exp

                meas.extrap_fit.change_fit_method(transects=meas.transects, new_fit_method=extrap_type,
                                                  idx=len(meas.transects), top=top_method,
                                                  bot=bot_method, exponent=exponent)
                change_extrap = True

        # Apply change
        if change_extrap:
            meas.apply_settings(meas.current_settings())

        if change or change_extrap:
            change = True
            meas.oursin.compute_oursin(meas)

        return meas, change

    @staticmethod
    def create_df(measurements_dict):
        """ Create a pandas DataFrame from the measurement dictionary
        measurements_dict: dict
            Dictionary of measurements objects
        """
        transects_data = []
        measurement_error = []

        # Get name information
        name_split = [re.split(';|,|\*|_|-| ', str(key)) for key in measurements_dict.keys()]
        max_sep = max([len(i) for i in name_split]) + 1
        split_label = [f'meas_label_{i + 1}' for i in range(max_sep-1)]
        for i in name_split:
            if len(i) < max_sep:
                diff_len = max_sep - len(i)
                i.extend(diff_len * [''])

        # Loop over measurements
        i = 0
        for key in measurements_dict:
            try:
                if key != 'results_import_df':
                    meas = measurements_dict[key]
                    if not hasattr(meas, 'user_label'):
                        meas.user_label = ''

                    oursin = meas.oursin

                    # Get transects properties
                    trans_prop = Measurement.compute_measurement_properties(meas)
                    # Compute alpha
                    alpha = LoadMeasurements.compute_alpha(meas)

                    # Get current settings
                    current_settings = meas.current_settings()
                    if current_settings['NavRef'] == 'bt_vel':
                        nav_ref = 'BT'
                    elif current_settings['NavRef'] == 'gga_vel':
                        nav_ref = 'GGA'
                    elif current_settings['NavRef'] == 'vtg_vel':
                        nav_ref = 'VTG'
                    else:
                        nav_ref = 'None'

                    # Get comments
                    meas_comment = ''
                    for comment in meas.comments:
                        meas_comment += comment + ' '

                    # Get nav ref and water quality
                    navref_status = getattr(meas.qa, current_settings['NavRef'])["status"]
                    water_status = meas.qa.w_vel["status"]

                    meas.compute_map()

                    # Loop over transects
                    for id_transect in meas.checked_transect_idx:
                        transect = meas.transects[id_transect]
                        ind_transect = meas.checked_transect_idx.index(id_transect)
                        # Compute perimeter
                        perimeter = LoadMeasurements.compute_perimeter(transect)

                        if not hasattr(meas, 'source'):
                            meas.source = transect.adcp.manufacturer

                        # Transect dictionary
                        tr_dict = {
                            'meas_id': key + '_' + str(id_transect),
                            'meas_name': key,
                            'meas_mean_q': meas.mean_discharges(meas)['total_mean'],
                            'meas_std_q': np.std([meas.discharge[x].total for x in range(len(meas.transects)) if
                                                  meas.transects[x].checked == 1]),
                            'meas_mean_v': trans_prop['avg_water_speed'][id_transect],
                            'meas_oursin_95': oursin.u_measurement['total_95'].values[0],
                            'meas_ntransects': oursin.nb_transects,
                            'meas_tool': 'ADCP',
                            'meas_source': meas.source,
                            'meas_manufacturer': transect.adcp.manufacturer,
                            'meas_model': str(transect.adcp.model),
                            'meas_serial': str(transect.adcp.serial_num),
                            'meas_navigation': nav_ref,
                            'navref_quality': navref_status,
                            'water_quality': water_status,
                            'meas_depth': trans_prop['avg_depth'][id_transect],
                            'meas_width': trans_prop['width'][id_transect],
                            'meas_area': trans_prop['area'][id_transect],
                            'tr_excluded_dist': meas.transects[id_transect].w_vel.excluded_dist_m,
                            'tr_perimeter': perimeter,
                            'meas_top_method': meas.extrap_fit.sel_fit[len(meas.extrap_fit.sel_fit) - 1].top_method,
                            'meas_bot_method': meas.extrap_fit.sel_fit[len(meas.extrap_fit.sel_fit) - 1].bot_method,
                            'meas_alpha': alpha,
                            'meas_coef': meas.extrap_fit.sel_fit[len(meas.extrap_fit.sel_fit) - 1].coef,
                            'meas_exponent': meas.extrap_fit.sel_fit[len(meas.extrap_fit.sel_fit) - 1].exponent,
                            'start_time': meas.transects[id_transect].date_time.start_serial_time,
                            'end_time': meas.transects[id_transect].date_time.end_serial_time,
                            'tr_number': id_transect,
                            'tr_index': ind_transect,
                            'tr_q_total': meas.discharge[id_transect].total,
                            'tr_q_bottom': meas.discharge[id_transect].bottom,
                            'tr_q_top': meas.discharge[id_transect].top,
                            'tr_q_middle': meas.discharge[id_transect].middle,
                            'tr_q_left': meas.discharge[id_transect].left,
                            'tr_q_right': meas.discharge[id_transect].right,
                            'tr_oursin_u_syst': oursin.u['u_syst'][ind_transect],
                            'tr_oursin_u_compass': oursin.u['u_compass'][ind_transect],
                            'tr_oursin_u_movbed': oursin.u['u_movbed'][ind_transect],
                            'tr_oursin_u_meas': oursin.u['u_meas'][ind_transect],
                            'tr_oursin_u_ens': oursin.u['u_ens'][ind_transect],
                            'tr_oursin_u_boat': oursin.u['u_boat'][ind_transect],
                            'tr_oursin_u_depth': oursin.u['u_depth'][ind_transect],
                            'tr_oursin_u_water': oursin.u['u_water'][ind_transect],
                            'tr_oursin_u_top': oursin.u['u_top'][ind_transect],
                            'tr_oursin_u_bot': oursin.u['u_bot'][ind_transect],
                            'tr_oursin_u_left': oursin.u['u_left'][ind_transect],
                            'tr_oursin_u_right': oursin.u['u_right'][ind_transect],
                            'tr_oursin_u_cov': oursin.u['u_cov'][ind_transect],
                            'tr_oursin_u_total': oursin.u['total_95'][ind_transect],
                            'meas_comments': meas_comment,

                            # 'QRev_95': meas.uncertainty.total_95,
                            # 'tr_qrev_u_cov': meas.uncertainty.cov_95 / 2,
                            # 'tr_qrev_u_invalid': meas.uncertainty.invalid_95 / 2,
                            # 'tr_qrev_u_edges': meas.uncertainty.edges_95 / 2,
                            # 'tr_qrev_u_extrap': meas.uncertainty.extrapolation_95 / 2,
                            # 'tr_qrev_u_syst': meas.uncertainty.systematic,
                            # 'tr_qrev_u_moving_bed': meas.uncertainty.moving_bed_95 /2
                        }
                        tr_dict |= {k: v for k, v in zip(split_label, name_split[i])}
                        tr_dict |= {'meas_label_user': meas.user_label}

                        transects_data.append(tr_dict)
                    i += 1
            except Exception:
                measurement_error.append(key)
                pass
            transect_df = pd.DataFrame(transects_data)

        return transect_df, measurement_error

    @staticmethod
    def compute_alpha(meas):
        """ Compute alpha value
        meas: Measurement object
            ADCP moving boat measurement object
        """
        sel_fit = meas.extrap_fit.sel_fit[-1]
        current_settings = meas.current_settings()
        top_method = current_settings['extrapTop']

        if top_method == 'Constant':
            alpha = 1 / sel_fit.u[-1]
        elif top_method == '3-points':
            alpha = 1 / np.interp(1, sel_fit.z[-2:], sel_fit.u[-2:])
        else:
            # PP law to the top
            coef = meas.extrap_fit.sel_fit[-1].coef
            if coef != 0:
                alpha = 1 / coef
            else:
                alpha = np.nan

        return alpha

    @staticmethod
    def compute_perimeter(transect):
        """ Compute alpha value
        transect: Transect object
            Measurement transect object
        """
        ship_data = transect.boat_vel.compute_boat_track(transect)
        dmg_ens = np.copy(ship_data["dmg_m"])
        dmg_ens += transect.edges.left.distance_m
        depth = getattr(transect.depths, transect.depths.selected)
        depth_ens = np.copy(depth.depth_processed_m)

        # Add left edge
        edge_type = transect.edges.left.type
        if edge_type == "Rectangular":
            dmg_ens = np.insert(dmg_ens, 0, [0, 0])
            depth_ens = np.insert(depth_ens, 0, [0, depth_ens[0]])
        else:
            dmg_ens = np.insert(dmg_ens, 0, 0)
            depth_ens = np.insert(depth_ens, 0, 0)

        # Add right edge
        edge_type = transect.edges.right.type
        right_dist = dmg_ens[-1] + transect.edges.left.distance_m
        if edge_type == "Rectangular":
            dmg_ens = np.append(dmg_ens, [right_dist, right_dist])
            depth_ens = np.append(depth_ens, [depth_ens[-1], 0])
        else:
            dmg_ens = np.append(dmg_ens, right_dist)
            depth_ens = np.append(depth_ens, 0)

        points = np.vstack((dmg_ens, depth_ens)).T
        perimeter = 0
        for i in range(len(points)):
            x1, y1 = points[i]  # x : abscisse, y : hauteur d’eau
            x2, y2 = points[(i + 1) % len(points)]
            d = ((x2 - x1) ** 2 + (y2 - y1) ** 2) ** 0.5
            perimeter += d

        return perimeter
