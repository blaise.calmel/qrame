"""
QRame
Copyright (C) 2023  INRAE

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
"""
import numpy as np
from sigfig import round as sigfig_round

def scientific_notation(n, sig: int = 3):
    if np.isnan(n):
        n = 0
    elif np.isinf(n):
        return n

    return sigfig_round(n, sig)

def formatSize(size, extend='%', n=3):
    return f'{scientific_notation(size, n)} {extend}'

def units_conversion(units_id="SI"):
    """Computes the units conversion from SI units used internally to the
    desired display units.

    Parameters
    ----------
    units_id: str
        String variable identifying units (English, SI) SI is the default.

    Returns
    -------
    units: dict
        dictionary of unit conversion and labels
    """

    if units_id == "SI":
        units = {
            "L": 1,
            "Q": 1,
            "A": 1,
            "V": 1,
            "label_L": "m",
            "label_Q": "m3/s",
            "label_A": "m2",
            "label_V": "m/s",
            "ID": "SI",
        }

    else:
        units = {
            "L": 1.0 / 0.3048,
            "Q": (1.0 / 0.3048) ** 3,
            "A": (1.0 / 0.3048) ** 2,
            "V": 1.0 / 0.3048,
            "label_L": "ft",
            "label_Q": "ft3/s",
            "label_A": "ft2",
            "label_V": "ft/s",
            "ID": "English",
        }

    return units

