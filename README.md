# QRame
**[QRame](https://drive.google.com/drive/folders/1__8j_BSSSTwo-QXZLwv0X34NNmvNZx2w)** (QRevint Adcp Massive Extraction) is a software which allow to extract data from multiple ADCP measurements. It is based on [QRevint](https://www.genesishydrotech.com/qrevint) developed by HydroTech LLC. 
QRame opens measurements from moving-boat ADCP using data collected with any of the Teledyne RD Instrument (TRDI) or SonTek bottom tracking ADCPs. It can be used in order to re-process a data set of ADCP measurements or to compute ADCP intercomparison results.
QRame provides:
* Use QRevInt results (quality filters, optimal extrapolation law, interpolation...)
* Main results of each measurement (discharge decomposition, extrapolation law, navigation of reference..)
* Uncertainty sources (from OURSIN method)
* Intercomparison results
* Visualisation of main results with exportable graphics
* Summary table which can be export as csv

## Bugs and Improvement
Please report all bugs or any improvement suggestion by email to blaise.calmel@inrae.fr

## Licence
QRame
Copyright (C) 2023  INRAE

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

## Installation
Windows Executable can be downloaded from https://bit.ly/QRame


## Requirements and Dependencies
QRame is currently being developed using Python 3.9.0 and makes use of the following packages:  
contourpy==1.0.7  
cycler==0.11.0  
et-xmlfile==1.1.0  
fonttools==4.39.0  
importlib-resources==5.12.0  
joblib==1.2.0  
kiwisolver==1.4.4  
llvmlite==0.39.1  
matplotlib==3.7.1  
numba==0.56.4  
numpy==1.23.5  
openpyxl==3.1.2  
packaging==23.0  
pandas==1.5.3  
patsy==0.5.3  
Pillow==9.4.0  
profilehooks==1.12.0  
properscoring==0.1  
pyparsing==3.0.9  
PyQt5==5.15.9  
PyQt5-Qt5==5.15.2  
PyQt5-sip==12.11.1  
python-dateutil==2.8.2  
pytz==2022.7.1  
scikit-learn==1.2.2  
scipy==1.10.1  
seaborn==0.12.2  
six==1.16.0  
statsmodels==0.13.5  
threadpoolctl==3.1.0  
utm==0.7.0  
xmltodict==0.13.0  
zipp==3.15.0  

## Author
Blaise Calmel  
Research Engineer  
INRAE Lyon-Grenoble 
blaise.calmel@inrae.fr